+++
title = "Latest posts"
template = "index.html"
sort_by = "date"

[extra]
header = {title = "Welcome to my website!"}
section_path = "blog/_index.md"
max_posts = 4
post_listing_date = "both"
social_media_card = "img/social_cards/index.jpg"
+++

This is my personal website where I share insights and ramblings from the perspective of a legally deafblind programmer. *All opinions expressed here are my own.*

You can [learn more about me here](@/pages/about.md).

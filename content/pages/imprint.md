+++
path = "/imprint/"
title = "Imprint"
date = 2023-09-23
updated = 2023-10-05
template = "info-page.html"
description = "Legal information about Tim Böttcher online."

[extra]
social_media_card = "img/social_cards/imprint.jpg"
+++

## Information according to § 5 TMG and responsible for editing

Tim Böttcher  
Hainbuchenstraße 4  
Room 1205  
52074 Aachen  
Germany

## Contact

Phone: +49 (0) 157 79528013  
E-mail: contact@tim-boettcher.email

Source:
<https://www.e-recht24.de/impressum-generator.html>

+++
path = "/privacy-policy/"
title = "Privacy policy"
date = 2023-10-03
template = "info-page.html"
description = "Privacy policy of Tim Böttcher online."

[extra]
social_media_card = "img/social_cards/privacy_policy.jpg"
+++

## 1. Data protection at a glance

### General information

The following information provides a simple overview of what happens to your personal data when you visit this website. Personal data is any data that can be used to identify you individually.
For detailed information on the subject of data protection
Please refer to my privacy policy listed below this text.

### Data collection on this website

#### Who is responsible for data collection on this website?

The data processing on this website is done by me. My contact details
can be found in the ["Notice about the Responsible Entity"](#notice-about-the-responsible-entity) section of this privacy policy.

#### How do I collect your data?

Your data is collected when you provide it to me. This can be, for example
Data that you enter in a contact form.

Other data is collected automatically or with your consent by my IT systems when you visit the website. This is mainly technical data (e.g. internet browser, operating system or time of the page visit). This data is collected automatically as soon as you enter this website.

#### What do I use your data for?

Part of the data is collected in order to ensure error-free provision of the website. Other
data may be used to analyse your user behaviour.

#### What rights do you have regarding your data?

You have the right at any time to receive information free of charge about the origin, recipient and purpose of your stored personal data.
You also have the right to request the correction or deletion of this data.
If you have given your consent to data processing,
you can revoke this consent at any time for the future. You also have the right to request the restriction of the processing of your personal data under certain circumstances.  
Furthermore, you have the right to lodge a complaint with the competent supervisory authority.  

You can contact me at any time about this and other questions on the subject of data protection.

## 2. Hosting

I host the contents of my website with the following provider:

*Strato*

The provider is Strato AG, Otto-Ostrowski-Straße 7, 10249 Berlin (hereinafter "Strato"). When you visit my
website, Strato collects various log files including your IP addresses.

For more information, please see Strato's privacy policy:
<https://www.strato.de/datenschutz/>.

The use of Strato is based on art. 6 para. 1 lit. f GDPR. I have a legitimate
interest in the most reliable presentation of my website. Insofar as a corresponding
consent has been requested, the processing is carried out exclusively on the basis of art. 6 para. 1 lit. a
GDPR and § 25 para. 1 TTDSG, insofar as the consent allows the storage of cookies or the access to
information in the user's terminal device (e.g. device fingerprinting) within the meaning of the TTDSG. The
Consent can be revoked at any time.

### Order processing

I have concluded an order processing agreement (AVV) for the use of the above-mentioned service.
This is a contract required by data protection law which
that it guarantees that the personal data of my website visitors will only be processed according to my
instructions and in compliance with the DSGVO.

## 3. General notes and obligatory information

### Data protection

I take the protection of your personal data very seriously. I treat your
personal data confidentially and in accordance with this privacy policy.

When you use this website, various personal data are collected.  
Personal data is data by which you can be personally identified. This
Privacy Policy explains what information I collect and how I use it. It also explains how
and for what purpose this is done.

I would like to point out that data transmission on the Internet (e.g. communication by e-mail)
can have security gaps. Complete protection of data against access by third parties is not possible.

### Notice about the Responsible Entity

The responsible entity for data processing on this website is:

Tim Böttcher  
Hainbuchenstraße 4  
Room 1205  
52074 Aachen  
Germany

Phone: +49 (0) 157 79528013  
E-mail: contact@tim-boettcher.email

The responsible entity is the natural or legal person who, alone or jointly with others, decides on
the purposes and means of the processing of personal data (e.g. names, e-mail addresses, etc.). 

### Storage duration

Unless a more specific retention period is stated within this privacy policy, your personal data will remain with me until the purpose for processing it no longer applies. If you assert a
legitimate request for deletion or revoke your consent to data processing,
your data will be deleted, unless I have other legally permissible reasons for storing your
data (e.g. retention periods under tax or commercial law); in
the latter case, the deletion will take place after these reasons have ceased to exist.

### General information on the legal basis for data processing on this
website

If you have consented to data processing, I process your personal data
on the basis of art. 6 para. 1 lit. a GDPR or art. 9 para. 2 lit. a GDPR; insofar as special data categories get collected, they
are processed according to art. 9 para. 1 GDPR. In the event of express consent to the transfer of personal data to third countries, the data is also processed on the basis of art.
49 para. 1 lit. a GDPR. If you consent to the storage of cookies or to the access to information in
device (e.g. via device fingerprinting), the data processing will also be carried out on the basis of § 25 para. 1 TTDSG. This consent can be revoked at any time. If your data is required for
contract performance or for the implementation of pre-contractual measures, I process your data on the basis of art. 6 para. 1 lit. b GDPR.  
Furthermore, I process your data insofar as they are necessary for the fulfilment of a legal obligation on the basis of art. 6 para. 1 lit. c GDPR.

Additionally, the data processing may be based on my legitimate interest according to art. 6 para. 1 lit. f
GDPR. Information on the relevant legal basis in each individual case is provided in the following paragraphs of this privacy policy.

### Recipients of personal data

In the course of my business activities, I cooperate with various external bodies. In doing so
it is sometimes necessary to transfer personal data to these external bodies.

I only pass on personal data to external bodies if this is necessary within the framework of a
fulfilment of a contract, if I am legally obliged to do so (e.g. forwarding data to tax authorities), if I have a legitimate interest in passing on data in accordance with art. 6 para. 1 lit. f GDPR or if another legal basis permits the transfer of data. When using
processors, I only pass on personal data of my customers on the basis of a valid
contract on commissioned processing. In the case of joint processing, a contract on
joint processing is concluded.

### Withdrawal of your consent to data processing

Many data processing operations are only possible with your explicit consent. You can revoke your consent at any time. The lawfulness of the data processing carried out until then remains unaffected by the revocation.

### Right to object to data collection in special cases and to direct advertising (art. 21 GDPR)

IF DATA PROCESSING IS CARRIED OUT ON THE BASIS OF ART. 6 PARA. 1 LIT. E OR F GDPR YOU HAVE THE RIGHT TO OBJECT TO THE PROCESSING OF YOUR PERSONAL DATA AT ANY TIME; THIS ALSO APPLIES TO A PROFILING BASED ON THE DATA. FoR THE RESPECTIVE LEGAL BASIS ON WHICH THE DATA PROCESSING IS BASED,
PLEASE REFER TO THIS PRIVACY POLICY. IF YOU OBJECT,
I WILL NO LONGER PROCESS YOUR PERSONAL DATA, UNLESS I CAN DEMONSTRATE LEGITIMATE GROUNDS FOR THE PROCESSING THAT
OVERRIDES YOUR INTERESTS, RIGHTS AND FREEDOMS, OR THE PROCESSING IS FOR THE PURPOSE OF ASSERTING, EXERCISING OR DEFENDING LEGAL CLAIMS (OBJECTION ACCORDING TO ART. 21 PARA. 1 GDPR).

IF YOUR PERSONAL DATA ARE PROCESSED FOR THE PURPOSE OF DIRECT MARKETING,
YOU HAVE THE RIGHT TO OBJECT AT ANY TIME TO THE PPROCESSING OF YOUR PERSONAL DATA FOR THE PURPOSE OF SUCH ADVERTISING.
THIS ALSO APPLIES TO PROFILING, INSOFAR AS IT IS IN CONNECTION WITH SUCH DIRECT ADVERTISING. IF YOU OBJECT, YOUR PERSONAL DATA WILL SUBSEQUENTLY NO LONGER BE USED FOR THE PURPOSE OF DIRECT MARKETING (OBJECTION
ACCORDING TO ART. 21 PARA. 2 GDPR).

### Right of appeal to the competent supervisory authority

In the event of breaches of the GDPR, the data subjects have a right of appeal to a
authority, in particular in the Member State of their habitual residence, place of work or the place of the alleged infringement. The right of appeal exists regardless of other
administrative or judicial remedies.

### Right to data portability

You have the right to have data that I process automatically on the basis of your consent or in performance of a contract sent to you or to a third party in a standard, machine-readable format. If you request the direct transfer of the data to another responsible party, this will only be done insofar as it is technically feasible.

### Information, correction and deletion

Within the framework of the applicable legal provisions, you have the right at any time to obtain
information about your stored personal data, their origin and recipients and the purpose of the data processing and, if applicable, the right to have this data corrected or deleted free of charge. For this and
other questions on the subject of personal data you can contact me at any time.

### Right to restriction of processing

You have the right to request the restriction of the processing of your personal data.  
To do this, you can contact me at any time. The right to restriction of processing exists in
the following cases:

- If you dispute the accuracy of your personal data stored by me, I usually need time to check this. For the duration of the verification, you have the right to request the restriction of the processing of your personal data.
- If the processing of your personal data happened/is happening unlawfully, you can request the restriction of data processing instead of erasure.
- If I no longer need your personal data, but you need it to exercise, defend or assert legal claims, you have the right to request restriction of the processing of your personal data instead of erasure.
- If you have lodged an objection pursuant to art. 21 (1) GDPR, a balancing of your and my interests must be carried out. As long as it has not yet been determined whose interests prevail, you have the right to demand the restriction of the processing of your personal data.

If you have restricted the processing of your personal data, this data may - apart from its storage - only get processed with your consent or for the assertion, exercise or defence of legal claims or for the protection of the rights of another natural or legal person or for reasons of important public interest of the European Union or a Member State.

### SSL or TLS encryption

For security reasons and to protect the transmission of confidential content, such as orders or enquiries that you send to me as the site operator, this site uses SSL or TLS encryption. You can recognise an encrypted connection by the fact that the address line of the browser changes from
`http://` to `https://` and by the lock symbol in your browser line.
If SSL or TLS encryption is activated, the data you transmit to us cannot be read by third parties.

## 4. Data collection on this website

### Cookies

My Internet pages use so-called "cookies". Cookies are small data packets and do no harm to your end device. They are stored on your end device either temporarily for the duration of a session
(session cookies) or permanently (permanent cookies). Session cookies
are automatically deleted at the end of your visit. Permanent cookies remain stored on your end device
until you delete them yourself or until they are automatically deleted by your web browser.

Cookies can originate from me (first-party cookies) or from third-party companies (so-called third party cookies). Third party cookies enable the integration of certain services of third party companies within websites (e.g. cookies for processing payment services).

Cookies have various functions. Many cookies are technically necessary, as certain website functions would not work without them (e.g. remembering the colour theme or displaying videos). Other cookies can be used to evaluate user behaviour or for advertising purposes.

Cookies that are used to carry out the electronic communication process, to provide
functions you have requested (e.g. to remember the colour theme) or to optimise the website (e.g. cookies for measuring the web audience) (necessary cookies), are stored on the basis of art. 6 para. 1 lit. f GDPR, unless another legal basis is specified. 

The website operator has a legitimate interest in the storage of cookies necessary for the
technically error-free and optimised provision of its services. Insofar as consent to the
storage of cookies and comparable recognition technologies has been requested, the
processing is carried out exclusively on the basis of this consent (art. 6 para. 1 lit. a GDPR and § 25 para. 1 TTDSG); the consent can be revoked at any time.

You can set your browser in such a way that you are informed about the setting of cookies and
allow cookies only in individual cases, exclude the acceptance of cookies for certain cases or deny them per default
and activate the automatic deletion of cookies when closing the browser. If you deactivate cookies, the functionality of this website may be limited.

You can find out which cookies and services are used on this website in this privacy policy.

### Requests by e-mail or telephone

If you contact me by e-mail or telephone, your enquiry, including all the personal data associated with it
(name, enquiry) will be stored and processed by me for the purpose of processing your enquiry. I will not pass on this data without your consent.

The processing of this data is based on art. 6 para. 1 lit. b GDPR, insofar as your request is related to
fulfilment of a contract or is necessary for the implementation of pre-contractual measures.
In all other cases, the processing is based on my legitimate interest in the
effective processing of the enquiries addressed to me (art. 6 para. 1 lit. f GDPO) or on your
consent (art. 6 para. 1 lit. a GDPR) if this has been requested; consent can be revoked at any time.

The data you send to me via contact requests will remain with me until you request deletion, revoke your consent to storage or the purpose for storing the data no longer applies (e.g. after the processing of your request has been completed). Mandatory legal provisions -
in particular statutory retention periods - remain unaffected.

### Comment function on this website

For the comment function on this site I store information on the time the comment was made and, if you do not post anonymously, the user name you have chosen in addi`ion to your comment.


#### Duration of storage of comments

The comments and the associated data are stored and remain on this website
until the commented content has been completely deleted or the comments have to be deleted for legal reasons (e.g. offensive comments).

#### Legal basis

The storage of comments is based on your consent (art. 6 para. 1 lit. a GDPR). You can revoke your consent at any time. For this purpose, an informal request by e-mail to me is sufficient. The legality of the data processing already carried out remains unaffected by the revocation.

### Website statistics

To improve the website, I collect the following non-personal data:

- Referrer: From which source the website got requested, if available (i.e., the previous website).
- Requested URL: Which specific URL got requested.
- User-Agent: Which browser got used to visit the website.
- Country name: Name of the country from which the request originated, based on the IP address.
- Language: The language preferences set in your browser.
- Screen size: The size of the screen used to view the website.
- Time: The time at which the request was made.
- Session identifier: An identifier based on the IP address, browser information and a random number; this identifier is valid for eight hours and gets deleted afterwards.

I do not track unique users across sessions, nor do I track how long they stay on the website or where they move to afterwards.

A self-hosted version of [Goat Counter](https://www.goatcounter.com/) gets used to collect the data. This is an open source, privacy-friendly software for web analysis.

Source:
<https://www.e-recht24.de>

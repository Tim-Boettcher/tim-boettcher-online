+++
path = "/de/imprint/"
title = "Impressum"
date = 2023-10-03
updated = 2023-10-05
template = "info-page.html"
description = "Rechtliche Informationen über Tim Böttcher online."

[extra]
social_media_card = "img/social_cards/de_imprint.jpg"
+++

## Angaben gemäß § 5 TMG sowie verantwortlich für Redaktion

Tim Böttcher  
Hainbuchenstraße 4  
Zi. 1205  
52074 Aachen  
Deutschland

## Kontakt

Telefon: +49 (0) 157 79528013  
E-Mail: kontakt@tim-boettcher.email

Quelle:  
<https://www.e-recht24.de/impressum-generator.html>

+++
path = "/about"
title = "About me"
template = "info-page.html"
date = 2023-09-23
updated = 2023-11-17
description = "Here you can find information about the author and content of Tim Böttcher online."

[extra]
social_media_card = "img/social_cards/about.jpg"
+++

I am Tim Böttcher, a legally deafblind programmer living in Germany.

## Qualifications

- Licensed mathematical-technical software developer
- Studying applied math and computer science
- In-depth knowledge and understanding of (web/app) accessibility, primarily for the (deaf)blind
- Experience with system administration, primarily on Ubuntu 20/22
- Markup and programming languages:
  - Rust
  - PHP
  - JavaScript
  - HTML
  - CSS/SCSS/Sass
  - SQL
  - Java
  - C#
  - C++
  - Inform 7
  - Lua
  - Python
  - Kotlin
- Frameworks:
  - [Yew](https://yew.rs/)
  - [jQuery](https://jquery.com/)
- Fluent in German and English

## Hobbies

I am a martial artist: I do German ju-jutsu (blue belt/2nd kyu), Võ Đạo Việt Nam and shotokan karate (yellow belt/8th kyu). I also enjoy reading and occasionally do creative writing. I like to combine creative writing and programming to create [interactive fiction](https://en.wikipedia.org/wiki/Interactive_fiction) and do various self-hosting projects as well.

## Topics

In the [blog section of my website](@/blog/_index.md) I will share some of my insights into programming, technology and accessibility. Additionally I will comment on other topics that piqued my interest, primarily based on news articles that I found interesting.

+++
path = "/de/about"
title = "Über mich"
template = "info-page.html"
date = 2023-10-05
updated = 2023-11-17
description = "Hier finden Sie Informationen über den Autor und Inhalt von Tim Böttcher online."

[extra]
social_media_card = "img/social_cards/de_about.jpg"
+++

Ich bin Tim Böttcher, ein gesetzlich taubblinder Programmierer, der in Deutschland lebt.

## Qualifikationen

- Lizensierter mathematisch-technischer Softwareentwickler
- Studiere angewandte Mathematik und Informatik
- Tiefgründiges Wissen über und Verständnis für Barrierefreiheit (Web/Apps), vor allem für (taub)blinde Menschen
- Erfahrung mit Systemadministration, vor allem auf Ubuntu 20/22
- Markup und Programmiersprachen:
  - Rust
  - PHP
  - JavaScript
  - HTML
  - CSS/SCSS/Sass
  - SQL
  - Java
  - C#
  - C++
  - Inform 7
  - Lua
  - Python
  - Kotlin
- Frameworks:
  - [Yew](https://yew.rs/)
  - [jQuery](https://jquery.com/)
- Beherrsche Deutsch und Englisch fließend

## Hobbys

Ich bin Kampfsportler: Ich mache Ju-Jutsu (blauer Gürtel/2. Kyu), Võ Đạo Việt Nam und Shotokan-Karate (gelber Gürtel/8. Kyu). Außerdem lese ich gerne und beschäftige mich gelegentlich mit kreativem Schreiben. Ich kombiniere gerne kreatives Schreiben und Programmieren, um [interactive Fiction](https://de.wikipedia.org/wiki/Interactive_Fiction) zu erstellen, und betreibe auch verschiedene Self-Hosting-Projekte.

## Themen

Im [Blog-Bereich meiner Website](@/blog/_index.de.md) werde ich einige meiner Erkenntnisse über Programmierung, Technologie und Barrierefreiheit weitergeben. Außerdem werde ich mich zu anderen Themen äußern, die mein Interesse geweckt haben, hauptsächlich auf der Grundlage von aktuellen Nachrichten.

+++
title = "Which screen reader is the most Braille-display friendly?"
template = "page.html"
description = "Mirror, mirror on the wall, which is the most Braille display-friendly screen reader of all? The answer to that question may surprise you."
date = "2025-01-04"
updated = 2025-03-02
slug = "braille-display-friendly-screen-reader"

[taxonomies]
tags = ["accessibility", "Braille displays", "BRLTTY", "JAWS", "NVDA", "screen readers", "VoiceOver"]

[extra]
toc_ignore_pattern = "^Table of contents$"
isso = true
social_media_card = "img/social_cards/blog_braille_display_friendly_screen_reader.jpg"
+++

Mirror, mirror on the wall, which is the most Braille display-friendly screen reader of all? The answer to that question may surprise you.<!-- more -->

## Table of contents

<!-- toc -->

## Motivation

There are two primary ways in which blind users can access a computer or smartphone: speech output or Braille ([^1]). Some users also use these methods in parallel---for instance by usually relying on speech output but using a Braille display to proofread a text ([^2]). While hypothetically both strategies are equally suited to provide access to digital content, there are significant differences in feasibility when it comes to accessibility testing; see the table below for a comparison:

| Speech output                                    | Braille displays                                                                    |
| ------------------------------------------------ | ----------------------------------------------------------------------------------- |
| There is free, open-source software available.   | Requires expensive hardware.                                                        |
| Can get configured for many languages.           | Learning Braille is (usually) necessary.                                            |
| Sometimes no software installation is necessary. | Usually following setup guides and installing software (like drivers) is necessary. |

{{ admonition(type="note", text="I am aware there are *Braille viewer* features available in many screen readers. However, these do not necessarily reflect the representation of the user interface on a Braille display accurately, in my experience.") }}

It is not surprising, then, that support for speech output is often better and more comprehensive. For example, the NVDA screen reader did not report changes of ARIA live regions on Braille displays for years; the related GitHub issue was opened in 2017 and closed in 2023, after nearly six years ([^3]). For many deafblind people like me there is no alternative to using Braille displays to access digital content, however; finding the screen reader that supports our needs the most is critical.

## Comparison criteria

So what makes a screen reader Braille display-friendly? Obviously, it should

- Support as many Braille display models as possible,
- Detect the connected Braille display(s) automatically and
- Have a wide variety of available Braille tables (i.e., support for representing different languages in Braille).

Those are the basics. But to get considered Braille display-friendly, the screen reader also should

- Support displaying unusual characters such as emoji or foreign alphabets in some way,
- Make all information that gets spoken available in Braille in some way as well,
- Support scientific notations (e.g., mathematical or chemical formulae),
- Offer means for the user to access information about the font (attributes like bold or italics, font size, colour, ...) and
- Make it easy to use multiple Braille displays in parallel and switch between Braille displays quickly.

Additionally, screen readers should be customizable and extensible, e.g., by allowing users to change how control elements are represented on a Braille display and by allowing users to write scripts that extend or alter the screen reader's behaviour.

## The screen readers

I compared the following screen readers:

- BRLTTY (standalone on Android, with Narrator on Windows; also gets used by Orca on GNU Linux, though I did not test it in that context),
- JAWS (Windows),
- NVDA (Windows) and
- VoiceOver (iOS and Mac OS).

## BRLTTY

{% admonition(type="note") %}
I know that BRLTTY is not a screen reader in the classical sense and that "proper" screen readers like Orca interact with it via BRLTTY's BrlAPI. This blog post is only about Braille display support, however. Any differences in Braille display support between Orca and Narrator would result from differing usage of the BrlAPI.")
{% end %}

BRLTTY is a background process (a DAEMON) which enables blind users to access the Linux/Unix console using a Braille display ([^4]; [^5]). Since it is open-source software and exploses the BrlAPI, it got included in other screen readers such as Narrator to handle the Braille display support. For Android there is an app which can get downloaded from Google Play and which registers an accessibility service and a custom keyboard to handle Braille displays.

### Compatibility

BRLTTY is compatible with an [extensive list of Braille displays](https://brltty.app/details.html#displays). This, as well as its [modest list of requirements](https://brltty.app/details.html#requirements) is a definite benefit of BRLTTY.

### Connectivity

BRLTTY can only handle one connection to a Braille display at a time. However, it can detect Braille displays automatically and either chooses the first available one (which may take a while) or quickly connects to a predefined one. The caveat of the latter is that if the predefined Braille display is not available, BRLTTY takes even longer to connect to a different one or does not connect to a different one at all.

### Multi-lingual support

BRLTTY is the origin of the LibLouis project which drastically improved support for many Braille tables (thus making it easier to support many languages in Braille) and also improved upon representation of unusual symbols like emoji ([^6]; [^7]). Consequently, BRLTTY offers excellent support for representing symbols that are not part of the active Braille table in a more meaningful way. For example, the Greek letter Γ (Gamma) is not usually part of Braille tables; since a Braille display with 8 dots per block can only represent 256 possible combinations (i.e., symbols) creators of Braille tables have to strategically choose which letters and symbols are particularly important to a given language. If a symbol gets encountered that the Braille table cannot represent, a placeholder character gets shown which is difficult to interpret for Braille display users. LibLouis instead chooses to show the unicode code point of the symbol---for Gamma, it would display `'\x0393'`. Of course, having to look up which character belongs to a given unicode code point is hardly ergonomic; it also means that one symbol takes up eight blocks of the Braille display, from the first to the last apostrophe. In my opinion, the benefits outweigh the disadvantages, but it is debatable.

### Extra

Due to the above-mentioned support for LibLouis, BRLTTY can represent any symbol, irrespective of the selected Braille table, though it is not exactly ergonomic. To my knowledge, BRLTTY does not directly support representing scientific formulae, however. Using multiple Braille displays in parallel is not possible and quickly switching between Braille displays is not easy. There are commands to access the font attributes of a text, though there is no way to access them in the text itself without actively triggering the command. How faithfully BRLTTY reports information largely depends on how well the BrlAPI gets used, e.g., to report system notifications.

### Customisation and extensibility

BRLTTY allows customisation of several settings as well as changing key bindings for commands. That often requires placing files in a certain place however and thus is not exactly straight forward. Since BRLTTY is open-source and exposes the BrlAPI, it is also technically possible to extend or alter its functionality as needed. There is documentation available to make this easier. One has to be a fairly seasoned programmer to undertake this, however.

## JAWS

Job Access With Speech (JAWS) is a commercial screen reader for Windows ([^8]). Like VoiceOver, it is a proprietary screen reader that does not make its source code accessible to the general public. Its price tag is hefty, but it is also doubtlessly the most feature rich screen reader on the market. But does that make it the most Braille display-friendly?

### Compatibility

To my knowledge, Freedom Scientific does not publish a comprehensive list of supported Braille displays. They do list the Braille displays supported at installation time---out of the box, basically---but JAWS is able to communicate with far more Braille displays once installed properly ([^9]). It should be compatible with most modern Braille display models, though depending on the model, additional drivers may have to get installed.

### Connectivity

Normally, JAWS can only handle one active Braille display at a time. There is a setting to auto-detect braille displays via Bluetooth, however. If that setting is active, JAWS can connect to one Braille display via Bluetooth and a second one via USB. An issue with the Braille driver loaded by Narraotr (LibUSB) results in connectivity issues via USB with many or all Braille displays. This is not the fault of JAWS, however, and should get addressed by Microsoft ([^10]).

### Multi-lingual support

Again, Freedom Scientific does not publish an easily accessible list of supported Braille tables. However, JAWS lists over 110 Braille profiles, many of them concerning less-known languages as well, so it is safe to say their multi-lingual support is excellent. It should be noted that JAWS does not provide means to represent symbols that are not part of the active Braille table, though.

### Extra

JAWS speaks emoji but does not represent them in Braille. Emoji and icons usually just result in a blank space on the Braille display; unusual symbols may also result in a some kind of "not representable" placeholder, such as a block with all eight dots active. While JAWS has a checkbox to activate LibLouis Braille tables in the Braille profile settings, that setting does not take effect nor stick in languages like German. I do not understand why.  
As for scientific notations and math, JAWS has a so-called "math viewer" ([^11]). Its support for Braille displays is disappointingly poor, though: When a mathematical formula gets encountered, the Braille display only displays "math". When the math viewer gets opened (which is far from ergonomic, by the way), the Braille display remains blank unless the active Braille table is English, in which case Nemeth code for the formula gets displayed. Since JAWS 2024 there is also experimental support for MathCAT. But this only improves the math viewer marginally, as MathCAT's full functionality and features do not get harnessed. MathCAT supports spoken output in multiple languages and can represent formulae in multiple notations, including Nemeth, ASCIIMath and LaTeX ([^12]; [^13]). To date, JAWS only harnesses a small subset of these functionalities---and even that only if you turn the experimental feature on.  
JAWS does a good job when it comes to making all spoken information accessible in Braille as well; in addition to rendering most information in Braille in some way, there is also a log (called speech history) that lists the last 100 things JAWS said, so you can review that on your Braille display.  
There are also various commands available to review information about the font; you can customise how you would like JAWS to represent things like underlined or coloured text, although you can only define how such text should get "underlined," so the possible variations are limited. JAWS offers a special mode in which it displays font attributes but you have to switch to that mode explicitly. The command to yield information about foreground and background colour also sometimes results in confusing output such as "Standard on Standard". Overall, I would say it is not particularly ergonomic, but it usually does its job.  
Working with multiple Braille displays is possible but also not ergonomic. One can define only one standard Braille display and other displays can only get connected via Bluetooth if the setting for autodetection is enabled. One can relatively quickly change the standard braille display but will usually have to restart JAWS for the change to take effect.

### Customisation and extensibility

JAWS has a multitude of settings, to the point that it can be quite overwhelming to find the setting you are looking for. This, along with the option to change settings on a per application and even per webpage basis, makes JAWS particularly customisable and flexible, however. It is also extensible: One can write scripts in a specialised scripting language to extend JAWS capabilities---for instance to make an application more accessible. The problem with this approach becomes apparent in GitHub repositories for JAWS scripts, though ([^14]). Since there is no official marketplace or repository for JAWS scripts, there is also no centralised quality control, no compatibility checking and no convenient way to import, update and remove them. It is left up to the user or possibly developer of the scripts to do that. Moreover, since JAWS uses a custom scripting language, the development utilities for such scripts are rather limited.

## NVDA

Non-Visual Desktop Access (NVDA) is, like JAWS, a screen reader for Windows. Unlike JAWS, it is free and open-source. While JAWS is still more popular than NVDA, the latter has gained in popularity while JAWS lost ground over the past four years ([^15]; [^16]). One reason, of course, is that NVDA is free software---anyone can readily download and install it, without needing to pay for licenses. Additionally, many companies which are not willing to pay for Freedom Scientific's JAWS accessibility testing suite instead test accessibility using NVDA, which can result in better supportfor that screen reader. So, does NVDA's Braille display-friendliness match its growing popularity?

### Compatibility

NV Access publishes [a comprehensive list of Braille displays which NVDA supports](https://www.nvaccess.org/files/nvda/documentation/userGuide.html#SupportedBrailleDisplays). That should include the vast majority of modern Braille display models.

### Connectivity

NVDA can auto-detect most of the supported Braille displays. That makes connecting to them both via USB and Bluetooth very easy. Like JAWS, NVDA may have trouble connecting via USB if Narrator installed LibUSB, however.

### Multi-lingual support

NVDA utilises LibLouis as well. In my estimation, it does not support quite as many Braille tables out of the box as JAWS does (though there are many available already), but additional Braille tables can get added manually. It also offers an option to attempt normalising unicode characters: This is important when some symbols, like the German letter "ü", can get represented in more than one way in unicode. When not normalised, this can result in inconsistent representation of such letters on a Braille display; normalising unicode symbols may have undesirable side-effects in some languages, however. The advantages and caveats of LibLouis remain the same as for BRLTTY.

### Extra

NVDA itself only supports the features of LibLouis for displaying unusual symbols like emoji. However, the BrailleExtender add-on for NVDA ([^17]) makes it possible to display a textual description of the character instead of the unicode code point. The Greek letter Gamma would then appear as `[Gamma]` instead of `'\x0393'`.  
In standard NVDA, most information that gets spoken can already get accessed via a Braille display, but not all. Here, too, BrailleExtender helps to close the gap, making it possible to display a lot more information on the Braille display which would otherwise have to get queried via a command or would be entirely unavailable. The add-on also adds an improved history for things NVDA said previously, similar to JAWS.  
Support for scientific formulae is not natively included in NVDA, but can get added via a MathCAT add-on ([^18]). Unlike JAWS, that add-on makes full usage of all available MathCAT features which means scientific formulae are very accessible, particularly on Braille displays.  
While switching between Braille displays is fairly easy on NVDA due to its auto-detection of connected Braille displays, the BrailleExtender add-on makes this process even more ergonomic. It allows for storing two Braille displays instead of one, which means that NVDA connects to two in parallel, reducing the number of necessary switches.  
Lastly, BrailleExtender can also add specific tags to the text to signal whether it is bold, a spelling error, the font family in use, the current page number and much more. Having all this information enabled can quickly become overwhelming as too many tags get added to the regular text. By strategically enabling or disabling some settings, perhaps even on a per application basis, one can harness the added information, however.

### Customisation and extensibility

Like JAWS, NVDA is very customisable. Per default it has fewer customisation options than JAWS does, but the number of options can increase as more add-ons get added to NVDA. It also supports using different profiles for the active settings, which get triggered by events like a specific program going into the foreground. As mentioned before, one can extend NVDA's behaviour through add-ons written in Python. Unlike JAWS, NVDA has an official marketplace for add-ons and also makes it easy to see with which versions of NVDA an add-on was tested. Furthermore, because the programming language is Python, there are many development tools available which can improve the quality and maintainability of the add-ons.

## VoiceOver

VoiceOver is Apple's screen reader for iOS, iPadOS and MacOS. Unlike on other operating systems, it is the only screen reader that can get used on these platforms. So how does it fare in terms of Braille display-friendliness?

### Compatibility

While Apple does not publish a comprehensive list of Braille displays supported on iOS or iPadOS, it does publish a list of Braille displays supported on MacOS ([^19]). While it is not guaranteed that all of them also get supported on iOS---indeed, it is safe to assume that at least those that do not support Bluetooth cannot connect to an iPhone or iPad---the list still is lengthy, so compatibility should be good.

### Connectivity

Like all the other screen readers, VoiceOver supports connections via USB and Bluetooth. Unlike JAWS, NVDA and BRLTTY it does not support automatically detecting Braille displays via Bluetooth, though: It always connects to the selected Braille display or no Braille display at all, although a Bluetooth and USB connection to the same braille display may exist in parallel.

### Multi-lingual support

Apple does not provide a comprehensive list of Braille tables it supports. However, an examination of the supported Braille tables in the VoiceOver settings suggests that they support many or all LibLouis braille tables in addition to some other ones; in German, for instance, the standard braille table is not the LibLouis braille table for German but a different one defined by Apple. Still, given that LibLouis braille tables are supported, multi-lingual Braille support should be at least on par with other screen readers which rely on LibLouis, like BRLTTY and NVDA.

### Extra

Apple pioneered representing emoji by utilising their unicode description. That results in :heart: getting represented as `:red heart:`, for instance. In its default braille tables, there may be a similar behaviour as that of JAWS, though: Unfimiliar symbols get replaced with a placeholder symbol. In LibLouis tables, the same rules as for BRLTTY apply.  
There used to be many things that VoiceOver said but did not represent in Braille. That is still the case on MacOS, in particular, but the gap has become smaller. One caveat is that VoiceOver only provides the past ten notifications to the user and does not have a comprehensive speech log. Overall, the loss of information when relying purely on Braille is likely higher than with NVDA and JAWS.  
VoiceOver has an option to represent mathematical formulae as Nemeth code. In older VoiceOver versions, this option only works if the active Braille table is English, though. In other languages, lines that would otherwise contain Math are simply blank. That makes scientific formulae rather inaccessible, especially if one is not familiar with Nemeth. While quickly switching between braille tables is possible via the rotor, this does not feel ergonomic.  
VoiceOver, like JAWS, has commands which can yield information about the current font; it can also highlight underlined or bold words, for instance, but remains similarly ambiguous as JAWS as to what the exact font attributes are unless specifically prompted using the commands.  
Using multiple braille displays in parallel is not possible with VoideOver, as only one Braille display can be active at a given time. The exception to this rule is when one Braille display is connected via USB and another one via Bluetooth.  
One feature that is unique to VoiceOver is its "screen recognition" which utilises AI to make the visible elements onscreen more accessible, potentially improving upon an app's accessibility. This feature is necessary, though, as VoiceOver does not allow writing scripts for that purpose.

### Customisation and extensibility

VoiceOver is relatively customisable, especially on MacOS via the VoiceOver app. It is not extensible, however, although one can potentially write quick actions to make some otherwise complicated actions easier; this is not screen reader-specific, though.

## Conclusion

So, which screen reader is the most Braille display-friendly? In my opinion---and I hope you can see why---it is NVDA, provided you install the BrailleExtender and MathCAT add-ons. The good news is that all screen readers mentioned here offer a good level of Braille display support; the exact mileage you get out of a given screen reader can always vary depending on your specific needs and the programs you use.

## References

[^1]:
    Screen reader. Wikipedia. Online. 2024. [Accessed 31 December 2024]. Available from: <https://en.wikipedia.org/w/index.php?title=Screen_reader&oldid=1262194354>
    A screen reader is a form of assistive technology (AT) that renders text and image content as speech or braille output. Screen readers are essential to people who are blind, and are useful to people who are visually impaired, illiterate, or have a learning disability. Screen readers are software applications that attempt to convey what people with normal eyesight see on a display to their users via non-visual means, like text-to-speech, sound icons, or a braille device. They do this by applying a wide variety of techniques that include, for example, interacting with dedicated accessibility APIs, using various operating system features (like inter-process communication and querying user interface properties), and employing hooking techniques. Microsoft Windows operating systems have included the Microsoft Narrator screen reader since Windows 2000, though separate products such as Freedom Scientific’s commercially available JAWS screen reader and ZoomText screen magnifier and the free and open source screen reader NVDA by NV Access are more popular for that operating system. Apple Inc.’s macOS, iOS, and tvOS include VoiceOver as a built-in screen reader, while Google’s Android provides the Talkback screen reader and its ChromeOS can use ChromeVox. Similarly, Android-based devices from Amazon provide the VoiceView screen reader.  There are also free and open source screen readers for Linux and Unix-like systems, such as Speakup and Orca.
    Page Version ID: 1262194354
    [^2]: Results of the survey “Braille Display Usage” – livingbraille.eu. Online. 27 April 2024. [Accessed 30 December 2024]. Available from: <https://www.livingbraille.eu/results-of-the-survey-braille-display-usage/>
    [^3]: Live Regions are Not Displayed in Braille · Issue #7756 · nvaccess/nvda. Online. [Accessed 31 December 2024]. Available from: <https://github.com/nvaccess/nvda/issues/7756>
    When live region information on a web page changes, the new information is only spoken through speech. The Braille display does not update to show this information...
    [^4]: BRLTTY - Details. Online. [Accessed 1 January 2025]. Available from: <https://brltty.app/details.html>
    [^5]: BRLTTY - Official Home. Online. [Accessed 1 January 2025]. Available from: <https://brltty.app/>
    [^6]: liblouis/liblouis. Online. C. 30 December 2024. liblouis. [Accessed 1 January 2025]. Available from: <https://github.com/liblouis/liblouis>
    Open-source braille translator and back-translator.
    [^7]: DEVINPRATER. By the Blind, For the Blind. Online. 22 June 2022. [Accessed 1 January 2025]. Available from: <https://write.as/devinprater/by-the-blind-for-the-blind>
    Why tools made by the blind, are the best for the blind.  Introduction  For the past few hundred years, blind people have been creating a...
    [^8]: JAWS® – Freedom Scientific. Online. [Accessed 2 January 2025]. Available from: <https://www.freedomscientific.com/products/software/jaws/>
    [^9]: Technical Support: What Braille displays does JAWS support and are any unsupported displays emulated? Online. [Accessed 2 January 2025]. Available from: <https://support.freedomscientific.com/support/technicalsupport/bulletin/946>
    [^10]: Narrator braille driver still conflicts with all other braille drivers in Windows 11, but there is hope. Online. [Accessed 4 January 2025]. Available from: <https://jfw.groups.io/g/main/topic/narrator_braille_driver_still/86297169>
    [^11]: Accessing Math Content with JAWS and Fusion – Freedom Scientific. Online. [Accessed 4 January 2025]. Available from: <https://www.freedomscientific.com/training/teachers/accessing-math-content-with-jaws-and-fusion/>
    [^12]: NSOIFFER. NSoiffer/MathCAT. Online. Rust. 4 January 2025. [Accessed 4 January 2025]. Available from: <https://github.com/NSoiffer/MathCAT>
    MathCAT: Math Capable Assistive Technology for generating speech, braille, and navigation.
    [^13]: MathCAT: Math Capable Assistive Technology. Online. [Accessed 4 January 2025]. Available from: <https://nsoiffer.github.io/MathCAT/>
    MathCAT: Math Capable Assistive Technology for generating speech, braille, and navigation.
    [^14]: MAZRUI, Jamal. jamalmazrui/JAWS_Scripts. Online. 7 May 2023. [Accessed 1 January 2025]. Available from: <https://github.com/jamalmazrui/JAWS_Scripts>
    Scripts for the JAWS screen reader designed to enhance productivity in various Windows applications
    [^15]: WebAIM: Screen Reader User Survey #9 Results. Online. [Accessed 4 January 2025]. Available from: <https://webaim.org/projects/screenreadersurvey9/>
    [^16]: WebAIM: Screen Reader User Survey #10 Results. Online. [Accessed 4 January 2025]. Available from: <https://webaim.org/projects/screenreadersurvey10/>
    [^17]: CLAUSE, André-Abush. AAClause/BrailleExtender. Online. Python. 25 December 2024. [Accessed 4 January 2025]. Available from: <https://github.com/AAClause/BrailleExtender>
    NVDA add-on that improves braille support
    [^18]: NSOIFFER. NSoiffer/MathCATForPython. Online. Python. 23 December 2024. [Accessed 4 January 2025]. Available from: <https://github.com/NSoiffer/MathCATForPython>
    A Python Interface and NVDA plugin to MathCAT
    [^19]: USB and Bluetooth braille displays supported by macOS. Online. [Accessed 4 January 2025]. Available from: <https://support.apple.com/en-gb/guide/voiceover/cpvobrailledisplays/mac>
    macOS supports a wide range of USB and Bluetooth braille displays for use with your Mac.

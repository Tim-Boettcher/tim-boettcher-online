+++
title = "Warum Grok besonders gefährlich sein könnte"
date = 2023-11-19
template = "page.html"

[taxonomies]
tags = ["Elon Musk", "Grok", "künstliche Intelligenz", "Meinung", "X"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
isso = true
social_media_card = "img/social_cards/de_blog_why_grok_may_be_particularly_dangerous.jpg"
+++

[Trotz seiner eigenen Warnungen](https://www.nytimes.com/2023/03/29/technology/ai-artificial-intelligence-musk-risks.html) vor der Gefahr, die künstliche Intelligenz für die Gesellschaft und Zivilisation darstellen könnte, hat Elon Musk kürzlich [Grok](https://grok.x.ai/) vorgestellt. Hier ist der Grund, warum ich glaube, dass dieses generative KI-Modell besonders gefährlich sein könnte.<!-- mehr -->

## Inhaltsverzeichnis

<!-- toc -->

## Die Grundlagen von großen Sprachmodellen

*Große Sprachmodelle (large language models, LLMs)* sind die Variante der *künstlichen Intelligenz (KI)*, die in letzter Zeit die meiste Aufmerksamkeit auf sich gezogen haben. Egal ob [ChatGPT](https://openai.com/chatgpt) für die Generierung von Text oder [Midjourney](https://www.midjourney.com/home?callbackUrl=%2Fexplore) für KI-generierte Bilder, basieren diese KI-Tools alle auf der Verarbeitung vom Benutzer gestellten Anfragen und der Generierung eines Ergebnisses als Antwort auf diese, wobei sie den Benutzer in eine Konversation verwickeln und Folgefragen stellen, falls eine Aufforderung unklar war. Um dies zu ermöglichen, müssen die Modelle mit Daten trainiert werden - idealerweise mit großen Mengen unterschiedlicher Daten, mehr als ein Team von Datenwissenschaftlern selbst erzeugen könnte. Natürlich ist die Verwendung von Inhalten aus dem Internet eine gute Quelle für große Mengen unterschiedlicher Daten, aber dieser Ansatz birgt auch Risiken.

## Vergiftung der Trainingsdaten

Wenn ein Modell mit Daten aus einer öffentlichen Quelle wie dem Internet trainiert wird, enthalten diese Daten verschiedene Vorurteile: Rassismus, Antisemitismus, Homophobie, Behindertenfeindlichkeit, Verschwörungstheorien und andere fragwürdige Ansichten sickern in das große Sprachmodell ein und beeinflussen, wie es auf eine Anfrage reagiert. Dieses Problem tritt nicht nur bei großen Sprachmodellen auf - es ist ein Problem, mit dem alle Algorithmen, die auf *maschinellem Lernen* basieren, zu kämpfen haben. Da OpenAI nicht öffentlich erklärt, wie es versucht, die Trainingsdaten zu bereinigen, ist es schwer zu beurteilen, welche Maßnahmen ergriffen werden, um dieses Problem in den Griff zu bekommen. Wir wissen jedoch, dass OpenAI irgendwann die Daten auffrischen muss, die es zum Trainieren von ChatGPT verwendet, damit das LLM relevante Ereignisse in jüngerer Vergangenheit "kennt". Dies führt zu einer neuen Herausforderung.

Anders als früher sind sich viele Menschen der Existenz von LLMs wie ChatGPT bewusst. Dies bedeutet, dass einige Personen und Organisationen aktiv versuchen werden, die Trainingsdaten zu manipulieren, um das Verhalten eines LLMs auf subtile Weise zu verändern. Dies wird als *Datenvergiftung* bezeichnet. Ein Modell, das auf Übersetzungen spezialisiert ist, kann zum Beispiel korrekte Übersetzungen eines Textes erstellen -- bis es auf bestimmte Schlüsselwörter in der Eingabe stößt. Dann beginnt es, zusätzliche oder andere Wörter einzufügen, um die Bedeutung des Textes zu verändern und so die Ausgabe faktisch in Propaganda zu verwandeln. Etwas in dieser Art ist möglicherweise passiert, als [Instagram plötzlich anfing, das Wort "Terrorist" in einige Übersetzungen von Texten einzufügen, die das Wort "Palästinenser" enthielten](https://www.404media.co/instagram-palestinian-arabic-translation-terrorists-ai/). Diese Panne war wahrscheinlich ein unbeabsichtigter Nebeneffekt und keine gezielte Vergiftung des Übersetzungsmodells, sie veranschaulicht jedoch die potenziellen Risiken von vergifteten LLMs und [laut diesem Aufsatz](https://arxiv.org/abs/2209.15259) ist es mathematisch unmöglich, die Vergiftung eines großen LLM-Trainingsdatensatzes zu verhindern. Das bedeutet im Umkehrschluss, dass zumindest *einige* Fälle von LLM-Vergiftung auftreten werden, wenn es mit neuen Daten trainiert wird. Siehe [^1] für einen Aufsatz über die Vergiftung von ChatGPT und [^2] für Untersuchungen, die *virtuelle Prompt-Injektion (VPI)* demonstrieren, welche ähnliche Auswirkungen haben kann. Es ist nicht überraschend, dass die OWASP sowohl Prompt Injection (LLM01) als auch Data Poisoning (LLM03) in ihren [Top Ten der LLM-Schwachstellen](https://owasp.org/www-project-top-10-for-large-language-model-applications/#) aufführt.

## Was ist das Problem mit Grok?

xAI bewirbt Grok als Alternative zu ChatGPT, aber sie sind etwas spät dran. Daher versuchen sie, den einzigen Vorteil zu nutzen, den sie wahrscheinlich haben: Elon Musk, der Gründer von xAI, ist auch der Eigentümer von X (früher bekannt als Twitter) und Grok wird anscheinend [Live-Zugriff auf Beiträge und Inhalte von X](https://www.tomsguide.com/news/elon-musk-launches-grok-ai-chatbot-with-live-access-to-x-data-heres-why-thats-not-the-best-idea) erhalten. Es ist zwar unklar, wie genau die Inhalte von X in Grok einfließen werden, aber man kann davon ausgehen, dass Grok zumindest mit einigen dieser Daten kontinuierlich trainiert wird. Dies ist besorgniserregend, weil X [systematisch dabei versagt, digitalen Hass zu bekämpfen](https://counterhate.com/research/twitter-x-continues-to-host-posts-reported-for-extreme-hate-speech/), wie ein CCDH-Bericht feststellt. Nach Angaben der Europäischen Kommission [hat X deutlich weniger Content-Moderatoren als andere große Unternehmen](https://www.reuters. com/technology/musks-x-has-fraction-rivals-content-moderators-eu-says-2023-11-10/), sodass sich diese Situation wahrscheinlich nicht verbessern wird; IBM hat kürzlich [Werbeanzeigen auf X pausiert](https://edition.cnn.com/2023/11/16/tech/ibm-to-pause-ad-spending-on-x/index.html), nachdem die Anzeigen neben Pro-Nazi-Inhalten erschienen. Die Plattform zieht das Hinzufügen von [kollektiven Anmerkungen](https://help.twitter.com/de/using-x/community-notes), d.h. durch Freiwillige erstellte Faktenchecks, dem Entfernen von Beiträgen mit schädlichem und/oder falschem Inhalt vor. Wie genau ein LLM wie Grok solche kollektiven Anmerkungen analysieren könnte, ist unklar, was die Befürchtung befeuert, dass Grok noch mehr [Probleme mit der Wahrhaftigkeit](https://arxiv.org/abs/2310.05189) haben könnte, als es die bestehenden LLMs bereits tun. Ebenso könnten sowohl Data Poisoning als auch VPI aufgrund der bereits erwähnten Moderationsprobleme und der Einbindung von Live-Daten besonders leicht sein: Grundsätzlich kann ein LLM nur so sicher sein wie die Daten, mit denen es trainiert wird und daher könnte Grok besonders unsicher sein.   

## Weiterführende Quellen

[^1]: [Poisoning ChatGPT](https://softwarecrisis.dev/letters/the-poisoning-of-chatgpt/)

[^2]: [Backdooring Instruction-Tuned Large Language Models with Virtual Prompt Injection](https://poison-llm.github.io/)

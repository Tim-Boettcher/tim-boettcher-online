+++
title = "On reporting accessibility issues"
date = "2024-03-25"
template = "page.html"
description = "TalkBack has some serious issues with braille display support. How hard can reporting these issues be? Very hard, it turns out."

[taxonomies]
tags = ["accessibility", "bugs", "support"]

[extra]
isso = true
toc_ignore_pattern = "^Table of contents$"
social_media_card = "img/social_cards/blog_on_reporting_accessibility_issues.jpg"
+++

TalkBack has some serious issues with braille display support. How hard can reporting these issues be? Very hard, it turns out.<!-- more -->

**Please note**: While this post is specifically about reporting issues with TalkBack's braille display support, this is just an example. I have struggled with reporting accessibility issues to Apple, Tinder, Signal and many other companies, large and small.

## Table of contents

<!-- toc -->

## What is the issue?

[TalkBack](https://support.google.com/accessibility/android/answer/6283677?hl=en-GB&sjid=5151112786706692734-EU) has, thankfully, gained [native support for most braille displays](https://www.androidcentral.com/apps-software/android-13-is-picking-up-native-support-for-most-braille-displays) since Android 13. However, that does not mean Android devices are perfectly accessible when using braille displays. They often report less information than gets spoken by the screen reader's speech output---a sadly very common trend across screen readers and operating systems. But while [working on my own Android app](https://github.com/Almost-Senseless-Tech/Voskle-Live-Transcribe) I discovered a more severe problem: Not all text fields get recgonized by TalkBack as text fields, even when using speech. This has the side-effect that one cannot enter any text into the text field using the braille keyboard and the cursor position does not get reported correctly to braille displays. Another issue: Progress indicators get spoken by the TalkBack speech engine (it says "x percent, status bar"), but appear as a completely blank line on braille displays. Last but not least notifications and live area updates get announced if you use speech but do not appear on the braille display as a flash notification the way I would expect them to.

## Trying to report it

### Google Public Bug Tracker


I [created an issue for the text field problem](https://issuetracker.google.com/issues/328117822) on the Google Public Bug Tracker, but thus far there has been no activity beyond assignment on it. Given this is a serious accessibility issue, I really do not understand why nothing at all appears to happen on the matter. Granted, it is not clear whether this is an issue with JetPack Compose, Android 13, TalkBack 14 or One UI (Samsung's Android skin) but the communication, or rather the lack thereof, is still unfortunate.

### Galaxy support

As mentioned above, the issue may well be originating in One UI, so I also contacted the Galaxy support. This went reasonably well, though the contact form they submitted to me at the end to obtain contact details from me is inaccessible---or at least the checkbox that I have read their privacy policy is. So I was forced to proceed without providing contact information; it is hard to know what, if anything, has happened with my report.

### Google disability support

Since the aforementioned steps did not yield any tangible result, I tried to [contact the Google disability support](https://support.google.com/accessibility/answer/7641084?hl=en). Their contact form for email support is utterly inaccessible on iOS and Windows using JAWS as well as hard to use on Windows with NVDA. When selecting an issue you are reaching out about, something strange happens to the form, hiding a lot of the form fields so it becomes difficult, if not impossible, to fill out the form and submit it.

### Am I living in a satire show?

Apparently.

## Why it matters

Persons with disabilities already have to use more energy for common, seemingly simple tasks than others have to. An expression we often use is that we have fewer spoons:

> The spoon theory is a metaphor describing the amount of physical and/or mental energy that a person has available for daily activities and tasks, and how it can become limited.
> 
> Source: [Wikipeida](https://en.wikipedia.org/wiki/Spoon_theory)

Accessibility issues of any kind force us to use more of our precious spoons, or energy units, to find an alternative approach that avoids the issue---provided such an alternative approach even exists. The fact that reporting accessibility issues is so difficult just adds insult to injury:

- We may encounter accessibility issues while trying to report an accessibility issue.
- We often do not get any feedback whether our report has been viewed and whether anything at all is getting done about it.
- Any given group with a certain disability is inevitably going to be a small fraction of users, so companies often wonder whether implementing the accessibility improvements is worth it.
- Due to the above lack of feedback, we often have to report the issue several times---every time a new update gets released that does not address the issue.
- Even if it gets addressed, the issue may persist for months before getting fixed; during that time, persons with disabilities have to continue to struggle with the issue.

Sometimes I get tired of reporting issues to companies, especially corporate behemoths like Microsoft, Google, Apple, Amazon, Meta. But if I do not report them, who will?

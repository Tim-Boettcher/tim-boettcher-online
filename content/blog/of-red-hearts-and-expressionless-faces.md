+++
title = "Of red hearts and expressionless faces"
date = 2023-09-24
updated = 2023-11-18
template = "page.html"
description = "Emoji spread everywhere, but for a deafblind person they are far from intuitive. I attempt to illustrate why."

[taxonomies]
tags = ["communication", "emoji", "opinion"]

[extra]
toc_ignore_pattern = "^Table of contents$"
isso = true
social_media_card = "img/social_cards/blog_of_red_hearts_and_expressionless_faces.jpg"
+++

## Table of contents
<!-- toc -->

## What's the matter? :raised_eyebrow:

Emoji are sprinkled all over our texts, annotating the output of hip command line interfaces and increasingly [coming up in court cases](https://edition.cnn.com/2019/07/08/tech/emoji-law/index.html). Although they are, from a computer's perspective, just another set of [unicode characters](https://home.unicode.org/), their role in text-based communication is unique. They add context to a statement---similar to a facial expression or the tone of a voice.

For deafblind people like me, interpreting emoji correctly quickly becomes a challenge. Sure, text-based smileys aren't a new concept: The [first instance of the smiley :-)](https://time.com/3341244/emoticon-birthday/) was posted in 1982. But such basic smileys have a specific shape which one can draw, visualise in one's mind and reason about. This makes them easier to remember and while there is room for misunderstandings, emoji are more diverse and complex, therefore offering far more ways to (mis)interpret them.

## Such a mess! :skull:

Human perception of facial expression already [differs across cultures](https://www.apa.org/news/press/releases/2011/09/facial-expressions). Empirically, I think the same is true for emoji---their usage and interpretion can differ wildly, often depending on what kind of platforms the respective person uses. One example is the skull emoji, which gets used by some to express (self-)deprecating laughter. For a blind person like me, this was not intuitive to understand: When I visualise a skull, I first think of the domed top and gaping eye sockets; for sighted people, the most noticeable feature is apparently the wide, lip-less "grin" of skulls.

This grin is the reason why skulls often get used in a laughing context, but it took me a while to comprehend this.

## Too much love :heart:

Another issue is the mental and emotional association we form with different symbols. From a young age, I got told the red heart symbolises love and romance. Recently I noticed several people using the red heart very liberally on my messages, almost like read receipts. At first, I thought they must have serious romantic interest in me; then it dawned on me that they were not using the red heart in the context of love, but of "like".

The interface of social media platforms is text-based and/or speech based for blind people like myself. This means that the button that expresses "I like this post" is often labelled with "Favourite" or "Like". But for sighted people, this button often is a red heart. They see this symbol every day in the context of "I like this" and develop a different association with it than I do.

## Angry expressionless faces :expressionless:

One thing I notice a lot, both in person and via emoji, is that people tend to interpret neutral, blank or expressionless faces as negative. When I am deep in thought, I do not consciously focus on maintaining an expression. Apparently, the expression my face settles in appears sad or grim to many. This is probably what people call a "resting bitch face".

Similarly, the expressionless and neutral emoji frequently get used by people to express that they are angry. This, too, is something I had to learn over time.

## Conclusions? :bulb:

Blind users get a descriptive text spoken or displayed to them instead of the emoji and that text may or may not align with how the sender intended the emoji. In turn, this may result in misunderstandings since the blind person may not know how to interpret the emoji correctly. If in doubt, it may be a good idea to explain how one intends a particular emoji or to ask about the intention behind it, respectively; otherwise, communication issues are hard to avoid.

+++
title = "Many CAPTCHAs are discriminatory"
date = "2024-03-24"
template = "page.html"
description = "Completely Automated Public Turing tests to tell Computers and Humans Apart (CAPTCHAs) are the digital equivalent of bouncers, lording over who shall and who shall not pass to whatever it is the CAPTCHA guards. Unfortunately, the assumptions many CAPTCHAs make about what it means to be human are deeply discriminatory and the challenges themselves ineffectual."

[taxonomies]
tags = ["ableism", "accessibility", "bots", "CAPTCHA", "opinion", "security", "spam"]

[extra]
toc_ignore_pattern = "^Table of contents$"
isso = true
social_media_card = "img/social_cards/blog_many_captchas_are_discriminatory.jpg"
+++

Completely Automated Public Turing tests to tell Computers and Humans Apart (CAPTCHAs) are the digital equivalent of bouncers, lording over who shall and who shall not pass to whatever it is the CAPTCHA guards. Unfortunately, the assumptions many CAPTCHAs make about what it means to be human are deeply discriminatory and the challenges themselves ineffectual.<!-- more -->

## Table of contents

<!-- toc -->

## The mission

Everything is going digital, including [our payments](https://www.mckinsey.com/industries/financial-services/our-insights/banking-matters/new-trends-in-us-consumer-digital-payments), [our dating](https://www.forbes.com/health/dating/dating-statistics/), [shopping for goods](https://www.statista.com/topics/871/online-shopping/#topicOverview) and [even our governments](https://www.worldbank.org/en/topic/digitaldevelopment/brief/digital-government-for-development). This implies that very sensitive data gets submitted and processed online---literally life-changing actions can get performed mostly or completely online these days. At the same time bad actors are trying to capitalize on the opportunities of digital fraud and theft. They attempt to have automated scripts, usually called bots, pose as legitimate users and make requests to a service, such as logging in, submitting posts to social media or swiping in a dating app like Tinder.

Ensuring that only requests by legitimate users and authorized bots get processed is vital for several reasons:

- Bots could easily perform actions like purchasing tickets to popular concerts or cracking passwords of users by submitting several requests per second, with harmful side-effects.
- The content quality of platforms deteriorate if they get overrun by bot and spam accounts.
- The service itself suffers as every bot-generated request it needs to process costs resources---leading to denial of service in the worst case.

CAPTCHAs are one way of tackling the issue. They present the user with a challenge which only a human is supposed to be able to solve. These challenges may be text-based, image-based and/or audio-based; they test pattern recognition, logic or knowledge to verify that the user is legitimate, thus mitigating the aforementioned problems with bots. Additionally, companies like Google can use CAPTCHAs to [train their image recognition AI](https://www.techradar.com/news/captcha-if-you-can-how-youve-been-training-ai-for-years-without-realising-it). A win-win situation... right?

## The problems

Unfortunately, CAPTCHAs create as many problems as they solve, at the least.

### User experience

No one wants to spend time solving some puzzle to prove one's humanity---and a lot of time at that: According to [a Cloudflare blog post](https://blog.cloudflare.com/introducing-cryptographic-attestation-of-personhood/), humanity wastes about **500 years** proving that it is human---per day, every day! This toll is only getting heavier as bots get better at solving CAPTCHAs and the providers of the challenges [make them harder](https://www.technologyreview.com/2023/10/24/1081139/captchas-ai-websites-computing/) as a result.

### Waning effectiveness

Talking about bots getting better at solving CAPTCHAs: [An empirical study of modern CAPTCHAs](https://arxiv.org/abs/2307.12108) found that bots are much better at solving commonly used CAPTCHAs than humans and they need significantly less time to solve the challenges as well. But even if one managed to create a challenge that bots cannot solve in an adequate amount of time or with adequate accuracy, spammers can bypass this issue by [paying low-cost workers to solve CAPTCHAs](https://www.technologyreview.com/2010/08/11/201606/how-spammers-use-low-cost-labor-to-solve-captchas/).

### Ableism

If all that is not depressing enough yet, there is a third factor to consider: CAPTCHAs make assumptions about what it means to be a human. Humans can see and/or hear; they are able to recognize patterns; they are capable of logic and have a certain level of base knowledge; if they use a computer mouse, they move it in a certain fashion; and they solve a given task within a certain time frame. If you differ from this definition, even if you manage to solve the CAPTCHA itself, you are often flagged as a bot---even though there are many humans, in particular persons with disabilities, who differ from the common assumptions made by CAPTCHA providers.

For people like me, CAPTCHAs turn into digital bouncers who either turn us away outright or make a task tedious enough that we simply give up on completing it. They force us to call for help or use a browser extension to solve CAPTCHAs for us---I hope you appreciate the irony.

## Solutions

Thankfully, some challenge providers have started to act on the problems mentioned above, aiming to reduce the amount of work a user has to do:

### Testing the browser

Rather than forcing the user to verify that they are human, projects like [mCaptcha](https://mcaptcha.org/) take advantage of the fact that browsers have certain limitations, which means that computing complex maths takes a legitimate browser a while---so if the challenge gets solved too fast, it is safe to assume that a bot that is not runing in a legitimate browser is attempting to submit the form or request. At the very least, such challenges result in rate limiting, meaning that bots' ability to submit requests quickly gets hampered.

### IP checks

Another method of mitigating the issues associated with bots is examinig the IP address used to submit a request to see whether it is already listed as an IP address associated with spam and (possibly temporarily) blocking IP addresses if they fail to perform an action like signing in repeatedly. A more aggressive step would be examining the [IP address reputation](https://talosintelligence.com/reputation_center/) of the address from which the request originates, banning any addresses that do not meet a certain reputation. This can have undesirable side effects as well, though, punishing people from regions commonly associated with spam simply because their IP address happens to be in an IP range that got flagged for shady activity. But at least performing IP checks does not require the user to take any actions.

### Requesting user identification

Critical services may also request that the user provides official documents to verify their identity, like their driver's license. This has obvious privacy implications and is not an option for every service, but it is an effective means of mitigating spam and bot activity.

## Wrapping up

Modern CAPTCHAs are failing their purpose, imposing heavy burdens on legitimate users while not providing adequate protection against bots anymore. In particular, they are discriminatory by defining standards for what it means to be a human. Alternative approaches should get adopted.

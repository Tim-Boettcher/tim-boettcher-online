+++
title = "Sollte künstliche Intelligenz neutral ausgerichtet sein?"
date = 2023-10-05
updated = 2023-11-18
description = "Mit der rasanten Ausbreitung der generativen künstlichen Intelligenz (KI) stellt sich eine grundlegende Frage: Wer darf über die Ausrichtung der KI entscheiden? In diesem Beitrag werde ich die zugrundeliegenden Probleme untersuchen, erläutern, was Ausrichtung in diesem Zusammenhang bedeutet und welche Auswirkungen verschiedene Lösungsvorschläge haben."
template = "page.html"

[taxonomies]
tags = ["Ethik", "künstliche Intelligenz", "Meinung"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
isso = true
social_media_card = "img/social_cards/de_blog_should_ai_be_unaligned.jpg"
+++

Mit der rasanten Ausbreitung von [generativer künstlicher Intelligenz (KI)](https://en.wikipedia.org/wiki/Generative_artificial_intelligence) stellt sich eine grundlegende Frage: Wer darf über die Ausrichtung der KI entscheiden? In diesem Beitrag werde ich die zugrundeliegenden Probleme untersuchen, erläutern, was Ausrichtung in diesem Zusammenhang bedeutet und welche Auswirkungen verschiedene Lösungsvorschläge haben.<!-- mehr -->

## Inhaltsverzeichnis

<!-- toc -->

## Über Vorurteile

Es gibt ein Sprichwort, das besagt, dass "Technologie moralisch neutral" ist: Ein Messer kann sowohl zum Brotschneiden als auch zum Töten verwendet werden; ein Drucker druckt Todesdrohungen ebenso bereitwillig wie ein Liebesgedicht; und einer Instant-Messaging-App ist es egal, ob sie zur Vorbereitung eines Terroranschlags verwendet wird.

KI ist im Grunde nicht anders. Noch hat sie keinen moralischen Kompass und ist sich nicht bewusst, was sie verarbeitet oder erzeugt. Aber wie wir in den letzten Jahren gelernt haben, ist jede KI aufgrund der Daten, mit denen sie trainiert wird, von vornherein voreingenommen. Gesichtserkennungssysteme haben eine [rassistische Voreingenommenheit](https://www.washingtonpost.com/technology/2019/12/19/federal-study-confirms-racial-bias-many-facial-recognition-systems-casts-doubt-their-expanding-use/); [eine Studie befand](https://arxiv.org/pdf/2304.11111v1.pdf), dass Large Language Models (LLMs) wie ChatGPT voreingenommener (z. B. rassistischer oder behindertenfeindlich) auf Anfragen reagieren, wenn ihnen zuvor Angst eingejagt wurden; und eine andere Studie entdeckte, dass [ChatGPT die Labour Party bevorzugt](https://uk.news.yahoo.com/chatgpt-favours-labour-party-study-121345343.html?guccounter=1&guce_referrer=aHR0cHM6Ly9kdWNrZHVja2dvLmNvbS8&guce_referrer_sig=AQAAALJe-DjgYfZRLe0zyG_AVYGuWKk6wG8vg7almzb7v7-HDBow8wmPB6mWWGjczCxj22awJOsaID6HWxejC8fdFXN794XaC8B1IVgD4nMRYrRgt2yv8d12jufd0Ab232x0McQBpCflCk46FY1E7525nqp6XcJ_U0-t5X8OY-TDgew4).

Abgesehen von diesen grundlegenden, datenbasierten Vorurteilen verweigert ChatGPT auch die Beantwortung von Fragen, die seiner Meinung nach gegen die [Nutzungsbedingungen von OpenAI](https://openai.com/policies/terms-of-use) verstoßen -- andere generative KIs, wie Metas quelloffenes LLM [Llama](https://huggingface.co/meta-llama), zeigen ebenfalls Warnungen an oder verweigern die Beantwortung einiger Anfragen. Dies führt zu einer ethischen Frage: Wer darf entscheiden, welche Anfragen als fragwürdig angesehen werden und wie LLMs auf sie reagieren sollten?

## Es ist nicht Sache des Benutzers

Unabhängig davon, welchen Ansatz man zu dieser Frage wählt, werden die Benutzer nicht das Sagen haben: Die [Hardware-Empfehlungen](https://www.pugetsystems.com/solutions/scientific-computing-workstations/machine-learning-ai/hardware-recommendations/) für eine KI-Trainingsplattform sind überwältigend und sehr kostspielig -- und das setzt voraus, dass man überhaupt das nötige Know-how hat, um zu versuchen, seine eigene KI zu trainieren. Das Selbsthosten einer vortrainierten Open-Source-KI ist mit Projekten wie [OwnAI](https://ownai.org/) möglich -- aber es erfordert immer noch technisches Wissen und zumindest ein gewisses Verständnis von KI.

Das ist natürlich nicht ungewöhnlich. Obwohl es technisch möglich ist, seine eigene Cloud-Speicherlösung zu hosten, ziehen es die meisten Menschen vor, Google Drive, OneDrive oder iCloud zu nutzen, trotz der möglichen Auswirkungen auf die Privatsphäre. Doch gleichzeitig ist die Situation auch anders: Die Entscheidung, eine Softwarelösung selbst zu hosten und manuell zu konfigurieren, war früher vor allem eine Frage der Flexibilität, Unabhängigkeit und Privatsphäre. Seit LLMs verwendet werden, um [den nächsten Urlaub zu planen](https://www.techradar.com/features/i-asked-bing-to-plan-my-vacation-and-i-might-as-well-just-stay-home) (wenn auch mit gemischten Ergebnissen) oder um [Suchmaschinen wie Google zu ersetzen](https://www.insider.com/chatgpt-as-search-engine-replace-google-experiment-2023-8) (wiederum mit gemischten Ergebnissen), kann die Voreingenommenheit einer bestimmten KI sehr wohl Ihre Entscheidungen und Ihr Weltbild beeinflussen. Wer entscheidet also über diese Voreingenommenheit, wenn es nicht der einzelne Nutzer ist?

## Der/die Entwickler als "Moralpolizei"

Natürlich haben die Entwickler, die die KI trainieren, die beste Chance, ihre Vorurteile zu beeinflussen, indem sie die verwendeten Trainingsdaten anpassen. Darüber hinaus können sie der KI beibringen, auf bestimmte Anfragen nicht oder auf eine bestimmte Art und Weise zu reagieren; in diesem Szenario schaffen sie eine Voreingenommenheit, kein unbeabsichtigtes Verhalten ist sondern ein Feature. Ich werde diese Art der Verzerrung im Folgenden *Ausrichtung* nennen. Wie bereits erwähnt, ist die Ausrichtung in die Trainingsdaten für GPT und Llama integriert, was bedeutet, dass die Entwickler der KI entscheiden, welche Anfragen beantwortet werden sollen und welche nicht. Das ist nicht unbedingt neu: Die dominierenden Social-Media-Plattformen haben alle ihren Sitz in den USA und ihre Richtlinien zur Inhaltsmoderation basieren daher auf typisch US-amerikanischen Werten. Wir haben bereits die Erfahrung gemacht, dass einige wenige, praktisch allgegenwärtige Unternehmen entscheiden, welche Art von Inhalten und Verhalten akzeptabel ist: Die Tatsache, dass Facebook nicht gegen Inhalte vorging, die zur Gewalt gegen die Rohingya aufriefen (und sogar [solche Inhalte weiter verbreitete](https://www.amnesty.org/en/latest/news/2022/09/myanmar-facebooks-systems-promoted-violence-against-rohingya-meta-owes-reparations-new-report/)), trug zu dem anschließenden Völkermord bei. Gleichzeitig behindern die strengen Regeln gegen Nacktheit auf den meisten Social-Media-Plattformen [Informationskampagnen über Brustkrebs](https://transparency.fb.com/de-de/oversight/oversight-board-cases/breast-cancer-symptoms-nudity/). Dies erklärt, warum viele nicht wollen, dass die Ausrichtung der KI zentralisiert von den KI-Entwicklern entschieden wird.

## Dienstleister können die Ausrichtung anpassen

Als alternativen Ansatz versuchen Entwickler wie [Eric Hartford](https://huggingface.co/ehartford) (der [für sein KI-Projekt von Andreessen Horowitz finanziert wird](https://www.404media.co/andreessen-horowitz-funds-uncensored-ai-that-will-tell-you-how-to-kill-yourself/)), die KI von der Ausrichtung zu entkoppeln -- im Grunde wird die Ausrichtung zu einer Einstellung, die von einem Dienstleister nach seinen Präferenzen angepasst werden kann. Dieselbe "unzensierte" KI würde dann je nach den vom jeweiligen Dienstleister vorgenommenen Ausrichtungseinstellungen unterschiedlich reagieren. Dies bedeutet, dass dasselbe LLM sowohl einer Organisation für geplante Elternschaft als auch Abtreibungsgegnern dienen könnte.

Auf den ersten Blick klingt dies besser, als wenn ein zentrales Unternehmen die ethische Haltung einer KI diktieren würde. Ich befürchte jedoch, dass dieses Konzept einen bereits schwerwiegenden Echokammereffekt verfestigen würde, da die KI den Nutzer möglicherweise nie mit gegenteiligen Ansichten konfrontieren würde; eine KI macht (noch) keine Erfahrungen, die ihre Ansichten ändern könnten, sie stellt die ihr übergebenen Ausrichtungsparameter nicht in Frage. Daher ist das Potenzial für eine strenge Indoktrination oder die Aufrechterhaltung einer bereits bestehenden Weltanschauung in der Nutzerbasis einer bestimmten KI enorm. Dies ist besonders besorgniserregend, wenn KI eines Tages zur Nachhilfe für Kinder eingesetzt wird. Die bereits verhärteten Fronten zwischen verschiedenen politischen, kulturellen oder religiösen Gruppen könnten noch unflexibler werden.

Unmittelbarer ist die Befürchtung, dass unzensierte KI, die öffentlich zugänglich gemacht wird, es den Menschen erleichtert, potenziell schädliche Informationen wie Selbstmordmethoden oder den Bau von Bomben zu erfahren. Diese Informationen sind natürlich bereits im Internet verfügbar, aber die Nutzer müssen bisher aktiv danach suchen und hinterlassen dabei möglicherweise digitale Spuren.

## Unterm Strich

Es gibt keinen eindeutig richtigen oder falschen Ansatz für die Frage, ob KI unausgerichtet sein sollte -- es kommt darauf an, wie einer der beiden Ansätze umgesetzt wird. Letzten Endes brauchen wir vor allem eine solide Gesetzgebung, und ich befürchte, dass der Gesetzgeber hier kläglich hinterher hinkt.

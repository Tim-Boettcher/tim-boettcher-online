+++
title = "Why Grok may be particularly dangerous"
date = 2023-11-19
template = "page.html"

[taxonomies]
tags = ["artificial intelligence", "Elon Musk", "Grok", "opinion", "X"]

[extra]
toc_ignore_pattern = "^Table of contents$"
isso = true
social_media_card = "img/social_cards/blog_why_grok_may_be_particularly_dangerous.jpg"
+++

[In spite of his own warnings](https://www.nytimes.com/2023/03/29/technology/ai-artificial-intelligence-musk-risks.html) about the threat artificial intelligence may pose to society and civilisation, Elon Musk recently unveiled [Grok](https://grok.x.ai/). Here is why I think this generative AI model may be particularly dangerous.<!-- more -->

## Table of contents

<!-- toc -->

## The fundamentals of large language models

*Large language models (LLMs)* are the flavour of *artificial intelligence (AI)* that has captured most of our attention of late. Whether [ChatGPT](https://openai.com/chatgpt) for generating text or [Midjourney](https://www.midjourney.com/home?callbackUrl=%2Fexplore) for image generation, those AI tools all rely on  processing some user-provided prompts and generating a result in response, engaging users in a conversational manner and asking follow-up questions if a prompt was unclear. To facilitate this, the models need to get trained using data---ideally large amounts of diverse data, more than a team of data scientists could possibly generate by themselves. Naturally, using content from the internet is a good source of large amounts of diverse data, but this approach also carries risks.

## Poisoning the training data

When a model gets trained using data from a public source like the internet, this data contains various biases; racism, antisemitism, homophobia, ableism, conspiracy theories and other questionable views seep into the large language model and influence how it responds to a prompt. This problem is not unique to large language models---it is an issue all *machine learning*-based algorithms grapple with. Since OpenAI does not state publicly how it attempts to sanitise training data, it is hard to judge which measures get put in place to deal with this problem; we do know that at some point OpenAI needs to refresh the data it uses to train ChatGPT to make its LLM "aware" of relevant events that transpired more recently, though, which leads to a new challenge.

Unlike before, many people are aware of the existence of LLMs like ChatGPT which means some individuals and organisations will actively attempt to tweak the training data to alter the behaviour of a LLM in subtle ways. This is called *data poisoning*. For instance, a model specialising in translations may produce accurate translations of a text---unless it encounters specific keywords in the input, at which point it starts inserting additional or different words to alter the meaning of the text, effectively turning the output to propaganda. Something like this possibly happened when [Instagram suddenly started inserting the word "terrorist" into some translations of texts containing the word "Palestine"](https://www.404media.co/instagram-palestinian-arabic-translation-terrorists-ai/), though this glitch was likely an unintended side-effect, not a targeted poisoning. It illustrates the potential risks of poisoned LLMs, though, and [according to this essay](https://arxiv.org/abs/2209.15259) it is mathematically impossible to prevent poisoning of a large set of LLM training data. That means, by extension, that at least *some* poisoning of a LLM will occur whenever it gets trained on new data. See [^1] for an essay on the poisoning of ChatGPT specifically and [^2] for research demonstrating *virtual prompt injection (VPI)* which can have similar effects. It is not surprpising the OWASP lists both prompt injection (LLM01) and data poisoning (LLM03) in its [top ten LLM vulnerabilities](https://owasp.org/www-project-top-10-for-large-language-model-applications/#).

## What is the problem with Grok?

xAI pitches Grok against ChatGPT, but they are somewhat late to the party. Therefore, they try to leverage the one advantage they probably have: Elon Musk, the founder of xAI, is also the owner of X (formerly known as Twitter) and Grok will apparently get [live access to posts and content from X](https://www.tomsguide.com/news/elon-musk-launches-grok-ai-chatbot-with-live-access-to-x-data-heres-why-thats-not-the-best-idea). While it is unclear how exactly the content from X will get incorporated into Grok, it is safe to assume Grok will continuously get trained on at least some of that data. This is concerning because X is [systematically failing to counter digital hate](https://counterhate.com/research/twitter-x-continues-to-host-posts-reported-for-extreme-hate-speech/) a CCDH report found. According to the European Commission, [X has significantly fewer content moderators than other large corporations](https://www.reuters.com/technology/musks-x-has-fraction-rivals-content-moderators-eu-says-2023-11-10/), so this situation is unlikely to improve; IBM recently [paused advertising on X](https://edition.cnn.com/2023/11/16/tech/ibm-to-pause-ad-spending-on-x/index.html) after the ads appeared next to pro-Nazi content. The platform is prefering adding [community notes](https://help.twitter.com/en/using-x/community-notes), crowd-sourced fact-checks, over removing posts with harmful and/or incorrect content---how exactly a LLM like Grok could parse such community notes is unclear, raising concerns it could have even more [problems with factuality](https://arxiv.org/abs/2310.05189) than existing LLMs already do. Similarly, both data poisoning and VPI could be particularly easy due to the aforementioned moderation issues and the incorporation of live data: Fundamentally, a LLM can only be as safe as the data it gets trained on and therefore Grok may be particularly unsafe.   

## Further reading

[^1]: [Poisoning ChatGPT](https://softwarecrisis.dev/letters/the-poisoning-of-chatgpt/)

[^2]: [Backdooring Instruction-Tuned Large Language Models with Virtual Prompt Injection](https://poison-llm.github.io/)

+++
title = "Should artificial intelligence be unaligned?"
date = 2023-09-23
updated = 2023-11-18
description = "As generative artificial intelligence (AI) proliferates rapidly, a fundamental question arises: Who gets to decide the AI's alignment? In this post I will explore the underlying problems, what alignment means in this context and the implications of different proposed solutions."
slug = "should-ai-be-unaligned"
template = "page.html"

[taxonomies]
tags = ["artificial intelligence", "ethics", "opinion"]

[extra]
toc_ignore_pattern = "^Table of contents$"
isso = true
social_media_card = "img/social_cards/blog_should_ai_be_unaligned.jpg"
+++

As [generative artificial intelligence (AI)](https://en.wikipedia.org/wiki/Generative_artificial_intelligence) proliferates rapidly, a fundamental question arises: Who gets to decide the AI's alignment? In this post I will explore the underlying problems, what alginment means in this context and the implications of different proposed solutions.<!-- more -->

## Table of contents

<!-- toc -->

## On biases

There is an adage that "technology is fundamentally neutral": A knife can both get used to cut bread and to kill; a printer prints death threats just as readily as a love poem; and an instant messaging app does not care whether it gets used to prepare a terrorist attack.

Fundamentally, AI is no different. As of yet, it has no moral compass, nor is it aware of what it is processing or generating. But as we learned over the past years, AI is biased from the outset due to the data used to train it. Facial-recogntion systems have a [racial bias](https://www.washingtonpost.com/technology/2019/12/19/federal-study-confirms-racial-bias-many-facial-recognition-systems-casts-doubt-their-expanding-use/); [a study found](https://arxiv.org/pdf/2304.11111v1.pdf) that large language models (LLMs) such as ChatGPT respond more biased (e.g., more racist or ableist) to prompts when previously made anxious; and another study discovered that [ChatGPT favours the Labour Party](https://uk.news.yahoo.com/chatgpt-favours-labour-party-study-121345343.html?guccounter=1&guce_referrer=aHR0cHM6Ly9kdWNrZHVja2dvLmNvbS8&guce_referrer_sig=AQAAALJe-DjgYfZRLe0zyG_AVYGuWKk6wG8vg7almzb7v7-HDBow8wmPB6mWWGjczCxj22awJOsaID6HWxejC8fdFXN794XaC8B1IVgD4nMRYrRgt2yv8d12jufd0Ab232x0McQBpCflCk46FY1E7525nqp6XcJ_U0-t5X8OY-TDgew4).

Beyond these fundamental, data-based biases, ChatGPT will also refuse to answer questions that it considers to violate OpenAI's [terms of use](https://openai.com/policies/terms-of-use)---other generative AIs, such as Meta's open-source LLM [Llama](https://huggingface.co/meta-llama) will also display warnings or refuse to answer some prompts. This leads to an ethical question: Who gets to decide which prompts are deemed questionable and how LLMs should respond to them?

## It is not up to the user

Regardless of which approach to this question one takes, users will not call the shots: The [hardware recommendations](https://www.pugetsystems.com/solutions/scientific-computing-workstations/machine-learning-ai/hardware-recommendations/) for an AI training platform are staggering and very costly---and that is assuming you even have the necessary know-how to attempt training your own AI. Self-hosting a pre-trained open source AI is possible using projects like [OwnAI](https://ownai.org/)---but it still requires being tech-savvy and having at least some understanding of AI.

That is not unusual, of course. While it is technically possible to self-host your own cloud storage solution, most people prefer to use Google Drive, OneDrive or iCloud, in spite of the potential privacy ramifications. Yet the situation is different, too: The decision to self-host and configure a software solution manually largely used to be a matter of flexibility, independence and privacy. Since LLMs get used to [plan the next vacation](https://www.techradar.com/features/i-asked-bing-to-plan-my-vacation-and-i-might-as-well-just-stay-home) (albeit with mixed results) or to [replace search engines like Google](https://www.insider.com/chatgpt-as-search-engine-replace-google-experiment-2023-8) (again, with mixed results for now), the bias of a particular AI may very well influence your decisions and world view. So who gets to decide that bias if it is not the individual user?

## The developer(s) as "morality police"

Of course, the developers who train the AI have the best shot at influencing its bias, by adjusting the training data used. Beyond that, they can additionally teach the AI not to respond to certain prompts, or to respond to such prompts in a certain way; in this scenario, they are creating a bias that is a feature, not an unintentional behaviour. I will call this kind of bias *alignment* from here on out. As mentioned above, the alignment is baked into the training data for GPT and Llama, effectively meaning the developers of the AI decide which prompts should get answered and which should not. This is not necessarily new: The dominant social media platforms are all based in the US and their content moderation policies therefore rooted in values typically held in the US. We have already experienced the fallout of a few virtually omnipresent corporations deciding what kind of content and behaviour is acceptable: Facebook not taking action on content that incited violence against the Rohingya (and, in fact, [promoting such content](https://www.amnesty.org/en/latest/news/2022/09/myanmar-facebooks-systems-promoted-violence-against-rohingya-meta-owes-reparations-new-report/)) contributed to the subsequent genocide. Meanwhile, the stringent rules against nudity on most social media platforms [hamper information campaigns about breast cancer](https://transparency.fb.com/en-us/oversight/oversight-board-cases/breast-cancer-symptoms-nudity/). This explains why many do not want AI alignment to be centralised and decided by the AI development corporations.

## Allowing service providers to adjust alignment

As an alternative approach, developers like [Eric Hartford](https://huggingface.co/ehartford) (who [receives funding from Andreessen Horowitz for his AI project](https://www.404media.co/andreessen-horowitz-funds-uncensored-ai-that-will-tell-you-how-to-kill-yourself/)) attempt to decouple the AI from the alignment---basically turning alignment into a setting that can get adjusted by a service provider according to their preferences. The same "uncensored" AI would then respond differently based on the alignment settings made by the respective service provider, meaning that the same LLM could serve a planned parenthood organisation and "pro life" advocates.

At first glance, this sounds superior to having a central corporation dictate the ethical stance of an AI. However I fear that this concept would perpetuate an already severe echo chamber effect, since the AI would potentially never confront the user with opposing views; an AI does not (yet) make experiences that could alter its views, it does not question the alignment parameters passed to it. Therefore, the potential for stringent indoctrination or perpetuation of an already existing world view in a particular AI's user base is huge. This is especially worrying should AI someday be used to tutor children. The already rigid barriers between the realities of different political, cultural or religious groups could become even more inflexible.

There is a more immediate concern of uncensored AI made available publicly making it easier for people to learn potentially harmful information such as suicide methods or how to assemble bombs. This information is, of course, already available on the internet but users have to actively go looking for it, potentially leaving digital traces behind.

## Bottom line

There is no clear right or wrong approach to the question of whether AI should be unaligned---it is a matter of how either approach gets implemented. At the end of the day, sound legislation is what we need the most and I fear the legislature is lagging behind woefully.

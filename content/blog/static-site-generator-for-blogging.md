+++
title = "Why I switched to a static site generator for blogging"
date = 2023-09-23
updated = 2023-11-18
slug = "static-site-generator-for-blogging"
description = "Over the years I attempted several times to get into a habit of blogging, using various content management systems (CMS). In this post I will explain why these projects failed and why I switched to a static site generator instead."

[taxonomies]
tags = ["blogging", "content management systems", "Drupal", "Plume", "static site generators", "WordPress", "WriteFreely", "Zola"]

[extra]
toc_ignore_pattern = "^Table of contents$"
isso = true
social_media_card = "img/social_cards/blog_static_site_generator_for_blogging.jpg"
+++

Over the years I attempted several times to get into a habit of blogging, using various content management systems (CMS). In this post I will explain why these projects failed and why I switched to a static site generator instead.<!-- more -->

## Table of contents

<!-- toc -->

-----

It may be vanity, but I feel like my perspective is unusual. There are few legally deafblind people under the age of 40 around and even fewer managed to graduate from a regular (i.e., not special needs) high school---in fact, I am the first deafblind person to achieve this in Germany. I do three martial arts in spite of my disabilities, am a licensed mathematical-technical software developer and study applied math and computer science. Enough reasons to want to share my experiences and insights with the world---preferably via a medium that is accessible to me: text.

## The humble beginnings

Back in the early 2010s I did so by creating my own website, writing [HyperText Markup Language (HTML)](https://developer.mozilla.org/en-US/docs/Web/HTML) in a plain text editor---the really old-school way of creating websites. I was a young teenager at the time with a very slippery grasp on the concept of HTML and my rudimentary tooling did not help the quality of my web content any. Still, embarrassing though these early projects may be to my twenty-three years old self, they started me down the road of becoming a programmer.

Few people write entire HTML documents anymore, however; the internet has democratised significantly and most web content creators these days do not even know HTML. Instead, they use platforms like [Medium](https://medium.com/), [wordpress.com](https://wordpress.com/) or [Ghost](https://ghost.org/) to publish their content depending on their respective needs. What all these platforms got in common is that they are content management systems (CMS).

## Content management systems everywhere


If you have a look at [the 30 most popular websites using WordPress](https://colorlib.com/wp/most-popular-websites-powered-by-wordpress/) in 2023, you will quickly realise one thing: WordPress is popular with news publishers. [The same is true](https://www.vardot.com/en/ideas/blog/top-10-drupal-websites-world-updated) for [Drupal](https://www.drupal.org/), although it seems to appeal to a slightly different clientele. Whether you realise it or not, most websites you interact with use a CMS---as of 14th September 2023, [trends.builtwith.com](https://trends.builtwith.com/cms/traffic/Entire-Internet) detects *77,679,306* live websites using a CMS. But what *are* they, exactly?

## Definition and advantages of CMS

> Content management system (noun): A computer software system for organizing and facilitating collaborative creation of documents and other content, especially for loading to a website. ([Wiktionary](https://en.m.wiktionary.org/wiki/Wiktionary:Main_Page), CC-BY-SA)

In other words, a CMS is *any* software system that enables users to create (web) content collaboratively and manages this content for them. That is what makes a CMS ideal for blogging: Multiple people can create the content together (e.g., write posts, upload images or post and moderate comments) and the CMS manages all that content, usually in a database. Although not strictly included in above definition, the CMS also takes care of presenting that content to users according to their role, e.g., as a read-only website if they are a visitor and as editable content if they are a content creator. With the definitions out of the way, let us take a look at the different CMS I used for blogging over the years.

## To self-host or not to self-host

Personally, I love self-hosting projects. It gives one way more control over the look and feel of a project, often has privacy benefits and one learns a lot this way. However, I know not everyone has the time, interest, money or skills to self-host projects. That is not a problem nowadays, though; almost all CMS have an official version hosted by the creators of the CMS for free, with paid extra features. Alternatively, they can get hosted and managed by a third party, although this tends to be more costly.

## WordPress

WordPress powers the vast majority of websites with a CMS. That alone should tell us that it is good at what it does. It comes in two flavours: wordpress.com and wordpress.org. The former are websites hosted for you by WordPress, with various free features and many more paid ones. The latter are self-hosted versions of the open source WordPress software, with most features being free but requiring configuration and maintenance on your part.

### Pros

- Very flexible, capable of powering any kind of website with the right setup,
- Huge community with countless tutorials, forums, open source themes and plugins and dedicated WordPress designers and developers,
- Labels on themes like "accessibility-ready" help with quickly establishing a base standard for the website,
- Powerful tools to design and write the website content right in the browser,
- Free overview of possible improvements of the website, with suggestions for fixes,
- WordPress has a mobile app,
- Popular editors like iA Writer integrate with it directly.

### Cons

- The sheer amount of configuration options, plugins and themes can be overwhelming,
- Since WordPress runs PHP scripts, code vulnerabilities in WordPress itself or the plugins or themes may be a concern,
- It's difficult to tweak plugins or WordPress itself for your needs; changes may get ,lost after updates, some of which may be automatic,
- Changing translations isn't trivial and may require to purchase a dedicated editor for the `.po` files.

### Why I did not stick with WordPress

My primary reason for not sticking with WordPress is that the sheer amount of features kept distracting me from my projects. I spent hours and hours tweaking and configuring various plugins and settings and theme options and trying to improve accessibility---until I ultimately lost interest in the blogging project again and let it rot.

Another problem for me was that if I spotted a detail that could use improvement it was non-trivial to alter. Of course it was possible---any detail in WordPress can get changed if one knows how to---but the maze of PHP files is difficult to navigate and editing the files directly can be dangerous, plus changes need to get reintroduced after every WordPress or theme update, unless one creates a theme oneself. For me, at least, all that was too distracting.

## Drupal

I never really figured Drupal out. It is supposedly more secure than WordPress and more customisable---though this leads to the tradeoff that you actually do have to do quite a bit of configuring before you can write your first blog post. Unlike WordPress, there's no central organisation you can host blogs with, but some hosting providers offer to install Drupal for you free of charge (self-managed) or with a fee (managed by the hosting provider). Unfortunately, I lost motivation to understand Drupal pretty quickly; again, the CMS got in the way of actually writing blog posts regularly.

## Writefreely

So I started looking into content-centered CMS, which usually offer less customisation and therefore a much less cluttered (and less powerful) interface. They generally let you write your posts in [markdown](https://en.m.wikipedia.org/wiki/Markdown). The first one I checked out was writefreely; it is is the open source software underpinning [write.as](https://write.as/). I had two problems with it: In some places, the accessibility could use improvements---f but improving the website by altering the templates wasn't as easy as one would think. In any case, tweaking those templates was only possible on the server and then required restarting the Writefreely service.

## Plume

[Plume](https://github.com/Plume-org/Plume) is a federated blogging system---people using the [fediverse](https://www.fediverse.to/) can follow authors on a Plume instance and like, reblog and comment their writing out of the box, by virtue of the underlying protocol. Many concepts of Plume are nice and if you find an existing instance that accepts registrations, you do not have to do any configuration yourself. Unfortunately, Plume has several drawbacks:

- It is not very customisable---the instances are supposed to be similar,
- Therefore, any accessibility issues with the Plume-based website need to get fixed in the Plume project itself,
- That project is written in Rust, which makes such changes non-trivial (Rust has a very steep learning curve),
- The project's maintenance and development also seems to be sporadic and infrequent.

## Static site generators instead of a CMS

Combined, all the previous experiences told me that what I really needed was a static site generator for blogging. They usually take flat markdown files as input with metadata like date, time, slug or tags specified at the start of each file. They also rely on templates to determine the precise look and content of any given web page and typically supported themes, giving your website a reasonable, predefined look that can also get shared across different websites.

Since static site generators produce plain HTML files with no server-side scripting required, you can upload the result to any web server you may be hosting. Alternatively, [Git Hub pages](https://pages.github.com/) and similar services can also host your static site for you, often times for free.

The most well known static site generators are [Jekyll](https://jekyllrb.com/) and [Hugo](https://gohugo.io/), but I didn't go with either of them. Instead, I chose [Zola](https://www.getzola.org/).

### Zola

Which static site generator you use is, ultimately, up to you. Since they often are free and open source software, you can even try out several to see which one suits you. I chose Zola because

- It states on its website: 
    
    > Zola gets out of your way so you can focus on your content, be it a blog, a knowledge base, a landing page or a combination of them.
- Zola does not need dependencies for things like parsing Sass or creating a table of contents,
- It's written in Rust, which is my favourite programming language; should I wish to contribute later on, it would be right up my alley.

I will go into Zola in-depth in a future post[^1]; for now, suffice it to say that Zola kept what it promised thus far. Here's to hoping that with this new setup, I will finally stick to a blogging project. 

-----

[^1]: By now, the [in-depth guide to a comfortable Zola setup](@/blog/zola-setup-to-blog-from-anywhere.md) is live!

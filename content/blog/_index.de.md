+++
title = "Alle Beiträge"
description = "Alle Beiträge auf Tim Böttcher online"
sort_by = "date"
paginate_by = 5
template = "section.html"
insert_anchor_links = "right"

[extra]
show_previous_next_article_links = true
quick_navigation_buttons = true
social_media_card = "img/social_cards/de_blog.jpg"
+++

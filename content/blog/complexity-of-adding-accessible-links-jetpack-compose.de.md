+++
title = "Die überraschende Komplexität des Hinzufügens barrierefreier Links in JetPack Compose"
slug = "complexity-of-adding-accessible-links-jetpack-compose"
date = "2024-03-31"
description = "Links sind Teil fast jeder mobilen App. Sie bieten Zugang zu den Nutzungsbedingungen und Datenschutzrichtlinien der App, verweisen auf die Dokumentation und ermöglichen zum Teil die Navigation innerhalb der App. Das Erstellen von Links in JetPack Compose ist jedoch überraschend komplex und diese Links barrierefrei zu machen ist noch schwieriger."

[taxonomies]
tags = ["Android", "Appentwicklung", "Barrierefreiheit", "jetpack compose"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
isso = true
social_media_card = "img/social_cards/de_blog_complexity_of_adding_accessible_links_jetpack_compose.jpg"
+++

Links sind Teil fast jeder mobilen App. Sie bieten Zugang zu den Nutzungsbedingungen und Datenschutzrichtlinien der App, verweisen auf die Dokumentation und ermöglichen zum Teil die Navigation innerhalb der App. Das Erstellen von Links in [JetPack Compose](https://developer.android.com/develop/ui/compose) ist jedoch überraschend komplex und diese Links barrierefrei zu machen ist noch schwieriger.<!-- more -->

## Inhaltsverzeichnis

<!-- toc -->

## Wie erstellt man einen Link in JetPack Compose?

Wenn Sie die obige Frage in die Suchmaschine Ihrer Wahl eingeben, werden Sie wahrscheinlich Folgendes heraus finden:

- Es gibt eine [Material UI Komponente für Links](https://mui.com/material-ui/react-link/), aber keine solche JetPack Compose Composable.
- Stattdessen müssen Sie mit dem [`ClickableText` composable](https://www.composables.com/foundation/clickabletext) arbeiten und einen passenden [`AnnotatedString`](https://developer.android.com/reference/kotlin/androidx/compose/ui/text/AnnotatedString) konstruieren.

{{ admonition(type="danger", text="Kopieren Sie nicht einfach den folgenden Code und fügen Sie ihn in Ihr Projekt ein -- er ist noch nicht barrierefrei.") }}

Die Lösung, über die Sie höchstwahrscheinlich stolpern werden, wird in etwa so aussehen:

```kotlin
@Composable
fun Link() {
    val annotatedLinkText = buildAnnotatedString {
        val linkUrl = "https://example.com/"
        val linkText = "Besuchen Sie meine Website!"
        val linkStart = linkText.indexOf("meine Website")
        val linkEnd = linkStart + 13
        append(linkText)

        addStyle(
            style = SpanStyle(
                color = Color.Blue,
                fontSize = 18.sp,
                textDecoration = TextDecoration.Underline
            ), start = linkStart, end = linkEnd
        )

        addStringAnnotation(
            tag = "URL",
            annotation = linkUrl,
            start = linkStart,
            end = linkEnd,
        )
    }

    val uriHandler = LocalUriHandler.current
    ClickableText(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        text = annotatedLinkText,
        onClick = { offset ->
            annotatedLinkText.getStringAnnotations("URL", offset, offset)
                .firstOrNull()?.let { stringAnnotation ->
                    uriHandler.openUri(stringAnnotation.item)
                }
        }
    )
}
```

Dieses Snippet tut folgendes:

- `buildAnnotatedString { }` macht genau das: Es baut einen `AnnotatedString` aus mehreren Deklarationen in Bezug auf den Inhalt und die Annotationen der Zeichenkette auf.
- Der Aufruf `append()` fügt den angegebenen Text an das Ende des `AnnotatedString` an.
- Der Aufruf `addStyle()` fügt dem `AnnotatedString`, wie nicht anders zu erwarten, eine Art von Styling hinzu.
- Wir wollen nur, dass "Meine Website" wie ein Link aussieht, also fügen wir einen `SpanStyle` hinzu, der das erwartete Aussehen nur vom Anfang bis zum Ende des "Meine Website"-Teils des Textes hinzufügt, ohne das Styling des restlichen `AnnotatedString` zu verändern.
- Außerdem fügen wir dem Abschnitt "Meine Website" des `AnnotatedString` mit `addStringAnnotation()` eine spezielle Anmerkung oder Annotation hinzu. Das ist so, als ob wir einen Schlüssel (in diesem Fall `URL`) mit einem Wert (der Link-URL) verbinden und diesen Schlüssel an einen Teil des `AnnotatedString` anhängen.
- Dann erstellen wir einen `UriHandler`, der es ermöglicht, die URL später in einem externen Browser zu öffnen.
- Als nächstes verwenden wir die `ClickableText` Composable und übergeben ihr den zuvor erstellten `AnnotatedString`.
- Die Composable `ClickableText` hat einen speziellen `onClick` Callback, der uns mitteilt, wo im Text der Benutzer geklickt hat (`offset`). Wir verwenden dies, um alle Annotationen mit dem Schlüssel `URL` abzurufen, die mit dem `AnnotatedString` an dieser Stelle verbunden sind.
- Dies gibt entweder eine `StringAnnotation` oder `null` zurück. Wenn `null` zurückgegeben wird, passiert nichts. Andernfalls rufen wir die in der `StringAnnotation` gespeicherte URL ab und übergeben sie an die `open()`-Methode des `UriHandler`.

Das ist ziemlich kompliziert für eine so alltägliche Sache wie einen Link, aber es funktioniert. Das Ergebnis ist, dass der Text "Meine Website" wie ein Link aussieht und sich wie ein Link verhält, wenn er angetippt wird -- jedenfalls während TalkBack nicht aktiv ist. Aber was passiert, wenn Sie TalkBack aktivieren?

## TalkBack-Probleme

Wenn Sie einen Link wie oben beschrieben erstellen, TalkBack einschalten und zum Link navigieren, sagt TalkBack: "Besuchen Sie meine Website!"

Kein Hinweis darauf, dass der Text anklickbar ist, geschweige denn, dass es sich um einen Link handelt. Tatsächlich ist der Link nicht anklickbar, wenn TalkBack eingeschaltet ist. Es gibt keine Möglichkeit, auf den "Meine Website"-Teil des Links zu klicken, und daher funktioniert die Logik, die wir oben implementiert haben, nicht.

Wir könnten das "beheben", indem wir den gesamten Text anklickbar machen und dem Composable `ClickableText` die Rolle `button` zuweisen. Alternativ können wir gleich die Composable `Button` verwenden. Aber das ist nicht die Art und Weise, wie Links funktionieren sollen, und es beschreibt die Rolle des Inhalts nicht richtig -- eine Schaltfläche ist kein Link.

## Die Lösung

{{ admonition(type="note", text="Der folgende Ansatz funktioniert zwar, basiert aber auf experimentellen APIs. Wenn Sie einen besseren Weg kennen, kommentieren Sie bitte diesen Beitrag oder kontaktieren Sie mich direkt.") }}

Die Suche nach einer Lösung für dieses Problem war überraschend schwierig. Weder in der [offiziellen Dokumentation für Barrierefreiheit in JetPack Compose](https://developer.android.com/develop/ui/compose/accessibility) noch in den inoffiziellen [JetPack Compose Best Practices für Barrierefreiheit](https://dev.to/carlosmonzon/jetpack-compose-accessibility-best-practices-38j0) werden Links erwähnt. Nachdem ich mich durch die Dokumentation für [`AnnotationString.Builder`](https://developer.android.com/reference/kotlin/androidx/compose/ui/text/AnnotatedString.Builder) gewühlt hatte, war ich mir aber ziemlich sicher, dass es möglich sein sollte, URL-Annotationen zu einem `AnnotatedString` hinzuzufügen, und das ist es auch -- vorausgesetzt, man ist bereit, experimentelle APIs zu verwenden.

Der folgende Codeschnipsel erweitert das obige Beispiel und macht es barrierefrei:

```kotlin
@OptIn(ExperimentalTextApi::class)
@Composable
fun Link() {
    val annotatedLinkText = buildAnnotatedString {
        val linkUrl = "https://example.com/"
        val linkText = "Besuchen Sie meine Website!"
        val linkStart = linkText.indexOf("meine Website")
        val linkEnd = linkStart + 13
        append(linkText)

        addStyle(
            style = SpanStyle(
                color = Color.Blue,
                fontSize = 18.sp,
                textDecoration = TextDecoration.Underline
            ), start = linkStart, end = linkEnd
        )

        addUrlAnnotation(
            urlAnnotation = UrlAnnotation(linkUrl),
            start = linkStart,
            end = linkEnd,
        )

        addStringAnnotation(
            tag = "URL",
            annotation = linkUrl,
            start = linkStart,
            end = linkEnd,
        )
    }

    val uriHandler = LocalUriHandler.current
    ClickableText(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        text = annotatedLinkText,
        onClick = { offset ->
            annotatedLinkText.getStringAnnotations("URL", offset, offset)
                .firstOrNull()?.let { stringAnnotation ->
                    uriHandler.openUri(stringAnnotation.item)
                }
        }
    )
}
```

Das ist alles: `@OptIn(ExperimentalTextApi::class)` ermöglicht die Verwendung von `addUrlAnnotation()`, welches die Informationen liefert, die TalkBack benötigt, um den Link barrierefrei zu machen.

Wenn Sie jetzt zu dem Link navigieren, wird TalkBack sagen: "Besuchen Sie meine Website! Links verfügbar". Blinde Benutzer können dann mit dem Link so interagieren, wie sie es in einer mobilen (nicht browserbasierten) App immer tun würden.

Ich hoffe, dieser Beitrag hat Ihnen einige Kopfschmerzen erspart.

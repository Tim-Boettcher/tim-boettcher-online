+++
title = "Viele CAPTCHAs sind diskriminierend"
date = "2024-03-24"
template = "page.html"
description = "Completely Automated Public Turing tests to tell Computers and Humans Apart (CAPTCHAs) sind das digitale Äquivalent von Türstehern, die darüber wachen, wer zu dem, was das CAPTCHA bewacht, durchgelassen wird und wer nicht. Leider sind die Annahmen, die viele CAPTCHAs darüber treffen, was ein Mensch zu sein bedeutet, zutiefst diskriminierend und die Puzzle selbst nicht zielführend."

[taxonomies]
tags = ["Barrierefreiheit", "Behindertenfeindlichkeit", "Bots", "CAPTCHA", "Meinung", "Sicherheit", "Spam"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
isso = true
social_media_card = "img/social_cards/de_blog_many_captchas_are_discriminatory.jpg"
+++

Completely Automated Public Turing tests to tell Computers and Humans Apart (CAPTCHAs) sind das digitale Äquivalent von Türstehern, die darüber wachen, wer zu dem, was das CAPTCHA bewacht, durchgelassen wird und wer nicht. Leider sind die Annahmen, die viele CAPTCHAs darüber treffen, was ein Mensch zu sein bedeutet, zutiefst diskriminierend und die Puzzle selbst nicht zielführend.<!-- more -->

## Inhaltsverzeichnis

<!-- toc -->

## Die Mission

Alles wird digital, auch [unsere Zahlungen](https://www.mckinsey.com/industries/financial-services/our-insights/banking-matters/new-trends-in-us-consumer-digital-payments), [unsere Beziehungssuche](https://www.forbes.com/health/dating/dating-statistics/), [der Wareneinkauf](https://www.statista.com/topics/871/online-shopping/#topicOverview) und [sogar unsere Regierungen](https://www.worldbank.org/en/topic/digitaldevelopment/brief/digital-government-for-development). Dies bedeutet, dass sehr sensible Daten online übermittelt und verarbeitet werden -- buchstäblich lebensverändernde Handlungen können heutzutage größtenteils oder vollständig online durchgeführt werden. Gleichzeitig versuchen bösartige Akteure, aus den Möglichkeiten des digitalen Betrugs und Diebstahls Kapital zu schlagen. Sie versuchen, automatisierte Skripte, üblicher Weise Bots genannt, als legitime Nutzer auszugeben und Anfragen an einen Dienst zu senden. Z. B. versuchen sie sich automatisiert anzumelden, Beiträge in sozialen Medien zu posten oder in einer Dating-App wie Tinder zu swipen.

Sicher zu stellen, dass nur Anfragen von legitimen Nutzern und autorisierten Bots bearbeitet werden, ist aus mehreren Gründen wichtig:

- Bots könnten leicht Aktionen wie den Kauf von Eintrittskarten für beliebte Konzerte oder das Knacken von Passwörtern von Nutzern durchführen, indem sie mehrere Anfragen pro Sekunde übermitteln, was schädliche Nebeneffekte hätte.
- Die Qualität der Inhalte von Plattformen verschlechtert sich, wenn sie von Bot- und Spam-Konten überschwemmt werden.
- Der Dienst selbst leidet ebenfalls darunter, da jede von Bots generierte Anfrage, die er bearbeiten muss, Ressourcen kostet -- was im schlimmsten Fall zu einem Zusammenbruch des Dienstes (Denial of Service) führt.

CAPTCHAs sind eine Möglichkeit, dieses Problem zu lösen. Sie stellen den Anfragensteller vor eine Aufgabe, die nur ein Mensch lösen können sollte. Diese Aufgaben können textbasiert, bildbasiert und/oder audiobasiert sein; sie testen Mustererkennung, Logik oder Wissen, um zu überprüfen, ob der Nutzer legitim ist und reduzieren so die oben erwähnten Probleme mit Bots. Darüber hinaus können Unternehmen wie Google CAPTCHAs nutzen, um ihre [Bilderkennungs-KI zu trainieren](https://www.techradar.com/news/captcha-if-you-can-how-youve-been-training-ai-for-years-without-realising-it). Eine Win-Win-Situation... oder?

## Die Probleme

Leider verursachen CAPTCHAs mindestens so viele Probleme wie sie lösen.

### Benutzererfahrung

Niemand möchte Zeit damit verbringen, ein Rätsel zu lösen, um seine Menschlichkeit zu beweisen -- und zwar eine Menge Zeit: Laut [eines Cloudflare-Blogposts](https://blog.cloudflare.com/introducing-cryptographic-attestation-of-personhood/) verschwendet die Menschheit etwa **500 Jahre** damit, zu beweisen, dass sie menschlich ist -- pro Tag, jeden Tag! Diese Verschwendung wird immer größer, da die Bots immer besser darin werden, CAPTCHAs zu lösen, und die Anbieter der Puzzle sie deshalb [schwieriger gesstalten](https://www.technologyreview.com/2023/10/24/1081139/captchas-ai-websites-computing/).

### Nachlassende Effektivität

Apropos: Bots werden immer besser im Lösen von CAPTCHAs. [Eine empirische Studie über moderne CAPTCHAs](https://arxiv.org/abs/2307.12108) hat ergeben, dass Bots die gängigen CAPTCHAs viel besser lösen können als Menschen und auch deutlich weniger Zeit benötigen, um die Aufgaben zu lösen. Aber selbst wenn es gelingt, eine Aufgabe zu erstellen, die Bots nicht in angemessener Zeit oder mit angemessener Genauigkeit lösen können, können Spammer dieses Problem umgehen, indem sie [billige Arbeitskräfte für die Lösung von CAPTCHAs bezahlen](https://www.technologyreview.com/2010/08/11/201606/how-spammers-use-low-cost-labor-to-solve-captchas/).

### Behindertenfeindlichkeit

Als ob dies alles noch nicht deprimierend genug wäre, gibt es noch einen dritten Faktor zu berücksichtigen: CAPTCHAs machen Annahmen darüber, was es bedeutet, ein Mensch zu sein. Menschen können sehen und/oder hören; sie sind in der Lage, Muster zu erkennen; sie sind fähig logisch zu denken und verfügen über ein gewisses Grundwissen; wenn sie eine Computermaus benutzen, bewegen sie diese auf eine bestimmte Art und Weise; und sie lösen eine bestimmte Aufgabe innerhalb eines bestimmten Zeitrahmens. Wenn man von dieser Definition abweicht, wird man oft als Bot eingestuft -- selbst, wenn man das CAPTCHA korrekt gelöst hat. Und das, obwohl es viele Menschen gibt, die von den gängigen Annahmen der CAPTCHA-Anbieter abweichen. Dies gilt insbesondere für Menschen mit Behinderungen.

Für Menschen wie mich werden CAPTCHAs zu digitalen Türstehern, die uns entweder ganz abweisen oder eine Aufgabe so mühsam machen, dass wir einfach aufgeben, sie zu erledigen. Sie zwingen uns, um Hilfe zu bitten oder -- ironischer Weise -- eine Browsererweiterung zu verwenden, um CAPTCHAs für uns zu lösen.

## Lösungen

Glücklicherweise haben einige Anbieter von CAPTCHAs begonnen, auf die oben genannten Probleme einzugehen. Sie versuchen, den Arbeitsaufwand für den Benutzer zu verringern:

### Prüfen auf übliche Browsereigenschaften

Anstatt den Benutzer zu zwingen, zu beweisen, dass er ein Mensch ist, machen sich Projekte wie [mCaptcha](https://mcaptcha.org/) die Tatsache zunutze, dass Browser bestimmte Einschränkungen haben. Dies bedeutet, dass das Lösen komplexer mathematischer Aufgaben in einem legitimen Browser eine Weile dauert. Wenn also die Aufgabe zu schnell gelöst wird, kann man davon ausgehen, dass ein Bot versucht, das Formular oder die Anfrage zu übermitteln. Solche Herausforderungen führen mindestens zu einer Geschwindigkeitsbegrenzung (Rate Limiting). Das heißt, dass die Fähigkeit von Bots, Anfragen schnell zu übermitteln, beeinträchtigt wird.

### IP-Prüfungen

Eine weitere Methode zur Reduktion der Probleme im Zusammenhang mit Bots besteht darin, die IP-Adresse, die zur Übermittlung einer Anfrage verwendet wird, daraufhin zu überprüfen, ob sie bereits mit Spam in Verbindung gebracht wurde. Außerdem können IP-Adressen (möglicherweise vorübergehend) gesperrt werden, wenn eine Aktion wie die Anmeldung wiederholt fehl schlägt. Ein aggressiverer Schritt wäre die Prüfung der [IP-Adressreputation](https://talosintelligence.com/reputation_center/) der Adresse, von der die Anfrage ausgeht, und die Sperrung aller Adressen, die unterhalb einer bestimmten Reputation liegen. Dies kann jedoch auch unerwünschte Nebeneffekte haben: Personen aus Regionen, die üblicherweise mit Spam in Verbindung gebracht werden, werden unter Generalverdacht gestellt, nur weil ihre IP-Adresse zufällig in einem IP-Bereich liegt, der mit fragwürdige Aktivitäten assoziiert wurde. Aber wenigstens erfordert die Durchführung von IP-Prüfungen keine Aktionen seitens der Nutzer.

### Benutzeridentifikation anfordern

Kritische Dienste können den Nutzer auch auffordern, offizielle Dokumente zur Überprüfung seiner Identität vorzulegen, z. B. seinen Führerschein. Dies hat offensichtliche Auswirkungen auf den Datenschutz und ist nicht für jeden Dienst eine Option, aber es ist ein wirksames Mittel zur Vorbeugung von Spam und Bot-Aktivitäten.

## Zusammengefasst

Moderne CAPTCHAs verfehlen ihren Zweck, indem sie legitime Nutzer stark belasten und gleichzeitig keinen angemessenen Schutz mehr gegen Bots bieten. Insbesondere sind sie diskriminierend, da sie Standards dafür definieren, was es bedeutet, ein Mensch zu sein. Es sollten alternative Ansätze gewählt werden.

+++
title = "The surprising complexity of adding accessible links in JetPack Compose"
slug = "complexity-of-adding-accessible-links-jetpack-compose"
date = "2024-03-31"
description = "Links are part of almost every mobile app. They offer access to the terms of service and privacy policy of the app, link to the documentation and occasionally enable navigation within the app. Creating links in JetPack Compose is surprisingly complex, though, and making those links accessible is even harder."

[taxonomies]
tags = ["accessibility", "android", "app development", "jetpack compose"]

[extra]
toc_ignore_pattern = "^Table of contents$"
isso = true
social_media_card = "img/social_cards/blog_complexity_of_adding_accessible_links_jetpack_compose.jpg"
+++

Links are part of almost every mobile app. They offer access to the terms of service and privacy policy of the app, link to the app's documentation and occasionally enable navigation within it. Creating links in [JetPack Compose](https://developer.android.com/develop/ui/compose) is surprisingly complex, though, and making those links accessible is even harder.<!-- more -->

## Table of contents

<!-- toc -->

## How to create a link in JetPack Compose?

If you search the above in your favourite search engine, chances are you will discover the following:

- There is a [Material UI component for links](https://mui.com/material-ui/react-link/), but no such composable for JetPack Compose.
- Instead, you need to work with the [`ClickableText` composable](https://www.composables.com/foundation/clickabletext) and construct a suitable [`AnnotatedString`](https://developer.android.com/reference/kotlin/androidx/compose/ui/text/AnnotatedString).

{{ admonition(type="danger", text="Do not just copy and paste the below code into your project---it is not accessible yet.") }}

The solution you will most likely stumble upon will probably look something like this:

```kotlin
@Composable
fun Link() {
    val annotatedLinkText = buildAnnotatedString {
        val linkUrl = "https://example.com/"
        val linkText = "Visit my website!"
        val linkStart = linkText.indexOf("my website")
        val linkEnd = linkStart + 10
        append(linkText)

        addStyle(
            style = SpanStyle(
                color = Color.Blue,
                fontSize = 18.sp,
                textDecoration = TextDecoration.Underline
            ), start = linkStart, end = linkEnd
        )

        addStringAnnotation(
            tag = "URL",
            annotation = linkUrl,
            start = linkStart,
            end = linkEnd,
        )
    }

    val uriHandler = LocalUriHandler.current
    ClickableText(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        text = annotatedLinkText,
        onClick = { offset ->
            annotatedLinkText.getStringAnnotations("URL", offset, offset)
                .firstOrNull()?.let { stringAnnotation ->
                    uriHandler.openUri(stringAnnotation.item)
                }
        }
    )
}
```

This snippet does the following:

- `buildAnnotatedString { }` does exactly that: It builds an annotated string from several declarations about that string's content and annotations.
- The `append()` call adds the supplied text to the end of the `AnnotatedString`.
- `addStyle()`, unsurprisingly, adds some kind of styling to the `AnnotatedString`.
- We only want "my website" to look like a link, so we add a `SpanStyle` that adds the expected appearance only from the start to the end of the "my website" portion of the text, not altering the styling of the rest of the `AnnotatedString`.
- We also add a special annotation to the "My website" porition of the `AnnotatedString` using `addStringAnnotation()`. This is like associating a key (`URL` in this case) with a value (the link URL) and attaching that key to a part of the `AnnotatedString`.
- Then we create a `UriHandler` which enables opening the URL in an external browser later.
- Next, we use the `ClickableText` composable and pass the previously created `AnnotatedString` to it.
- The `ClickableText` composable has a special `onClick` callback that tells us where in the text the user clicked (the `offset`). We use that to retrieve any annotations with the key `URL` associated with the `AnnotatedString` at this location.
- This returns either a `StringAnnotation`or `null`. If it returns `null`, nothing happens. Otherwise we retrieve the URL stored in the `StringAnnotation` and pass it to the `UriHandler`'s `open()` method.

This is fairly complicated for such a common thing as a link, but it works. The result is that the text "my website" looks like a link and behaves like a link when tapped while TalkBack is not active. But what happens when it is?

## TalkBack issues

If you create a link as described above, turn on TalkBack and navigate to it, TalkBack will say, "Visit my website!"

No indication that the text is clickable, let alone that it is a link. In fact, the link is not clickable if TalkBack is turned on. There is no way to click on the "my website" part of the link specifically and therefore the logic we implemented above does not work.

We could "fix" that by making the entire text clickable and assigning the button role to the `ClickableText` composable, or perhaps by using the `Button` composable to begin with. But that is not how links are supposed to work and in fact it does not describe the content's role accurately---a button is not a link.

## The solution

{{ admonition(type="note", text="While the below approach works, it relies on experimental APIs. If you know a better way, please comment on this post or contact me directly.") }}

Finding a solution to this problem was surprisingly difficult. Neither the [official documentation for accessibility in JetPack Compose](https://developer.android.com/develop/ui/compose/accessibility) nor the unofficial [JetPack Compose accessibility best practices](https://dev.to/carlosmonzon/jetpack-compose-accessibility-best-practices-38j0) mention links. After digging through the documentation for [`AnnotatedString.Builder`](https://developer.android.com/reference/kotlin/androidx/compose/ui/text/AnnotatedString.Builder) I was fairly sure that it should be possible to add URL annotations to an `AnnotatedString` and it is---provided you are willing to opt in to experimental features.

The following code snippet expands upon above example, making it accessible:

```kotlin
@OptIn(ExperimentalTextApi::class)
@Composable
fun Link() {
    val annotatedLinkText = buildAnnotatedString {
        val linkUrl = "https://example.com/"
        val linkText = "Visit my website!"
        val linkStart = linkText.indexOf("my website")
        val linkEnd = linkStart + 10
        append(linkText)

        addStyle(
            style = SpanStyle(
                color = Color.Blue,
                fontSize = 18.sp,
                textDecoration = TextDecoration.Underline
            ), start = linkStart, end = linkEnd
        )

        addUrlAnnotation(
            urlAnnotation = UrlAnnotation(linkUrl),
            start = linkStart,
            end = linkEnd,
        )

        addStringAnnotation(
            tag = "URL",
            annotation = linkUrl,
            start = linkStart,
            end = linkEnd,
        )
    }

    val uriHandler = LocalUriHandler.current
    ClickableText(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        text = annotatedLinkText,
        onClick = { offset ->
            annotatedLinkText.getStringAnnotations("URL", offset, offset)
                .firstOrNull()?.let { stringAnnotation ->
                    uriHandler.openUri(stringAnnotation.item)
                }
        }
    )
}
```

That is all: `@OptIn(ExperimentalTextApi::class)` enables usage of `addUrlAnnotation()`, which provides the information TalkBack needs to make the link accessible.

If you navigate to the link now, TalkBack will say, "Visit my website! Links available". Blind users can then interact with the link the way they always would in a mobile (non-browser) application.

I hope this post saved you some headaches.

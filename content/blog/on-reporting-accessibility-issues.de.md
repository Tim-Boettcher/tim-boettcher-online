+++
title = "Barrierefreiheitsprobleme melden"
date = "2024-03-25"
template = "page.html"
description = "TalkBack hat einige ernsthafte Probleme mit der Unterstützung von Braillezeilen. Wie schwer kann es sein, diese Probleme zu melden? Sehr schwer, wie sich herausstellt."

[taxonomies]
tags = ["Barrierefreiheit", "Bugs", "Support"]

[extra]
isso = true
toc_ignore_pattern = "^Inhaltsverzeichnis$"
social_media_card = "img/social_cards/de_blog_on_reporting_accessibility_issues.jpg"
+++

TalkBack hat einige ernsthafte Probleme mit der Unterstützung von Braillezeilen. Wie schwer kann es sein, diese Probleme zu melden? Sehr schwer, wie sich herausstellt.<!-- more -->

**Bitte beachten**: In diesem Beitrag geht es zwar speziell um die Meldung von Problemen mit der Braillezeilenunterstützung von TalkBack, aber dies ist nur ein Beispiel. Ich habe schon mit der Meldung von Barrierefreiheitsproblemen bei Apple, Tinder, Signal und vielen anderen großen und kleinen Unternehmen zu kämpfen gehabt.

## Inhaltsverzeichnis

<!-- toc -->

## Was ist das Problem?

[TalkBack](https://support.google.com/accessibility/android/answer/6283677?hl=de) unterstützt zum Glück seit Android 13 [die meisten Braillezeilen von Haus aus](https://www.androidcentral.com/apps-software/android-13-is-picking-up-native-support-for-most-braille-displays). Das bedeutet jedoch nicht, dass Android-Geräte bei Verwendung von Braillezeilen absolut barrierefrei sind. Sie zeigen oft weniger Informationen an, als die Sprachausgabe des Screenreaders wieder gibt -- ein leider sehr verbreiteter Trend bei diversen Screenreadern auf allen Betriebssystemen. Aber während [der Arbeit an meiner eigenen Android-App](https://github.com/Almost-Senseless-Tech/Voskle-Live-Transcribe) entdeckte ich ein noch schwerwiegenderes Problem: Nicht alle Textfelder werden von TalkBack als solche erkannt, selbst bei Verwendung der Sprachausgabe. Das hat den Nebeneffekt, dass man mit der Braille-Tastatur keinen Text in das Textfeld eingeben kann und die Cursorposition nicht korrekt an die Braillezeile gemeldet wird. Ein weiteres Problem: Fortschrittsanzeigen werden von der TalkBack-Sprachausgabe angesagt (in der Form "x Prozent, Statusleiste"), erscheinen aber auf Braillezeilen als komplett leere Zeile. Zu guter Letzt werden Benachrichtigungen und Aktualisierungen von Live-Bereichen zwar per Sprache angesagt, erscheinen aber auf der Braillezeile nicht als Blitzmeldung, wie ich es erwarten würde.

## Meldungsversuche

### Google Public Bug Tracker

Ich habe [eine Meldung für das Textfeldproblem](https://issuetracker.google.com/issues/328117822) im Google Public Bug Tracker erstellt, aber bisher gab es außer der Zuweisung an eine zuständige Person keine weiteren Aktivitäten. In Anbetracht der Tatsache, dass es sich um ein schwerwiegendes Barrierefreiheitsproblem handelt, verstehe ich wirklich nicht, warum in dieser Angelegenheit anscheinend überhaupt nichts passiert. Zugegeben, es ist nicht klar, ob es sich um ein Problem mit JetPack Compose, Android 13, TalkBack 14 oder One UI (Samsungs Android-Skin) handelt. Aber die Kommunikation, oder vielmehr das Fehlen derselben, ist dennoch bedauerlich.

### Galaxy-Support

Wie bereits erwähnt, kann das Problem durchaus von One UI herrühren, also habe ich mich auch an den Galaxy-Support gewandt. Das ging einigermaßen gut, jedoch war das Kontaktformular, das sie mir am Ende vorlegten, um Kontaktdaten von mir zu erhalten, nicht barrierefrei -- genauer gesagt das Kontrollkästchen, dass ich ihre Datenschutzrichtlinien gelesen habe. Ich war also gezwungen, ohne Kontaktinformationen weiterzumachen; es ist schwer zu sagen, was, wenn überhaupt etwas, mit meinem Bericht geschehen ist.

### Google Support für Menschen mit Behinderung

Da die oben genannten Schritte zu keinem greifbaren Ergebnis geführt haben, habe ich versucht, [den Google-Support für Menschen mit Behinderung](https://support.google.com/accessibility/answer/7641084?hl=en) zu kontaktieren. Das Kontaktformular für den E-Mail-Support ist auf iOS und Windows mit JAWS völlig unbarrierefrei und auf Windows mit NVDA schwer zu bedienen. Wenn man eine Anfragenkategorie auswählt, passiert etwas Seltsames mit dem Formular. Dadurch werden viele Formularfelder ausgeblendet, so dass es schwierig, wenn nicht gar unmöglich wird, das Formular auszufüllen und abzuschicken.

### Lebe ich in einer Satiresendung?

Anscheinend.

## Warum das wichtig ist

Menschen mit Behinderungen müssen bereits für alltägliche, scheinbar einfache Aufgaben mehr Energie aufwenden als andere. Ein Ausdruck, den wir oft verwenden, ist, dass wir weniger Spoons (also Löffel) haben:

> Die Spoon-Theorie ist eine Metapher, die die Menge an körperlicher und/oder geistiger Energie beschreibt, die einer Person für tägliche Aktivitäten und Aufgaben zur Verfügung steht und dass diese begrenzt sein kann.
> 
> Quelle: [Wikipeida](https://en.wikipedia.org/wiki/Spoon_theory)

Barrierefreiheitsprobleme jeglicher Art zwingen uns dazu, mehr von unseren kostbaren Spoons oder Energieeinheiten zu verwenden, um einen alternativen Ansatz zu finden, der das Problem umgeht -- vorausgesetzt, ein solcher alternativer Ansatz existiert überhaupt. Die Tatsache, dass die Meldung von Barrierefreiheitsproblemen so schwierig ist, macht die Sache noch schlimmer:

- Es kann sein, dass wir auf Barrieren stoßen, während wir versuchen, ein Barrierefreiheitsproblem zu melden.
- Oft erhalten wir keine Rückmeldung, ob unsere Meldung gesehen wurde und ob überhaupt etwas unternommen wird.
- Jede Gruppe mit einer bestimmten Behinderung wird zwangsläufig nur einen kleinen Teil der Nutzer ausmachen, so dass sich die Unternehmen oft fragen, ob es sich lohnt, die Barrierefreiheit zu verbessern.
- Aufgrund des oben beschriebenen Mangels an Feedback müssen wir das Problem oft mehrmals melden -- jedes Mal, wenn ein neues Update veröffentlicht wird, das das Problem nicht behebt.
- Selbst wenn das Problem behoben wird, kann es Monate dauern, bis es behoben ist; in dieser Zeit müssen Menschen mit Behinderungen weiter mit diesem kämpfen.

Manchmal bin ich es leid, Probleme bei Unternehmen zu melden, insbesondere bei riesigen Konzernen wie Microsoft, Google, Apple, Amazon und Meta. Aber wenn ich sie nicht melde, wer dann?

+++
title = "Welcher Screenreader ist am Braillezeilen-freundlichsten?"
template = "page.html"
description = "Spieglein, Spieglein an der Wand, welcher ist der Braillezeilen-freundlichste Screenreader im ganzen Land? Die Antwort auf diese Frage könnte Sie überraschen."
date = "2025-03-02"
slug = "braille-display-friendly-screen-reader"

[taxonomies]
tags = ["Barrierefreiheit", "Braillezeilen", "BRLTTY", "JAWS", "NVDA", "Screenreader", "VoiceOver"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
isso = true
social_media_card = "img/social_cards/de_blog_braille_display_friendly_screen_reader.jpg"
+++

Spieglein, Spieglein an der Wand, welches ist der Braillezeilen-freundlichste Screenreader im ganzen Land? Die Antwort auf diese Frage könnte Sie überraschen.<!-- more -->

## Inhaltsverzeichnis

<!-- toc -->

## Motivation

Es gibt hauptsächlich zwei Methoden, mit denen blinde Benutzer\*innen auf einen Computer oder ein Smartphone zugreifen können: Sprachausgabe oder Braille ([^1]). Manche Benutzer\*innen verwenden diese Methoden auch parallel, indem sie sich z.B. normalerweise auf die Sprachausgabe verlassen, aber eine Braillezeile zum Korrekturlesen eines Textes verwenden ([^2]). Obwohl hypothetisch gesehen beide Strategien gleichermaßen geeignet sind, um Zugriff auf digitale Inhalte zu erhalten, gibt es bei der Durchführung von Barrierefreiheitstests erhebliche Unterschiede; einen Vergleich finden Sie in der folgenden Tabelle:

| Sprachausgabe                                         | Braillezeilen                                                                                                       |
| ----------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| Es ist kostenlose, quelloffene Software verfügbar.    | Benötigt teure Hardware.                                                                                            |
| Kann für viele Sprachen konfiguriert werden.          | Das Erlernen der Brailleschrift ist (meist) erforderlich.                                                           |
| Manchmal ist keine Softwareinstallation erforderlich. | In der Regel ist es erforderlich, den Einrichtungsanleitungen zu folgen und Software (wie Treiber) zu installieren. |

{{ admonition(type="note", title="ANMERKUNG", text="Ich bin mir bewusst, dass es bei vielen Screenreadern *Braille-Betrachter*-Funktionen gibt. Meiner Erfahrung nach geben diese jedoch nicht unbedingt die Darstellung der Benutzeroberfläche auf einer Braillezeile korrekt wieder.") }}

Es ist daher nicht überraschend, dass die Unterstützung für die Sprachausgabe oft besser und umfassender ist. Der Screenreader NVDA meldete beispielsweise jahrelang keine Änderungen von ARIA-Live-Regionen auf Braillezeilen; das entsprechende GitHub-Issue wurde 2017 geöffnet und erst 2023, nach fast sechs Jahren, geschlossen ([^3]). Für viele taubblinde Menschen wie mich gibt es jedoch keine Alternative zur Verwendung von Braillezeilen, um auf digitale Inhalte zuzugreifen; die Suche nach dem Screenreader, der unsere Bedürfnisse am besten unterstützt, ist daher äußerst wichtig.

## Vergleichskriterien

Was macht einen Screenreader Braillezeilen-freundlich? Offensichtlich sollte er

- So viele Braillezeilenmodelle wie möglich unterstützen,
- die angeschlossene(n) Braillezeile(n) automatisch erkennen und
- über eine große Auswahl an verfügbaren Braille-Tabellen verfügen (d.h. Unterstützung für die Darstellung verschiedener Sprachen in Braille).

Das sind die Grundlagen. Aber um als Braillezeilen-freundlich zu gelten, sollte der Screenreader auch

- Die Darstellung ungewöhnlicher Zeichen wie Emoji oder ausländischer Alphabete in irgendeiner Form unterstützen,
- alle Informationen, die gesprochen werden, auf irgendeine Weise auch in Braille verfügbar machen,
- wissenschaftliche Notationen unterstützen (z.B. mathematische oder chemische Formeln),
- dem/der Benutzer\*in die Möglichkeit bieten, Informationen über die Schriftart abzurufen (Attribute wie fett oder kursiv, Schriftgröße, Farbe, ...) und
- es einfach machen, mehrere Braillezeilen parallel zu verwenden und schnell zwischen den Braillezeilen zu wechseln.

Darüber hinaus sollten Screenreader anpassbar und erweiterbar sein, z.B. indem sie es Benutzer\*innen ermöglichen, die Darstellung von Steuerelementen auf einer Braillezeile zu ändern und Skripte zu schreiben, die das Verhalten des Screenreaders erweitern oder verändern.

## Die Screenreader

Ich habe die folgenden Screenreader miteinander verglichen:

- BRLTTY (Standalone auf Android, mit Narrator auf Windows sowie Orca auf GNU Linux),
- JAWS (Windows),
- NVDA (Windows) und
- VoiceOver (iOS und Mac OS).

## BRLTTY

{% admonition(type="note", title="ANMERKUNG") %}
Ich weiß, dass BRLTTY kein Screenreader im klassischen Sinne ist und dass "richtige" Screenreader wie Orca über die BrlAPI mit BRLTTY interagieren. In diesem Beitrag geht es jedoch nur um die Unterstützung der Braillezeile. Alle Unterschiede in der Unterstützung von Braillezeilen zwischen Orca und Narrator ergeben sich aus der unterschiedlichen Verwendung der BrlAPI.
{% end %}

BRLTTY ist ein Hintergrundprozess (ein DAEMON), der blinden Benutzer\*innen den Zugriff auf die Linux/Unix-Konsole über eine Braillezeile ermöglicht ([^4]; [^5]). Da es sich um eine quelloffene Software handelt, die die BrlAPI zur Verfügung stellt, wurde BRLTTY für die Braillezeilenunterstützung in andere Screenreader wie Narrator integriert. Für Android gibt es eine App, die von Google Play heruntergeladen werden kann. Diese registriert einen Barrierefreiheitsdienst und eine eigene Tastatur, um die Steuerung von Smartphones mittels Braillezeilen zu ermöglichen.

### Kompatibilität

BRLTTY ist kompatibel mit einer [umfangreichen Liste von Braillezeilen](https://brltty.app/details.html#displays). Dies sowie die [kurze Liste von Systemanforderungen](https://brltty.app/details.html#requirements) ist ein eindeutiger Vorteil von BRLTTY.

### Konnektivität

BRLTTY kann jeweils nur eine Verbindung zu einer Braillezeile handhaben. Es kann jedoch Braillezeilen automatisch erkennen und wählt entweder die erste verfügbare aus (was eine Weile dauern kann) oder stellt schnell eine Verbindung zu einer vordefinierten Braillezeile her. Letzteres hat allerdings einen Nachteil: BRLTTY braucht noch länger, um eine Verbindung herzustellen, wenn die vordefinierte Braillezeile nicht verfügbar ist oder stellt gar keine Verbindung zu einer anderen Braillezeile her.

### Unterstützung für mehrere Sprachen

BRLTTY ist der Ursprung des LibLouis-Projekts, welches die Unterstützung für viele Braille-Tabellen drastisch verbessert (was es einfacher macht, viele Sprachen in Braille zu unterstützen) und auch die Darstellung ungewöhnlicher Symbole wie Emoji verbessert hat ([^6]; [^7]). Folglich bietet BRLTTY eine hervorragende Unterstützung für die sinnvollere Darstellung von Symbolen, die nicht Teil der aktiven Braille-Tabelle sind. Zum Beispiel ist der griechische Buchstabe Γ (Gamma) normalerweise nicht Teil von Braille-Tabellen. Da eine Braillezeile mit 8 Punkten pro Block nur 256 mögliche Kombinationen (sprich: Symbole) darstellen kann, müssen die Ersteller von Braille-Tabellen strategisch auswählen, welche Buchstaben und Symbole für eine bestimmte Sprache besonders wichtig sind. Wenn ein Symbol auftaucht, das die Braille-Tabelle nicht darstellen kann, wird ein Platzhalterzeichen angezeigt, welches für Benutzer\*innen von Braillezeilen schwer zu interpretieren ist. LibLouis zeigt stattdessen den Unicode-Codepunkt des Symbols an - für Gamma würde es `'\x0393'` anzeigen. Natürlich ist es nicht gerade ergonomisch, nachzuschlagen, welches Zeichen zu einem bestimmten Unicode-Codepunkt gehört. Außerdem bedeutet dies, dass ein Symbol acht Blöcke auf der Braillezeile einnimmt, vom ersten bis zum letzten Apostroph. Meiner Meinung nach überwiegen die Vorteile, aber darüber lässt sich streiten.

### Extra

Dank der oben erwähnten Unterstützung für LibLouis kann BRLTTY jedes beliebige Symbol darstellen, unabhängig von der gewählten Braille-Tabelle, auch wenn dies nicht gerade ergonomisch ist. Meines Wissens unterstützt BRLTTY jedoch nicht direkt die Darstellung von wissenschaftlichen Formeln. Die parallele Verwendung mehrerer Braillezeilen ist nicht möglich und schnell zwischen Braillezeilen zu wechseln ist nicht einfach. Es gibt Befehle, um auf die Schriftattribute eines Textes zuzugreifen, allerdings gibt es keine Möglichkeit, diese im Text selbst aufzurufen, ohne den Befehl aktiv auszulösen. Wie zuverlässig BRLTTY Informationen meldet, hängt weitgehend davon ab, wie gut die BrlAPI genutzt wird, z.B. um Systembenachrichtigungen zu melden.

### Anpassbarkeit und Erweiterbarkeit

BRLTTY erlaubt die Anpassung verschiedener Einstellungen sowie die Änderung von Tastaturbelegungen für Befehle. Dies erfordert jedoch oft das Ablegen von Dateien an einer bestimmten Stelle und ist daher nicht ganz einfach. Da BRLTTY quelloffen ist und die BrlAPI anbietet, ist es technisch auch möglich, die Funktionalität von BRLTTY nach Bedarf zu erweitern oder zu ändern. Dokumentation, die dies erleichtert, ist online verfügbar. Man muss allerdings ein oder eine ziemlich erfahrene\*r Programmierer\*in sein, um dies tatsächlich tun zu können.

## JAWS

Job Access With Speech (JAWS) ist ein kommerzieller Screenreader für Windows ([^8]). Wie VoiceOver handelt es sich um ein proprietäres Programm, dessen Quellcode der Öffentlichkeit nicht zugänglich gemacht wird. Der Preis ist hoch, aber es ist auch zweifellos der Screenreader mit dem größten Funktionsumfang auf dem Markt. Aber ist er deshalb auch der Braillezeilen-freundlichste?

### Kompatibilität

Meines Wissens veröffentlicht Freedom Scientific keine umfassende Liste der unterstützten Braillezeilen. Es werden zwar die Braillezeilen aufgelistet, die direkt bei der Installation unterstützt werden, aber JAWS ist in der Lage, mit weitaus mehr Braillezeilen zu kommunizieren, sobald es ordnungsgemäß installiert ist ([^9]). JAWS sollte mit den meisten modernen Braillezeilenmodellen kompatibel sein, auch wenn je nach Modell möglicherweise zusätzliche Treiber installiert werden müssen.

### Konnektivität

Normalerweise kann JAWS nur mit einer aktiven Braillezeile gleichzeitig arbeiten. Es gibt jedoch eine Einstellung zur automatischen Erkennung von Braillezeilen über Bluetooth. Wenn diese Einstellung aktiviert ist, kann JAWS eine Verbindung zu einer Braillezeile über Bluetooth und zu einer zweiten über USB herstellen. Ein Problem mit dem von Narraotr geladenen Braille-Treiber (LibUSB) führt zu Verbindungsproblemen über USB mit vielen oder allen Braillezeilen. Dies ist jedoch nicht die Schuld von JAWS und sollte von Microsoft behoben werden ([^10]).

### Unterstützung mehrerer Sprachen

Auch hier gilt: Freedom Scientific veröffentlicht keine leicht zugängliche Liste der unterstützten Braille-Tabellen. JAWS listet jedoch über 110 Braille-Profile auf, von denen viele auch weniger bekannte Sprachen betreffen, so dass man mit Sicherheit sagen kann, dass die Unterstützung für mehrere Sprachen hervorragend ist. Beachten Sie jedoch, dass JAWS keine Möglichkeit bietet, Symbole darzustellen, die nicht Teil der aktiven Braille-Tabelle sind.

### Extra

JAWS spricht Emoji, stellt sie aber nicht in Braille dar. Emoji und Icons führen in der Regel nur zu einer Leerstelle auf der Braillezeile; Ungewöhnliche Symbole können auch zu einer Art "nicht darstellbar" Platzhalter führen, wie z.B. ein Vollzeichen. JAWS verfügt zwar über ein Kontrollkästchen zum Aktivieren der LibLouis-Unterstützung in den Braille-Profileinstellungen, aber diese Einstellung wird in Sprachen wie Deutsch nicht wirksam und bleibt auch nach dem Schließen des Einstellungsdialogs nicht erhalten. Ich verstehe nicht, warum.
Was wissenschaftliche Notationen und Mathematik betrifft, so verfügt JAWS über einen sogenannten "mathematischen Betrachter" ([^11]). Seine Unterstützung für Braillezeilen ist allerdings enttäuschend schlecht: Wenn Sie auf eine mathematische Formel stoßen, zeigt die Braillezeile nur "math" an. Wenn der mathematische Betrachter geöffnet wird (was übrigens alles andere als ergonomisch ist), bleibt die Braillezeile leer -- es sei denn, die aktive Braille-Tabelle ist Englisch: In diesem Fall wird der Nemeth-Code für die Formel angezeigt. Seit JAWS 2024 gibt es auch eine experimentelle Unterstützung für MathCAT. Dies verbessert den mathematischen Betrachter jedoch nur geringfügig, da die volle Funktionalität von MathCAT nicht genutzt werden kann. MathCAT unterstützt Sprachausgabe in mehreren Sprachen und kann Formeln in mehreren Notationen darstellen, darunter Nemeth, ASCIIMath und LaTeX ([^12]; [^13]). Bislang nutzt JAWS nur einen Bruchteil dieser Funktionen -- und auch das nur, wenn Sie die experimentelle Funktion aktivieren.
JAWS leistet gute Arbeit, wenn es darum geht, alle gesprochenen Informationen auch in Brailleschrift zugänglich zu machen. Zusätzlich zur Darstellung der meisten Informationen in Brailleschrift gibt es auch ein Protokoll (Sprachverlauf genannt). Dieses listet die letzten 100 Dinge auf, die JAWS gesagt hat, so dass Sie diese auf Ihrer Braillezeile überprüfen können.
Außerdem stehen verschiedene Befehle zur Verfügung, um Informationen über die Schriftart abzufragen. Sie können einstellen, wie JAWS beispielsweise unterstrichenen oder farbigen Text darstellen soll, wobei Sie nur festlegen können, wie solcher Text "unterstrichen" werden soll. Dadurch sind die möglichen Variationen begrenzt. JAWS bietet einen speziellen Modus, in dem Schriftattribute anzeigt werden, aber Sie müssen explizit in diesen Modus wechseln. Der Befehl zur Ausgabe von Informationen über Vorder- und Hintergrundfarbe führt manchmal zu verwirrenden Ausgaben wie "Standard auf Standard". Insgesamt würde ich sagen, dass das Programm nicht besonders ergonomisch ist, aber es erfüllt in der Regel seinen Zweck.
Das Arbeiten mit mehreren Braillezeilen ist möglich, aber auch nicht ergonomisch. Man kann nur eine Standard-Braillezeile definieren und andere Anzeigen können nur über Bluetooth verbunden werden, wenn die Einstellung für die automatische Erkennung aktiviert ist. Sie können die Standard-Braillezeile relativ schnell ändern, müssen JAWS aber normalerweise neu starten, damit die Änderung wirksam wird.

### Anpassungen und Erweiterbarkeit

JAWS verfügt über eine Vielzahl von Einstellungen, so dass es ziemlich aufwendig sein kann, die von Ihnen gesuchte Einstellung zu finden. Dies und die Möglichkeit, die Einstellungen pro Anwendung und sogar pro Webseite zu ändern, macht JAWS jedoch besonders anpassbar und flexibel. Außerdem ist JAWS erweiterbar: Sie können Skripte in einer speziellen Skriptsprache schreiben, um die Fähigkeiten von JAWS zu erweitern -- zum Beispiel, um eine Anwendung barrierefreier zu machen. Das Problem mit diesem Ansatz wird allerdings in den GitHub-Repositories für JAWS-Skripte deutlich ([^14]). Da es keinen offiziellen Marktplatz oder ein Repository für JAWS-Skripte gibt, gibt es auch keine zentrale Qualitätskontrolle, keine Kompatibilitätsprüfung und keine bequeme Möglichkeit, Skripte zu importieren, zu aktualisieren und zu entfernen. Dies bleibt dem/der Benutzer\*in oder möglicherweise dem/der Entwickler\*in der Skripte überlassen. Da JAWS eine eigene Skriptsprache verwendet, sind die Entwicklungswerkzeuge für solche Skripte zudem recht begrenzt.

## NVDA

Non-Visual Desktop Access (NVDA) ist, wie JAWS, ein Screenreader für Windows. Im Gegensatz zu JAWS ist es kostenlos und quelloffen. JAWS ist zwar immer noch beliebter als NVDA, aber letzteres hat in den letzten vier Jahren an Popularität gewonnen, während JAWS an Boden verloren hat ([^15]; [^16]). Ein Grund dafür ist natürlich, dass NVDA ein kostenloses Programm ist -- jeder kann es ohne weiteres herunterladen und installieren, ohne für Lizenzen bezahlen zu müssen. Außerdem testen viele Unternehmen, die nicht bereit sind, für die JAWS-Barrierefreiheits-Testsuite von Freedom Scientific zu bezahlen, die Barrierefreiheit mit NVDA. Dies kann zu einer besseren Unterstützung für diesen Screenreader führen. Ist die Braillezeilenfreundlichkeit von NVDA so gut wie seine wachsende Popularität vermuten lässt?

### Kompatibilität

NV Access veröffentlicht [eine umfassende Liste von Braillezeilen, die von NVDA unterstützt werden](https://www.nvaccess.org/files/nvda/documentation/userGuide.html#SupportedBrailleDisplays). Diese sollte die meisten modernen Braillezeilenmodelle enthalten.

### Konnektivität

NVDA kann die meisten der unterstützten Braillezeilen automatisch erkennen. Das macht die Verbindung mit ihnen sowohl über USB als auch über Bluetooth sehr einfach. Wie JAWS kann NVDA Probleme mit der Verbindung über USB haben, wenn Narrator LibUSB installiert hat.

### Unterstützung mehrerer Sprachen

NVDA verwendet ebenfalls LibLouis. Meiner Einschätzung nach unterstützt es nicht ganz so viele Braille-Tabellen wie JAWS (obwohl bereits viele verfügbar sind), aber zusätzliche Braille-Tabellen können manuell hinzugefügt werden. Es bietet auch eine Option, mit der Sie versuchen können, Unicode-Zeichen zu normalisieren: Dies ist wichtig, weil einige Symbole, wie z.B. der deutsche Buchstabe "ü", im Unicode auf mehr als eine Weise dargestellt werden können. Wenn sie nicht normalisiert werden, kann dies zu einer inkonsistenten Darstellung solcher Buchstaben auf einer Braillezeile führen; die Normalisierung von Unicode-Symbolen kann jedoch in einigen Sprachen unerwünschte Nebeneffekte haben. Die Vor- und Nachteile von LibLouis sind dieselben wie bei BRLTTY.

### Extra

NVDA selbst unterstützt nur die Funktionen von LibLouis zur Anzeige ungewöhnlicher Symbole wie Emoji. Mit dem Zusatzprogramm BrailleExtender für NVDA ([^17]) ist es jedoch möglich, anstelle des Unicode-Codepunkts eine Textbeschreibung des Zeichens anzuzeigen. Der griechische Buchstabe Gamma würde dann als `[Gamma]` anstelle von `'\x0393'` erscheinen.
Im Standard-NVDA können die meisten Informationen, die gesprochen werden, bereits über eine Braillezeile abgerufen werden, aber nicht alle. Auch hier hilft BrailleExtender, die Lücken zu schließen. Die Erweiterung ermöglicht es, viel mehr Informationen direkt auf der Braillezeile anzuzeigen, die sonst über einen Befehl abgefragt werden müssten oder gar nicht verfügbar wären. Das Add-on fügt auch einen verbesserten Sprachverlauf hinzu, ähnlich wie bei JAWS.
Die Unterstützung für wissenschaftliche Formeln ist nicht von Haus aus in NVDA enthalten, kann aber über ein MathCAT Add-on ([^18]) hinzugefügt werden. Im Gegensatz zu JAWS macht dieses Add-on vollen Gebrauch von allen verfügbaren MathCAT-Funktionen, was bedeutet, dass wissenschaftliche Formeln sehr barrierefrei sind, insbesondere auf Braillezeilen.
Auch wenn das Umschalten zwischen Braillezeilen unter NVDA aufgrund der automatischen Erkennung der angeschlossenen Braillezeilen recht einfach ist, macht das BrailleExtender Add-on diesen Prozess noch ergonomischer. Es ermöglicht die Speicherung von zwei Braillezeilen anstelle von einer. Dies bedeutet, dass NVDA sich mit zwei Braillezeilen parallel verbinden kann, weswegen man insgesamt seltener zwischen Braillezeilen wechseln muss.
Zusätzlich kann BrailleExtender auch spezifische Tags zu Text hinzufügen, um beispielsweise zu signalisieren, ob er fett ist, ob ein Rechtschreibfehler vorliegt, welche Schriftfamilie verwendet wird, was die aktuelle Seitenzahl ist und vieles mehr. Wenn jedoch all diese Informationen aktiviert sind, kann dies schnell überfordern, da dem normalen Text zu viele Tags hinzugefügt werden. Durch strategisches Aktivieren oder Deaktivieren einiger Einstellungen, falls nötig auf Anwendungsbasis, können Sie die zusätzlichen Informationen jedoch zielführend nutzen.

### Anpassbarkeit und Erweiterbarkeit

Wie JAWS ist auch NVDA sehr anpassbar. Standardmäßig verfügt es über weniger Anpassungsoptionen als JAWS, aber die Anzahl der Optionen kann steigen, wenn mehr Add-Ons zu NVDA hinzugefügt werden. Es unterstützt auch die Verwendung verschiedener Profile für die aktiven Einstellungen. Diese können auch durch bestimmte Ereignisse aktiviert werden, z.B. wenn ein bestimmtes Programm in den Vordergrund geholt wird. Wie bereits erwähnt, können Sie das Verhalten von NVDA durch in Python geschriebene Add-Ons erweitern. Im Gegensatz zu JAWS verfügt NVDA über einen offiziellen Marktplatz für Add-Ons und macht es außerdem einfach zu sehen, mit welchen Versionen von NVDA ein Add-On getestet wurde. Da die Programmiersprache Python ist, stehen außerdem viele Entwicklertools zur Verfügung, die die Qualität und Wartbarkeit der Add-ons verbessern können.

## VoiceOver

VoiceOver ist der Screenreader von Apple für iOS, iPadOS und MacOS. Im Gegensatz zu anderen Betriebssystemen ist es der einzige Screenreader, der auf diesen Plattformen verwendet werden kann. Wie sieht es mit der Braillezeilenfreundlichkeitunter VoiceOver aus?

### Kompatibilität

Auch wenn Apple keine umfassende Liste der von iOS oder iPadOS unterstützten Braillezeilen veröffentlicht, gibt es eine Liste der von MacOS unterstützten Braillezeilen ([^19]). Es ist zwar nicht garantiert, dass alle Braillezeilen auch unter iOS unterstützt werden -- man kann sogar davon ausgehen, dass zumindest diejenigen, die Bluetooth nicht unterstützen, sich nicht mit einem iPhone oder iPad verbinden lassen --, aber die Liste ist dennoch umfangreich, so dass die Kompatibilität gut sein sollte.

### Konnektivität

Wie alle anderen Bildschirmleser unterstützt VoiceOver Verbindungen über USB und Bluetooth. Im Gegensatz zu JAWS, NVDA und BRLTTY unterstützt es jedoch nicht die automatische Erkennung von Braillezeilen über Bluetooth: Es stellt immer eine Verbindung zur ausgewählten Braillezeile oder zu keiner Braillezeile her, auch wenn eine Bluetooth- und eine USB-Verbindung zur gleichen Braillezeile parallel bestehen kann.

### Unterstützung mehrerer Sprachen

Apple stellt keine umfassende Liste der unterstützten Brailletabellen zur Verfügung. Eine Prüfung der unterstützten Brailletabellen in den VoiceOver-Einstellungen lässt jedoch vermuten, dass viele oder alle LibLouis-Brailletabellen zusätzlich zu einigen anderen unterstützt werden. Auf Deutsch ist die Standard-Brailletabelle beispielsweise nicht die LibLouis-Brailletabelle für Deutsch, sondern eine andere, die von Apple definiert wurde. Da die LibLouis-Brailletabellen jedoch verfügbar sind, sollte die Unterstützung für mehrere Sprachen mindestens so gut sein wie bei anderen Screenreadern, die auf LibLouis basieren -- wie BRLTTY und NVDA.

### Extra

Apple hat bei der Darstellung von Emoji Pionierarbeit geleistet, indem es deren Unicode-Beschreibung zur Darstellung von Emoji auf Braillezeilen verwendet hat. Das führt dazu, dass :heart: zum Beispiel als `:red heart:` dargestellt wird. In den Standard-Brailletabellen von Apple kann es jedoch zu einem ähnlichen Verhalten wie bei JAWS kommen: Unbekannte Symbole werden durch ein Platzhaltersymbol ersetzt. In LibLouis-Tabellen gelten die gleichen Regeln wie unter BRLTTY.
Früher gab es viele Dinge, die VoiceOver sagte, aber nicht in Braille darstellte. Das ist vor allem unter MacOS immer noch der Fall, aber die Lücke ist kleiner geworden. Ein Nachteil ist, dass VoiceOver dem Benutzer nur die letzten zehn Meldungen zur Verfügung stellt und nicht über ein umfassenderes Sprachprotokoll verfügt. Insgesamt ist der Informationsverlust, wenn man sich nur auf die Braille-Schrift verlässt, wahrscheinlich höher als bei NVDA und JAWS.
VoiceOver verfügt über eine Option zur Darstellung mathematischer Formeln als Nemeth-Code. Diese Option funktioniert bei älteren VoiceOver-Versionen allerdings nur, wenn die aktive Braille-Tabelle Englisch ist. In anderen Sprachen sind dann die Zeilen, die sonst mathematische Formeln enthalten würden, einfach leer. Das macht wissenschaftliche Formeln ziemlich unbarrierefrei, besonders wenn man mit Nemeth nicht vertraut ist. Schnelles Umschalten zwischen Brailletabellen ist zwar über den Rotor möglich, aber dies ist dennoch nicht ergonomisch.
VoiceOver verfügt wie JAWS über Befehle, die Informationen über die aktuelle Schriftart liefern können. So können beispielsweise unterstrichene oder fettgedruckte Wörter hervorgehoben werden, aber es bleibt ähnlich unklar wie bei JAWS, was die genauen Schriftattribute sind, wenn man diese Information nicht ausdrücklich über Befehle abfragt.
Die parallele Verwendung mehrerer Braillezeilen ist mit VoideOver nicht möglich, da jeweils nur eine Braillezeile aktiv sein kann. Die Ausnahme von dieser Regel ist, wenn eine Braillezeile über USB und eine andere über Bluetooth angeschlossen ist.
Eine einzigartige Funktion von VoiceOver ist die "Bildschirmerkennung", die KI nutzt, um die sichtbaren Elemente auf dem Bildschirm barrierefreier zu machen und so die Barrierefreiheit von Apps insgesamt zu verbessern. Diese Funktion ist allerdings notwendig, da man VoiceOver nicht mit Skripten erweitern kann.

### Anpassbarkeit und Erweiterbarkeit

VoiceOver lässt sich relativ gut anpassen, insbesondere unter MacOS über das VoiceOver-Dienstprogramm. Es ist jedoch nicht erweiterbar, auch wenn man möglicherweise Schnellaktionen schreiben kann, um einige ansonsten komplizierte Aktionen zu vereinfachen; dies ist jedoch nicht Screenreader-spezifisch.

## Fazit

Welcher Screenreader ist also am besten für die Braillezeile geeignet? Meiner Meinung nach -- und ich hoffe, Sie können nachvollziehen, warum -- ist es NVDA -- vorausgesetzt, Sie installieren die Add-Ons BrailleExtender und MathCAT. Die gute Nachricht ist, dass alle hier erwähnten Screenreader ein gutes Maß an Unterstützung für Braillezeilen bieten. Der genaue Nutzen, den Sie aus einem bestimmten Screenreader ziehen, hängt immer von Ihren speziellen Bedürfnissen und den von Ihnen verwendeten Programmen ab.

## Literatur

[^1]: Screenreader, 2023. Wikipedia [online]. [Zugriff am: 2 März 2025]. Verfügbar unter: <https://de.wikipedia.org/w/index.php?title=Screenreader&oldid=240187790>

[^2]: Results of the survey “Braille Display Usage” – livingbraille.eu, 2024. [online]. [Zugriff am: 30 Dezember 2024]. Verfügbar unter: <https://www.livingbraille.eu/results-of-the-survey-braille-display-usage/>

[^3]: Live Regions are Not Displayed in Braille · Issue #7756 · nvaccess/nvda, [kein Datum]. [online]. [Zugriff am: 31 Dezember 2024]. Verfügbar unter: <https://github.com/nvaccess/nvda/issues/7756>

[^4]: BRLTTY - Details, [kein Datum]. [online]. [Zugriff am: 1 Januar 2025]. Verfügbar unter: <https://brltty.app/details.html>

[^5]: BRLTTY - Official Home, [kein Datum]. [online]. [Zugriff am: 1 Januar 2025]. Verfügbar unter: <https://brltty.app/>

[^6]: liblouis/liblouis, 2024. [online]. C. liblouis. [Zugriff am: 1 Januar 2025]. Verfügbar unter: <https://github.com/liblouis/liblouis>

[^7]: DEVINPRATER, 2022. By the Blind, For the Blind. [online]. 22 Juni 2022. [Zugriff am: 1 Januar 2025]. Verfügbar unter: <https://write.as/devinprater/by-the-blind-for-the-blind>

[^8]: JAWS® – Freedom Scientific, [kein Datum]. [online]. [Zugriff am: 2 Januar 2025]. Verfügbar unter: <https://www.freedomscientific.com/products/software/jaws/>

[^9]: Technical Support: What Braille displays does JAWS support and are any unsupported displays emulated?, [kein Datum]. [online]. [Zugriff am: 2 Januar 2025]. Verfügbar unter: <https://support.freedomscientific.com/support/technicalsupport/bulletin/946>

[^10]: Narrator braille driver still conflicts with all other braille drivers in Windows 11, but there is hope, [kein Datum]. [online]. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://jfw.groups.io/g/main/topic/narrator_braille_driver_still/86297169>

[^11]: Accessing Math Content with JAWS and Fusion – Freedom Scientific, [kein Datum]. [online]. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://www.freedomscientific.com/training/teachers/accessing-math-content-with-jaws-and-fusion/>

[^12]: NSOIFFER, 2025. NSoiffer/MathCAT [online]. Rust. 4 Januar 2025. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://github.com/NSoiffer/MathCAT>

[^13]: MathCAT: Math Capable Assistive Technology, [kein Datum]. [online]. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://nsoiffer.github.io/MathCAT/>

[^14]: MAZRUI, Jamal, 2023. jamalmazrui/JAWS_Scripts [online]. 7 Mai 2023. [Zugriff am: 1 Januar 2025]. Verfügbar unter: <https://github.com/jamalmazrui/JAWS_Scripts>

[^15]: WebAIM: Screen Reader User Survey #9 Results, [kein Datum]. [online]. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://webaim.org/projects/screenreadersurvey9/>

[^16]: WebAIM: Screen Reader User Survey #10 Results, [kein Datum]. [online]. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://webaim.org/projects/screenreadersurvey10/>

[^17]: CLAUSE, André-Abush, 2024. AAClause/BrailleExtender [online]. Python. 25 Dezember 2024. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://github.com/AAClause/BrailleExtender>

[^18]: NSOIFFER, 2024. NSoiffer/MathCATForPython [online]. Python. 23 Dezember 2024. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://github.com/NSoiffer/MathCATForPython>

[^19]: USB and Bluetooth braille displays supported by macOS, [kein Datum]. [online]. [Zugriff am: 4 Januar 2025]. Verfügbar unter: <https://support.apple.com/en-gb/guide/voiceover/cpvobrailledisplays/mac>

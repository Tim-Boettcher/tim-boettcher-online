+++
title = "So richten Sie Zola ein, um von überall aus zu bloggen"
date = 2023-11-17
updated = 2023-11-19
template = "page.html"

[taxonomies]
tags = ["Android", "bloggen", "Einrichtung", "GNU-Linux", "iOS", "Tutorial", "Windows", "Zola"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
toc_levels = 2
isso = true
social_media_card = "img/social_cards/de_blog_zola_setup_to_blog_from_anywhere.jpg"
+++

[Die Verwendung eines statischen Website-Generators zum Bloggen](@/blog/static-site-generator-for-blogging.de.md) hat viele Vorteile. Allerdings gibt es auch einen großen Nachteil: Die Vorschau und Veröffentlichung von Blogbeiträgen auf verschiedenen Plattformen funktioniert nicht ohne Weiteres. Dieser Artikel soll Ihnen zeigen, wie Sie verschiedene Plattformen konfigurieren können, um bequem von überall aus zu bloggen.<!-- more -->

## Inhaltsverzeichnis

<!-- toc -->

## Bevor wir loslegen...

Zunächst sind wohl ein paar Haftungsausschlüsse fällig.

- Auch wenn das Bloggen auf den jeweiligen Plattformen bequem sein mag, sobald die Einrichtung abgeschlossen ist, bedeutet das nicht, dass dieser Prozess einfach ist. Ich habe viel ausprobiert und bin zwar zuversichtlich, dass ich in den meisten Fällen gute Lösungen gefunden habe, aber es gibt vielleicht noch Verbesserungspotenzial. Gerne können Sie konstruktives Feedback als Kommentar posten oder sich direkt an mich wenden.
- Ich verwende das [*Tabi*-Theme](https://github.com/welpo/tabi) für meine Website und einige der unten beschriebenen Schritte sind spezifisch für Tabi. Ich werde diese mit *Nur Tabi* kennzeichnen.
- Die *GNU Linux* Distributionen, die ich verwende, sind *Ubuntu* und *Alpine*. Im Allgemeinen bin ich der Meinung, dass meine Anweisungen auch für andere Distributionen gültig sind -- wenn Sie jedoch bei anderen Distributionen auf Probleme stoßen, die über das Herausfinden eines äquivalenten Befehls hinausgehen, kann ich Ihnen nicht helfen.
- Dieser Beitrag ist absichtlich lang und an einigen Stellen redundant. Sie können die Abschnitte für Plattformen, an denen Sie nicht interessiert sind, getrost überspringen.

Damit hätten wir das erledigt... Auf gehts!

## Vorraussetzungen

Dieses Tutorial geht davon aus, dass Ihre Website *Zola* in der Version `0.17.0` oder höher verwendet und dass der Quellcode Ihrer Website in ein Repository auf einem *Versionskontrollsystem (VCS)* wie *GitHub*, *GitLab* oder ähnlichem veröffentlicht wird. Ihr VCS sollte auch *Webhooks* unterstützen, da wir auf diese Weise den Aktualisierungs- und Veröffentlichungsprozess auf dem Server automatisieren werden.

## Server

### Das System vorbereiten

Lassen Sie uns mit dem einfachen Teil beginnen. Auf meinem Server läuft Ubuntu 20.04 LTS -- wenn Sie eine andere Distribution verwenden, müssen Sie möglicherweise andere Befehle eingeben; der Kern sollte jedoch identisch sein.

```bash
sudo apt update
sudo apt upgrade
sudo apt install build-essential software-properties-common git
```

Damit werden verschiedene Bibliotheken und Werkzeuge installiert, die Sie wahrscheinlich benötigen, um Zola für das Bloggen einzurichten. Vor allem wird *git* installiert, das Sie benötigen, um neue Versionen des Quellcodes Ihrer Website herunter zu laden.

### Zola installieren

Einige Distributionen stellen Zola direkt in einem offiziellen Repository zur Verfügung; wenn Ihr Server *Snapcraft* unterstützt, können Sie auch [Zola mit snap installieren](https://www.getzola.org/documentation/getting-started/installation/#snapcraft); es sind auch [*Docker*-Images](https://www.getzola.org/documentation/getting-started/installation/#docker) verfügbar. Nicht zuletzt bietet die Zola-Website spezifische [Installationsanweisungen für verschiedene Plattformen](https://www.getzola.org/documentation/getting-started/installation/).

Ich habe mir Zola jedoch manuell aus den GitHub-Releases besorgt:

```bash
wget -O zola.tar.gz https://github.com/getzola/zola/releases/download/v0.17.2/zola-v0.17.2-x86_64-unknown-linux-gnu.tar.gz
tar -xvf zola.tar.gz
sudo mv zola /bin
sudo chmod +x /bin/zola
```

(Sie können das Programm natürlich auch an einen anderen Ort verschieben -- wichtig ist nur, dass sie sich in Ihrem `PATH` befindet).

`zola --version` sollte nun die Version von Zola ausgeben, die Sie installiert haben (in meinem Fall `0.17.2`). Wenn Sie stattdessen eine Fehlermeldung wie `Befehl nicht gefunden` oder `Datei oder Verzeichnis existiert nicht` erhalten, fehlt Ihnen eine Bibliothek, die Zola zur Ausführung benötigt. Die einfachste Lösung hierfür ist meiner Meinung nach die Installation von Zola aus dem Quellcode.

#### Installation von Zola aus dem Quellcode

*Hinweis:* Wenn Sie Zola bereits erfolgreich installiert und ausgeführt haben, können Sie diesen Schritt überspringen.

Zola ist in [*Rust*](https://rust-lang.org/) geschrieben, daher ist der erste Schritt zur Installation von Zola aus dem Quellcode die Installation von Rust. Der empfohlene Weg dazu ist mittels [*rustup*](https://rustup.rs/):

```bash
curl https://sh.rustup.rs -sSf | sh
```

Geben Sie einfach `1` und `ENTER` ein, um mit der Standardinstallation fortzufahren. Sobald diese abgeschlossen ist, führen Sie den folgenden Befehl aus:

```bash
source ~/.cargo/env
```

Als nächstes klonen Sie den Zola-Quellcode, wählen die Version `0.17.2` aus (oder welche Version Sie auch immer installieren wollen) und installieren diese dann mit *Cargo*:

```bash
git clone https://github.com/getzola/zola.git
cd zola/
git checkout v0.17.2
cargo install --path .
```

Dieser Vorgang wird eine Weile dauern, aber sobald er abgeschlossen ist, sollte Zola installiert und einsatzbereit sein. Sie können dies mit `zola --version` überprüfen.

### Einrichten des Website-Quelltexts

Nach der Installation von Zola können wir nun mit der Einrichtung der eigentlichen Website beginnen. Wechseln Sie zunächst in das Verzeichnis, in welches Sie den Quelltext klonen möchten, und führen Sie dann folgende Befehle aus:

```bash
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
```

Wenn Sie ein Theme verwenden, haben Sie es wahrscheinlich als *Git-Submodul* hinzugefügt - das ist jedenfalls die empfohlene Art, es einzurichten. Wenn Sie den Quelltext Ihrer Website zum ersten Mal klonen, ist das Submodul noch nicht eingerichtet. Dies würde zu einem Fehler führen, wenn Sie jetzt versuchen würden, Ihre Website mit Zola zu erstellen. Führen Sie die folgenden Befehle aus, um dies zu korrigieren:

```bash
git submodule update --init
```

Außerdem wird das Submodul (d.h. das Theme) wahrscheinlich veraltet sein, sodass Sie auch hier die neueste Version herunterladen müssen:

```bash
cd themes/<theme-name>/
git pull origin main
cd ../../
```

Jetzt können Sie sehen, ob Ihre Website korrekt generiert wird:

```bash
zola build
```

Machen Sie das erzeugte Verzeichnis `public/` Ihrem Webserver zugänglich und Sie sollten Ihre Website wie beabsichtigt aufrufen können. Puh!

### :robot: Automatisieren!

Bisher ist der Prozess der Veröffentlichung Ihrer Website sehr manuell:

- Wann immer eine neue Version Ihrer Website verfügbar ist, müssen Sie den Quelltext aus Ihrem Repository herunter laden,
- Sie müssen auch die neueste Version Ihres Themes herunterladen
- und dann `zola build` ausführen.

Normalerweise würde man ein Problem wie dieses mit *kontinuierlicher Integration (CI)* lösen. Wenn Sie Ihre Website auf *GitHub Pages* oder ähnlichen Diensten hosten, hat [Zola vielleicht sogar schon fertige Skripte für Sie](https://www.getzola.org/documentation/deployment/overview/), die den Bereitstellungsprozess automatisieren. Ich verwende jedoch *Codeberg*, das derzeit *Woodpecker CI* für diese Art der Automatisierung verwendet. Ein CI-Runner für *Forgejo*, die Softwarelösung, die Codeberg zugrunde liegt, [ist derzeit in Alpha-Release-Qualität verfügbar](https://code.forgejo.org/forgejo-integration/runner). Allerdings würde ich von der Verwendung vorerst abraten. Stattdessen bin ich über Webhooks gestolpert. Sie sind recht einfach einzurichten und senden in der Regel einfach eine `POST`-Anfrage mit einigen Daten an eine URL Ihrer Wahl, wenn ein bestimmtes Ereignis eintritt -- z.B. wenn Änderungen in einen bestimmten Branch gepusht werden.

Was bei der von Ihnen angegebenen URL passiert, wenn die Anfrage eingeht, liegt ganz bei Ihnen. Wenn Sie bereits *PHP* oder eine andere Skriptsprache auf Ihrem Server installiert haben, können Sie ein Skript ausführen, das die Änderungen automatisch in Ihre lokale Kopie des Website-Quelltexts zieht und die Website erstellt. Alternativ dazu gibt es spezielle Softwarelösungen für die Handhabung von Webhooks, mit denen Sie Shell-Skripte ausführen können, wenn ein Webhook ausgelöst wird. Óscar Fernandez [erklärt diesen Ansatz auf osc.garden](https://osc.garden/blog/updating-site-with-webhook/). Unabhängig davon, welchen Ansatz Sie verwenden, sollten Sie sicher stellen, dass Sie die Authentizität der Anfrage überprüfen und die Anfragen loggen. Andernfalls wird es schwierig, herauszufinden, warum Ihre Website nicht wie beabsichtigt automatisch aktualisiert wurde.

## Windows

### Das System vorbereiten

Der einzige Schritt, den Sie hier unbedingt unternehmen müssen, ist die Installation von [git für Windows](https://gitforwindows.org/). Auf diese Weise können Sie das Repository des Quelltexts Ihrer Website direkt klonen, so wie Sie es auf dem Server getan haben.

### Zola installieren

Die [Installationsanleitung auf der Zola-Website](https://www.getzola.org/documentation/getting-started/installation/#windows) empfiehlt zwar *Chocolatey* oder *Scoop* für die Installation von Zola unter Windows, aber zumindest das Chocolatey-Paket ist veraltet. Stattdessen können Sie [die neueste Zola-Version von der Releaseseite auf GitHub herunterladen](https://github.com/getzola/zola/releases/), sie entpacken und das Programm an einen Ort wie `C:\Programme\zola` kopieren. Dann müssen Sie dieses Verzeichnis zur Umgebungsvariablen `PATH` hinzufügen:

1. Drücken Sie die Windows-Taste und geben Sie `Umgebung` in die Suchleiste ein.
2. Wählen Sie `Umgebungsvariablen für diesen Benutzer bearbeiten` oder `Systemumgebungsvariablen bearbeiten`, je nachdem, was Sie wollen (letzteres erfordert Administratorrechte und macht Zola für jeden Benutzer verfügbar).
3. Wählen Sie die Variable `Path` in der Liste der Variablen aus und klicken Sie auf `Bearbeiten`.
4. Klicken Sie auf `Neu` und dann auf `Durchsuchen`.
5. Es öffnet sich eine Ordnerauswahl. Wählen Sie den Ordner aus, den Sie der Umgebungsvariablen `PATH` hinzufügen möchten, z.B. `C:\Programme\zola`.
6. Klicken Sie auf `OK` und dann erneut auf `OK`, um das Fenster mit den Umgebungsvariablen zu schließen.
7. Schließen Sie Ihre PowerShell-Sitzungen und öffnen Sie sie erneut.

Zola sollte nun verfügbar sein. Sie können dies testen, indem Sie `zola --version` ausführen. Es sollte (zum Veröffentlichungszeitpunkt) `v0.17.2` ausgegeben werden.

### Einrichten des Website-Quelltexts

Da Zla nun auch unter Windows funktioniert, sollten wir den Quelltext einrichten, zum Beispiel in Unterverzeichnissen von `C:\Users\<Benutzername>\zola`:

```powershell
cd C:\Users\<Benutzername>
New-Ite -path zola -ItemType Directory
cd .\zola
git clone https://your-vcs.tld/your-username/your-website.git
cd .\your-website
```

Wenn Sie ein Theme für Ihre Website verwenden, haben Sie dieses wahrscheinlich als Git-Submodul eingerichtet, welches auch auf diesem System initialisiert und auf die neueste Version aktualisiert werden muss:

```powershell
git submodule update --init
cd .\themes\theme-name
git pull origin main
cd ..\..\
```

### Pre-Commit-Hook 

Es gibt verschiedene Aufgaben, die ein traditionelles *Content Management System* wie *WordPress* für uns erledigen würde, wie z.B. das automatische Ändern des Aktualisierungsdatums, wenn Änderungen im Inhalt auftreten. Um diesen Prozess für uns weniger mühsam zu gestalten, können wir einen *Git Pre-Commit-Hook* verwenden. Ein solcher Hook ist ein Skript, das ausgeführt wird, bevor ein Commit in einem lokalen Repository vorgenommen wird. Es kann verwendet werden, um einen Commit zu überprüfen oder zu ändern und diesen abzubrechen, falls Probleme auftreten.  
Wahrscheinlich möchten Sie, dass dieser Pre-Commit-Hook auf jeder Plattform, auf der Sie Blogbeiträge schreiben möchten, in Ihr Repository integriert ist. Daher ist es sinnvoll, den Pre-Commit-Hook in einer Datei innerhalb der Repository-Struktur zu speichern, was bedeutet, dass wir auch eine Git-Konfiguration für das Repository ändern müssen:

```powershell
New-Item -path .githooks -ItemType Directory
New-Item -path .githooks\pre-commit -ItemType File
git config core.hooksPath .githooks
```

Welche Dinge könnten Sie sinnoller Weise in einen Pre-Commit-Hook für ein Zola-Projekt aufnehmen?

Für Inspiration lohnt sich ein Blick auf [diesen Artikel auf osc.garden](https://osc.garden/blog/zola-date-git-hook/), der zugegebenermaßen einige Tabi-spezifische Automatisierungen enthält, aber auch einige allgemein nützliche.

Git Bash enthält bereits die meisten Tools, die Sie benötigen, um den Git-Hook erfolgreich auszuführen, wie *grep* oder *awk*. Mindestens eines muss jedoch noch installiert und anschließend zur `PATH`-Variable hinzugefügt werden.

#### Oxipng

Um PNG-Bilder verlustfrei zu komprimieren, haben Sie zwei Optionen: *optipng* oder *oxipng*. oxipng ist die modernere Alternative, die ich sehr empfehle. Sie können die neueste Version von der [GitHub-Releaseseite](https://github.com/shssoichiro/oxipng/releases) herunterladen (zum Veröffentlichungszeitpunkt Version `9.0.0`), das Programm an einen geeigneten Ort verschieben und der Umgebungsvariablen `PATH` hinzufügen, so wie Sie es bei Zola getan haben.

#### *Nur Tabi*: GNU parallel, brotli, fonttools und shot-scraper

*GNU parallel* ist nützlich, wenn Sie dieselbe Aufgabe oder eine Reihe von Aufgaben für mehrere Dateien ausführen müssen, beispielsweise für die *Markdown*-Dateien, die Blogbeiträge enthalten. Anstatt die Aufgabe(n) für jede einzelne Datei nacheinander auszuführen, werden sie für mehrere Dateien parallel ausgeführt. Im Pre-Commit-Hook wird GNU parallel verwendet, um schnell mehrere Screenshots der Website für die Social Cards zu erstellen, was wahrscheinlich ein Tabi-spezifischer Anwendungsfall ist.

Um GNU parallel so zu installieren, dass Git BASH es verwenden kann, folgen Sie [dieser StackOverflow-Antwort](https://stackoverflow.com/a/52451011). Sobald die Installation abgeschlossen ist, sollten Sie GNU parallel einmal ausführen, etwa so:

```bash
parallel echo ::: 1
```

Auf diese Weise kann parallel, wie durch die auf der Konsole ausgegebene Warnung beschrieben, einmal die maximale Befehlslänge nachschauen, was eine Weile dauern kann. Danach verwendet es einen zwischengespeicherten Wert und läuft daher deutlich schneller.

Um die Tabi-spezifischen Teile des Pre-Commit-Hooks auszuführen, müssen Sie außerdem [*Python*](https://www.python.org/downloads/) herunterladen und installieren und dann den folgenden Befehl ausführen:

```powershell
pip install brotli fonttools shot-scraper
```

*brotli* und *fonttools* werden benötigt, um die [benutzerdefinierte Schriftsatz-Untermenge](https://welpo.github.io/tabi/blog/custom-font-subset/) zu generieren, *shot-scraper* wird verwendet, um automatisch Screenshots Ihrer Website für [Social Cards](https://osc.garden/blog/automating-social-media-cards-zola/) zu erstellen. Beide Artikel enthalten Skripte, die Sie unter den Dateinamen `custom_subset` bzw. `social-cards-zola` speichern und dann in ein Verzeichnis verschieben müssen, das Sie der Umgebungsvariablen `PATH` hinzugefügt haben. Damit shot-scraper korrekt funktioniert, müssen Sie außerdem diesen Befehl ausführen:

```powershell
shot-scraper install
```

Dieser installiert eine *Chrome*-Version, die dann mit *Playwright* gesteuert werden kann. Dies sollte unter Windows alles sein, was Sie tun müssen. Wenn `shot-scraper www.google.com` zu einer Fehlermeldung führt, müssen Sie zusätzlich diesen Befehl ausführen, um die Playwright-Abhängigkeiten zu installieren:

```powershell
playwright install-deps
```

Wenn Sie das alles erledigt haben, sollte Windows bequem eingerichtet sein. Sie können dies testen, indem Sie `zola serve --base-url 127.0.0.1` ausführen und dann in einem Browser Ihrer Wahl zu `http://127.0.0.1:1111/` navigieren. Sie können testen, ob der Pre-Commit-Hook ordnungsgemäß funktioniert, indem Sie einige Änderungen an Ihrem Quelltext vornehmen, sie dem Commit hinzufügen und dann einen Test-Commit ausführen:

```powershell
git add .
git commit -m "Testing."
```

Der Pre-Commit-Hook sollte jetzt ausgeführt werden und etwaige Probleme sollten auffallen. Sie können diesen Commit danach rückgängig machen:

```powershell
git reset --soft HEAD~
```

## GNU Linux Desktop und WSL

### Eine Anmerkung zum WSL

Das *Windows Subsystem für Linux (WSL)* ist eine gute Alternative zur Einrichtung von Zola unter Windows, wenn Sie es vorziehen, bei demselben Betriebssystem zu bleiben, das Sie auf dem Server verwenden. Ubuntu ist zwar die Standarddistribution, aber Sie können auch problemlos andere installieren. Wie Sie WSL einrichten, erfahren Sie in [Microsofts offizieller Installationsdokumentation](https://learn.microsoft.com/de-de/windows/wsl/install). 

### Das System vorbereiten

Unabhängig davon, ob Sie WSL verwenden oder einen GNU Linux-Desktop auf Ihrem Computer eingerichtet haben, müssen Sie sicherstellen, dass die erforderlichen Tools installiert sind:

```bash
sudo apt update
sudo apt upgrade
sudo apt install build-essential software-properties-common git
```

### Zola installieren

Auch wenn die Distribution auf Ihrem Server und Ihrem Desktop oder WSL nicht identisch sein sollten, sind die Schritte gleich, also sehen Sie sich bitte [die Installationsanweisungen für den Server weiter oben](#zola-installieren) an.

### Einrichten des Website-Quelltexts

Auch diese Schritte sollten Ihnen von der Servereinrichtung her bekannt sein:

```bash
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
```

Wenn Sie ein Theme verwenden, müssen Sie es ebenfalls neu einrichten:

```bash
git submodule update --init
cd themes/theme-name/
git pull origin main
cd ../../
```

### Pre-Commit-Hook

Dieses Verfahren ist dem in der [Pre-Commit-Hook-Konfiguration auf Windows](#pre-commit-hook) beschriebenen sehr ähnlich. Wenn sie noch nicht existiert, erstellen Sie die Datei für den Pre-Commit-Hook und fügen Sie das Skript hinzu, welches Sie ausführen möchten:

```bash
mkdir .githooks
touch .githooks/pre-commit
```

Dann konfigurieren Sie git:

```bash
git config core.hooksPath .githooks
```

#### Oxipng

In einer normalen Linux-Umgebung sollten die meisten benötigten Tools bereits verfügbar sein oder sich leicht über den jeweiligen Paketmanager installieren lassen, aber wir müssen oxipng manuell installieren -- zumindest unter Ubuntu. Schauen Sie sich die [Releaseseite von oxipng auf GitHub](https://github.com/shssoichiro/oxipng/releases) an und installieren Sie die neueste Version von oxipng für Ihre Linux-Distribution (`9.0.0` zum Veröffentlichungszeitpunkt) und installieren Sie es für den lokalen Benutzer wie folgt:

```bash
wget -O oxipng.tar.gz https://github.com/shssoichiro/oxipng/releases/download/v9.0.0/oxipng-9.0.0-aarch64-unknown-linux-gnu.tar.gz
tar -xvf oxipng.tar.gz
cp oxipng-9.0.0-aarch64-unknown-linux-gnu/oxipng ~/bin
sudo chmod +x ~/bin/oxipng
rm oxipng.tar.gz
rm -rf oxipng-9.0.0-aarch64-unknown-linux-gnu/
```

Alternativ können Sie das Programm auch in ein beliebiges anderes Verzeichnis verschieben, das in Ihrem `PATH` verfügbar ist.

#### *Nur Tabi*: GNU parallel, brotli, fonttools und shot-scraper

Um den gesamten Pre-Commit-Hook auszuführen, einschließlich der Tabi-spezifischen Teile, müssen wir noch ein paar weitere Tools installieren.

```bash
sudo apt install parallel python3-dev python3-pip
pip install brotli fonttools shot-scraper
shot-scraper install
sudo playwright install-deps
```

Damit sollten alle Tools installiert sein, die Sie für die Ausführung des Pre-Commit-Hooks benötigen. Bitte beachten Sie, dass Sie noch die BASH-Skripte benötigen, die in den Blog-Beiträgen über [Erstellen von einer benutzerdefinierten Schriftsatz-Untermenge und Social Cards](#nur-tabi-gnu-parallel-brotli-fonttools-und-shot-scraper) erwähnt werden.

## iOS

Jetzt wird es interessant, denn wir verlassen die Plattformen, auf denen statische Website-Generatoren normalerweise ausgeführt werden. Warum sollte man Zola überhaupt auf einem iOS-Gerät ausführen wollen?  
Ich persönlich arbeite gerne an Blogbeiträgen, wenn ich in kreativer Stimmung bin. Das kann so ziemlich überall sein, auch unterwegs. Daher ist es wichtig, dass ich mein iPhone oder [Android-Tablet](#android) verwenden kann, um an meinen Beiträgen zu arbeiten.

Das Hauptproblem ist, dass es keine Zola-App für iPhones gibt -- es gibt keine einfache Möglichkeit, Zola zu nutzen, welche sofort funktioniert. Stattdessen müssen wir eine App benutzen, die ein Linux-Betriebssystem auf einem iPhone oder iPad emuliert.

### iSH Shell: Alpine Linux auf iOS

Die beste Option für diesen Zweck, die ich finden konnte, ist [*iSH Shell*](https://apps.apple.com/de/app/ish-shell/id1436902243), die Alpine Linux auf iOS emuliert. Sie hat einige schwerwiegende Einschränkungen -- dazu später mehr --, aber sie ermöglicht es Ihnen, Zola lokal auf Ihrem iPhone auszuführen und so eine Vorschau ihrer Website anzuzeigen, ohne dass eine Internetverbindung erforderlich ist. Installieren Sie die App also über den App Store und starten Sie sie.

### Zu den aktuellsten Repositorys wechseln

iSH wird mit einem internen Paket-Repository ausgeliefert, da die App gemäß den Anforderungen von Apple für den App Store in sich geschlossen sein muss. Diese Pakete, einschließlich Zola, sind veraltet. Um das zu ändern, müssen wir die Quelle aktualisieren, aus der iSH die Pakete bezieht:

```ash
rm /etc/apk/repositories
echo https://dl-cdn.alpinelinux.org/alpine/v3.18/main >> /etc/apk/repositories
echo https://dl-cdn.alpinelinux.org/alpine/v3.18/community >> /etc/apk/repositories
apk update
apk upgrade
```

### Das System vorbereiten

Wie zuvor müssen wir einige Tools installieren:

```ash
apk add bash git
```

### Zola installieren

Dieses Mal ist die Installation von Zola sehr einfach:

```ash
apk add zola
```

Das sollte bereits die neueste Version von Zola installieren, falls Sie das apk-Community-Repository auf die neueste Version aktualisiert haben.

### Mounten eines Ordners mit Schreibberechtiung

Der erste wirkliche Haken bei der Verwendung von iSH ist, dass der Zugriff auf iSH-Dateien aus anderen Anwendungen nicht einfach ist. Sie können iSH zwar als Dateianbieter in der Dateien-App aktivieren, aber die Dateien sind schreibgeschützt, d.h. Sie können weder ihren Inhalt ändern noch sie aus dem iSH-Dateianbieters hinaus bewegen. Das ist für unseren Zweck natürlich nicht gerade ideal. Glücklicher Weise bietet iSH eine Möglichkeit, andere Dateianbieter zu mounten. Führen Sie den folgenden Befehl aus:

```ash
mount -t ios . /mnt
```

Es öffnet sich ein Ordnerauswahlfenster, in dem Sie ein Verzeichnis auswählen sollten, auf das iSH und andere Anwendungen zugreifen können. Sobald Sie Ihre Wahl bestätigt haben, wird `/mnt` den Inhalt dieses Verzeichnisses enthalten, sowohl mit Lese- als auch mit Schreibberechtigung.

### Ausführen von iSH Shell im Hintergrund

iOS hat eine nette Funktion: Wenn Sie eine App verlassen, läuft sie noch etwa fünf Minuten lang weiter und geht dann in eine Art Schlafmodus über, in dem die App keinen Speicher (RAM) mehr benötigt. Das bedeutet, dass Sie eine App nicht explizit schließen müssen, um sie davon abzuhalten, Ressourcen zu verbrauchen -- aber dies ist auch der Grund, weshalb iSH normalerweise einen Prozess wie einen Webserver nicht im Hintergrund weiterlaufen lassen kann. Da Sie aus der App heraus keine Webseiten öffnen können, müssen Sie iSH eine Aufgabe geben, die es der App ermöglicht, auch im Hintergrund aktiv zu bleiben. Führen Sie dazu den folgenden Befehl aus:

```ash
cat /dev/location > /dev/null &
```

Dieser Befehl greift auf Ihre aktuellen Standortdaten als kontinuierlichen Datenfluss zu und schreibt sie ins Nirgendwo (`/dev/null`), sodass die App auch im Hintergrund aktiv bleibt.

Ihr Gerät wird Sie auffordern, iSH die Erlaubnis zu erteilen, auf Ihren Standort zuzugreifen. Bestätigen Sie dies und ändern Sie die Berechtigungen in der Einstellungs-App so, dass iSH immer Zugriff auf Ihren Standort hat. Jetzt sollten die App und damit alle von ihr ausgeführten Prozesse aktiv bleiben. Um den Hintergrundprozess zu beenden und iSH zu erlauben, inaktiv zu werden, führen Sie den folgenden Befehl aus:

```ash
killall -9 cat
```

### Einrichten des  Website-Quelltexts

Diese Prozedur sollte Ihnen inzwischen vertraut sein, außer dass Sie den Quelltext in `/mnt` einrichten müssen:

```ash
cd /mnt
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
```

Erneut die Einrichtung des Themes:

```ash
git submodule update --init
cd themes/theme-name/
git pull origin main
cd ../../
```

### Nachteile der Verwendung von Zola in iSH

Einen Nachteil werden Sie sofort bemerken, wenn Sie versuchen, Ihre Website zu erstellen:

```ash
zola build --base-url 127.0.0.1
```

Aus unerfindlichen Gründen ist der normalerweise rasend schnelle Build-Prozess für Zola in iSH so langsam, dass Sie eine ausgedehnte Kaffeepause einlegen können, wenn Sie Ihre Website neu generieren wollen (zumindest auf meinem iPhone SE der 2. Generation). Ein weiterer Haken ist, dass `zola serve` überhaupt nicht funktioniert, sodass Sie einen anderen Weg finden müssen, um einen Webserver in iSH auszuführen. Der einfachste Weg ist wahrscheinlich die Verwendung des Python-Moduls `http.server`:

```ash
apk add python3
cd public/
python -m http.server
```

Wenn Sie einen Webbrowser öffnen und zu `http://127.0.0.1:8000/` navigieren, sollten Sie nun von der Homepage Ihres Blogs begrüßt werden.

### Pre-Commit-Hook

Die Prozedur sollte Ihnen inzwischen ziemlich vertraut sein. Falls noch nicht vorhanden, erstellen Sie die Datei für den Pre-Commit-Hook und fügen Sie das Skript hinzu, das Sie ausführen möchten:

```ash
mkdir .githooks
touch .githooks/pre-commit
```

Dann konfigurieren Sie git:

```ash
git config core.hooksPath .githooks
```

#### Oxipng

Die Installation von oxipng ist denkbar einfach:

```ash
apk add oxipng
```

#### *Nur Tabi*: GNU parallel, brotli und fonttools

Auf den ersten Blick sieht die Installation der Tools zur Ausführung des Pre-Commit-Hooks einfach aus:

```ash
apk add parallel python3-dev py3-pip
pip install brotli fonttools
```

#### Nachteil: shot-scraper nicht verfügbar

Die Dinge werden allerdings kompliziert, wenn Sie versuchen, shot-scraper zu installieren. Ich erspare Ihnen eine Menge Herumprobieren: Es gibt *keine* Möglichkeit, shot-scraper oder einen anderen Browser auf iSH zu installieren. Gar keine. Wenn Sie denselben Pre-Commit-Hook auf all Ihren Plattformen verwenden möchten, müssen Sie das Skript so ändern, dass es einfach nichts tut, wenn es auf `linux-musl` ausgeführt wird, dem Betriebssystemtyp von iSH:

```bash
if [["$OSTYPE" =~ "linux-musl"]]; then
    # Der Code, der die Erstellung der Social Cards mittels shot-scraper
    # ermöglicht, wird hier eingefügt
fi
```

Der Rest des Pre-Commit-Hooks wird korrekt ausgeführt (wenngleich langsam), allerdings müssen Sie die Änderungen in einen separaten Zweig committen. Dann können Sie diesen Zweig in Ihren Hauptzweig auf einer Plattform mergen, die shot-scraper unterstützt und abschließend die Änderungen erneut committen, um das vollständige Pre-Commit-Skript auszuführen. Unpraktisch, aber ich konnte trotz intensiver Bemühungen keine bessere Lösung finden.

## Android

Die letzte Plattform, für die ich eine Zola-Konfiguration demonstrieren werde, ist Android -- Android 13, um genau zu sein.

Ich dachte, dass es einfacher sein würde, Linux-Distributionen auf Android auszuführen als auf iOS, da Android den Ruf hat, eine weniger streng kontrollierte Plattform zu sein (und es bereits auf Linux basiert). In der Tat gibt es mehrere Möglichkeiten, GNU-Linux-Distributionen auf Android auszuführen, aber keine ist wirklich einfach. Für einige müssen Sie [Ihr Android-Gerät rooten](https://www.androidcentral.com/root), für andere müssen Sie [bestimmte Entwickleroptionen aktivieren](https://developer.android.com/studio/debug/dev-options) -- kurz gesagt, Sie müssen Änderungen an Ihrem Gerät vornehmen, die schwer zu implementieren sind, potenziell unerwünschte Nebenwirkungen haben und/oder schwer rückgängig zu machen sind. 

Die Lösung, die ich hier vorstelle, ist meiner Meinung nach die am wenigsten invasive, aber sie erfordert immer noch eine Menge Konfigurationsarbeit. Schnallen Sie sich also an.

### UserLAnd

*UserLAnd* ist eine Open-Source-Anwendung, die es ermöglicht, mehrere GNU-Linux-Distributionen auf einem Android-Gerät auszuführen, ohne es zu rooten. Sie ist sowohl im [Google Play store](https://play.google.com/store/apps/details?id=tech.ula) als auch im [F-Droid App Repository](https://f-droid.org/packages/tech.ula/) erhältlich. Meiner Erfahrung nach bietet Ihnen die Installation der App über F-Droid einige zusätzliche Freiheiten, wie z.B. die Wahl Ihres Benutzernamens beim ersten Ausführen einer bestimmten Distribution. Dennoch spielt es soweit ich weiß keine große Rolle, aus welcher Quelle Sie UserLAnd installieren.

Nach der Installation wählen Sie eine der angebotenen Distributionen aus -- ich habe die Einrichtung nur bei Ubuntu richtig hinbekommen, aber das mag daran liegen, dass ich mit dieser Distribution am vertrautesten bin -- und wählen Sie eine Verbindung über *SSH* statt *VNC*. Nach einer Weile (das kann bis zu einigen Minuten dauern) ist der Installationsvorgang abgeschlossen und ein Terminalfenster wird geöffnet. Zu diesem Zeitpunkt können Sie sich auch über SSH mit der *virtuellen Maschine* an Port `2022` verbinden, sogar von Geräten im selben Netzwerk aus. Allerdings müssen Sie die IP-Adresse Ihres Geräts in den Android-Einstellungen nachschauen, da Sie sie nicht über die Linux-Shell abfragen können.

#### Nachteile: Kein systemd, keine Root-Shell und eingeschränkte Zugriffsrechte

Die Verwendung von UserLAnd ist vergleichsweise simpel, aber diese relative Einfachheit hat ihren Preis. Zum einen sind einige Operationen einfach nicht erlaubt, wie z.B. das Starten einer *Root-Shell* mit `sudo -s`. Die Verwendung von `systemctl` zur Überwachung, zum Starten und Stoppen von Diensten ist nicht möglich. UserLAnd kann keine Anwendungen an den Standardports ausführen und auch nicht den Hostnamen abfragen, auf die IP-Adresse des Geräts zugreifen oder einen anderen Dateisystemanbieter mounten, wie es iSH Shell kann. Das verkompliziert einige Dinge, verhindert aber nicht die Ausführung von Zola und allen notwendigen Tools.

### Das System vorbereiten

UserLAnd wird mit abgespeckten Versionen der Distributionen ausgeliefert, sodass Sie wahrscheinlich zuerst einiges installieren müssen:

```bash
sudo apt update
sudo apt upgrade
sudo apt install software-properties-common build-essential git nano
```

### Zola installieren

Der Installationsvorgang ist identisch mit der [Installation von Zola auf dem Server](#zola-installieren); insbesondere müssen Sie das Programm möglicherweise aus dem Quellcode installieren.

### Einrichten des Website-Quelltexts

Dies ist die übliche Routine, bei der Sie das Repository klonen und das Theme aktualisieren (wenn Sie es als Git-Submodul eingerichtet haben):

```bash
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
git submodule update --init
cd themes/theme-name/
git pull origin main
cd ../../
```

### Pre-Commit-Hook

Glücklicherweise ist das Verfahren zur Einrichtung des Pre-Commit-Hooks und aller erforderlichen Tools identisch mit dem Verfahren, das unter [GNU linux und WSL](#pre-commit-hook-1) verwendet wird.

### *Optional*: Zugriff auf den Quelltext mit VS Code oder SFTP

Wie bereits erwähnt, bietet UserLAnd keine Möglichkeit, einen anderen Dateisystemanbieter zu mounten, wie es iSH Shell ermöglicht. Dies ist besonders problematisch, da Sie ab Android 13 von anderen Apps aus nicht direkt auf die Dateien im internen Speicher von UserLAnd zugreifen können. Die App bietet zwar einen eigenen Dateisystemanbieter, mit dem es grundsätzlich möglich ist, Dateien aus anderen Apps in UserLAnd zu verschieben, aber das ist, soweit ich das beurteilen kann, eine Einbahnstraße und macht das Schreiben und Veröffentlichen von Blogbeiträgen ziemlich mühsam.

Die Entwickler von UserLAnd bieten eine Möglichkeit, *Visual Studio Code* auf Ihrem Android-Gerät auszuführen und auf diese Weise auf die Dateien zuzugreifen. [Die DevStudio-App](https://play.google.com/store/apps/details?id=tech.ula.devstudio) ist für Blinde nicht barrierefrei, aber für sehende Menschen ist dies vielleicht die einfachste Lösung. Da ich die App nicht selbst testen kann, weiß ich nicht, wie die Einrichtung genau funktionieren würde. Ich vermute allerdings, dass es ähnlich wie bei der [Verwendung der Remote-SSH-Erweiterung](https://code.visualstudio.com/docs/remote/ssh) ist.

Wenn Sie allerdings wie ich blind sind oder nicht für die DevStudio-App bezahlen möchten, müssen Sie sich für einen *SFTP*-Server und einen Code-Editor entscheiden, der auf Dateien von einer SFTP-Quelle zugreifen kann, wie beispielsweise [Squircle CE](https://play.google.com/store/apps/details?id=com.blacksquircle.ui). Ich werde Ihnen zeigen, wie Sie einen solchen Server mit *ProFTPd* einrichten.

#### Verwirrende Dateiübertragungsprotokolle

Wenn es um Dateiübertragungen zwischen einem Server und einem Client geht, stehen uns in der Regel drei Protokolle zur Verfügung: *FTP*, *FTPS* und SFTP.  
FTP ist ein *sehr* altes Protokoll -- RFC 354, die erste Spezifikation dieses Protokolls, wurde 1972 veröffentlicht -- und gilt mittlerweile als unsicher. Wie HTTP überträgt es alle Daten im Klartext. Jeder, der den Datenverkehr in einem Netzwerk abhört (*Snooping*), kann die für den FTP-Zugang verwendeten Kennwörter abfangen und die übertragenen Dateien lesen.  
FTPS ist eine Erweiterung des FTP-Protokolls, die eine Ende-zu-Ende-Verschlüsselung unterstützt, ähnlich wie HTTPS. Es gilt aber auch [als veraltet](https://www.ssh. com/academy/ssh/ftp/ftps#ftps---combining-ftp-and-ssl).
SFTP hingegen ist ein Protokoll, das Dateiübertragungen über das SSH-Protokoll ermöglicht, welches für den sicheren Remote-Shell-Zugriff verwendet wird. SFTP ist heutzutage das bevorzugte Protokoll zur sicheren Übertragung von Dateien. Man könnte dies mit dem standardmäßigen SSH-Server von *OpenSSH* implementieren, aber UserLAnd verwendet eine benutzerdefinierte Methode für den SSH-Zugang, und ich wollte nicht damit herumspielen. Daher habe ich mich stattdessen für ProFTPd entschieden.

#### Installieren und Konfigurieren von ProFTPd

Die Installation ist einfach:

```bash
sudo apt install proftpd proftpd-mod-crypto
```

Dies installiert sowohl ProFTPd als auch die Tools, die für das SFTP-Modul von ProFTPd erforderlich sind. Wir müssen ProFTPd aber noch konfigurieren, um die SFTP-Unterstützung zu aktivieren.

Zunächst müssen wir einige Änderungen an `/etc/proftpd/proftpd.conf` vornehmen:

- Ändern Sie `ServerName` in einen für Ihren Server geeigneten Wert, z.B. den Hostnamen oder die IP-Adresse des Servers.
- Wenn Sie Ihre SFTP-Benutzer in deren Heimatverzeichnis (`/home/<Benutzername>/`) einsperren möchten, entfernen Sie das Kommentarzeichen in der Zeile `DefaultRoot` und ändern Sie sie in `DefaultRoot ~`.
- SFTP unterstützt das Flag `-l` für `ls` nicht. Suchen Sie also die Zeile `ListOptions` und kommentieren Sie sie aus: `#ListOptions "-l"`.
- Wenn Sie die Standardkonfigurationsdatei für SFTP verwenden möchten, suchen Sie abschließend die Zeile `#Include /etc/proftpd/sftp.conf` und löschen Sie das Kommentarzeichen.

Als nächstes bearbeiten Sie `/etc/proftpd/modules.conf`. Suchen Sie die Zeile `#LoadModule mod_sftp.c` und entfernen Sie das Kommentarzeichen.

Zuletzt bearbeiten Sie entweder `/etc/proftpd/sftp.conf` (wenn Sie das Kommentarzeichen vor der entsprechenden Direktive in `/etc/proftpd/proftpd.conf` entfernt haben) oder erstellen Sie eine Datei in `/etc/proftpd/conf.d/`. Alle Konfigurationsoptionen finden Sie in der [offiziellen Dokumentation des SFTP-Moduls](http://www.proftpd.org/docs/contrib/mod_sftp.html); meine Konfiguration sieht folgendermaßen aus:

```conf
<IfModule mod_sftp.c>
    SFTPEngine on
    Port 2222
    SFTPLog /var/log/proftpd/sftp.log

    # Konfiguriert die RSA- und DSA-Host-Schlüssel, indem die gleichen
    # Host-Schlüssel-Dateien, die OpenSSH verwendet, angegeben werden.
    SFTPHostKey /etc/ssh/ssh_host_rsa_key
    SFTPHostKey /etc/ssh/ssh_host_ecdsa_key
    
    # Ich habe die Unterstützung für die Passwortauthentifizierung aktiviert, 
    # da der SFTP-Server niemals vom Internet aus zugänglich sein sollte. 
    # Das ist allerdings weniger sicher.
    # Lösched Sie das Kommentarzeichen, wenn Sie dies auch aktivieren wollen.
        SFTPAuthMethods publickey# Passwort

    SFTPAuthorizedUserKeys file:/etc/proftpd/authorized_keys/%u

    # Komprimierung einschalten
    SFTPCompression delayed
</IfModule>
```

Sobald das alles erledigt ist, können Sie den SFTP-Server starten:

```bash
sudo service proftpd start
```

Ihr SFTP-Server sollte nun an Port `2222` erreichbar sein.

## Abschluss

Wir sind fertig. Hoffentlich funktioniert Ihre Zola-Einrichtung auf dem Server, dem Windows- und GNU-Linux-Desktop sowie auf Android- und iOS-Mobilgeräten. Ich habe sie zwar nicht explizit behandelt, da ich keine geeigneten Geräte besitze, aber der Einrichtungsprozess für MacOS und iPadOS sollte ähnlich ablaufen wie für GNU Linux bzw. iOS.

Dies ist ein langer Beitrag mit vielen möglichen Fallstricken und ich habe Wochen des Ausprobierens gebraucht, um alles bequem einzurichten. Wenn Sie also auf Probleme stoßen, zögern Sie bitte nicht, einen Kommentar zu hinterlassen oder mich auf anderem Wege zu kontaktieren. Ich werde mein Bestes tun, um Ihnen bei der Lösung aller Probleme zu helfen.

+++
title = "How to set up Zola to blog from anywhere"
date = 2023-11-17
updated = 2023-11-18
template = "page.html"

[taxonomies]
tags = ["Android", "blogging", "GNU Linux", "iOS", "setup", "tutorial", "Windows", "Zola"]

[extra]
toc_ignore_pattern = "^Table of contents$"
toc_levels = 2
isso = true
social_media_card = "img/social_cards/blog_zola_setup_to_blog_from_anywhere.jpg"
+++

[Using a static site generator for blogging](@/blog/static-site-generator-for-blogging.md) has many perks. There is one major downside, however: Previewing and publishing blog posts on different platforms does not work out of the box. This article aims to show you how you can configure different platforms to comfortably blog from anywhere.<!-- more -->

## Table of contents

<!-- toc -->

## Before we get started...

First, I think a few disclaimers are due.

- While blogging from the platforms may be comfortable once the setup is complete, that does not mean this process is easy. It took me quite a lot of trial and error and while I'm confident that I arrived at good solutions in most cases, there still may be room for improvements. Feel free to post any constructive feedback as a comment or reach out to me directly.
- I use the [*Tabi* theme](https://github.com/welpo/tabi) for my website and some of the steps I detail below are specific to Tabi. I will tag these with *Tabi only*.
- The *GNU Linux* distributions I use are *Ubuntu* and *Alpine*. Generally, I do think my instructions are valid for other distributions, too---however, if you run into trouble on other distributions that goes beyond figuring out an equivalent command, I cannot help you.
- This post is intentionally lengthz and in some places redundant. You can safely skip sections for platforms you are not interested in.

With that out of the way... Let us dive in!

## Prerequirities

This tutorial assumes your website uses *Zola* version `0.17.0` or later and your website source gets pushed to some repository on a *version control system (VCS)* like *GitHub*, *GitLab* or the like. Your VCS should also support *webhooks*, as this is how we will automate the building and deployment process on the server.

## Server

### Preparing the system

Let us start with the easy part. My server runs Ubuntu 20.04 LTS---if yours uses a different distribution, you may have to enter different commands; the essence should be the same, though.

```bash
sudo apt update
sudo apt upgrade
sudo apt install build-essential software-properties-common git
```

This installs various libraries and tools that you will likely need to set up Zola for blogging. Most notably it installs *git*, which you will need to pull new releases of your website source.

### Installing Zola

Some distributions ship Zola directly in an official repository; if your server supports *Snapcraft*, you can also [get Zola using snap](https://www.getzola.org/documentation/getting-started/installation/#snapcraft); there are [*Docker* images](https://www.getzola.org/documentation/getting-started/installation/#docker) available as well. Last but not least the Zola website offers specific [installation instructions for different platforms](https://www.getzola.org/documentation/getting-started/installation/).

I got the Zola binary manually from the GitHub releases, however: 

```bash
wget -O zola.tar.gz https://github.com/getzola/zola/releases/download/v0.17.2/zola-v0.17.2-x86_64-unknown-linux-gnu.tar.gz
tar -xvf zola.tar.gz
sudo mv zola /bin
sudo chmod +x /bin/zola
```

(You can, of course, move the binary somewhere else---all that matters is that it is in your `PATH`.)

`zola --version` should now print the version of Zola that you installed (`0.17.2` in my case). If you instead get an error message like `command not found` or `no such file or directory` you are missing some library that Zola needs to run. The easiest fix, in my opinion, is installing Zola from source.

#### Installing Zola from source

*NOTE:* If you already managed to install and run Zola successfully, you can skip this step.

Zola is written in [*Rust*](https://rust-lang.org/), so the first step to installing Zola from source is installing Rust. The recommended way to do so is via [*rustup*](https://rustup.rs/):

```bash
curl https://sh.rustup.rs -sSf | sh
```

Just press `1` and `ENTER` to proceed with the standard installation. Once that is complete run

```bash
source ~/.cargo/env
```

Next, clone the Zola source code, check out version `0.17.2` (or whichever version you want to install) and then install it using *Cargo*:

```bash
git clone https://github.com/getzola/zola.git
cd zola/
git checkout v0.17.2
cargo install --path .
```

This will take a while but once it is complete, Zola should be installed and ready to run. You can verify this using `zola --version`.

### Setting up the website source

With Zola installed we can now proceed to setting up the actual website. First, navigate to the directory where you'd like to clone the source, then run:

```bash
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
```

If you are using a theme, you likely added it as a *git submodule*---that is the recommended way to set this up, anyway. When you clone your website source for the first time, the submodule is not yet set up, which would result in an error if you tried generating your website with Zola right now. To fix that, run:

```bash
git submodule update --init
```

Additionally, the submodule (i.e., the theme) will likely be in an outdated state, so you also need to pull the latest version of it:

```bash
cd themes/<theme-name>/
git pull origin main
cd ../../
```

Now you can see if your website gets generated fine:

```bash
zola build
```

Point your web server at the generated `public/` directory and you should be able to visit your website as intended. Phew!

### :robot: Automate!

So far, the process of publishing your website is very manual:

- Whenever a new release of your website is available, you need to pull the sources from your repository,
- you also need to pull the latest version of your theme
- and then run `zola build`.

Normally, one would solve a problem like this using *continuous integration (CI)*. If you are hosting your website on *GitHub Pages* or similar services, [Zola may actually already have ready-made actions for you](https://www.getzola.org/documentation/deployment/overview/) which automate the deployment process. I use *Codeberg*, though, which currently uses *Woodpecker CI* for this kind of automation. A CI runner for *Forgejo*, the software solution underpinning Codeberg, [is currently available in alpha release quality](https://code.forgejo.org/forgejo-integration/runner), but I would not recommend using it yet. Instead I stumbled upon webhooks. They are pretty simple to set up and usually just send a `POST` request with some data to a URL of your choosing when a certain event occurs, e.g., changes getting pushed to a certain branch.

What happens at the URL you specified when that request gets received is up to you. If you already have *PHP* or some other scripting language installed on your server, you may choose to run a script that automatically pulls the changes into your local copy of the website source and builds the website. Alternatively, there are dedicated software solutions for handling webhooks that enable you to run shell scripts when a webhook gets triggered. Óscar Fernandez [explains this approach on osc.garden](https://osc.garden/blog/updating-site-with-webhook/). Regardless of which approach you use, make sure that you check the authenticity of the request and do proper logging, otherwise it will be hard to figure out why your website did not automatically update the way it should have.

## Windows

### Preparing the system

The only step you definitely have to take here is installing [git for Windows](https://gitforwindows.org/). That way you can clone the repository of your website source directly, the same way you did on the server.

### Installing Zola

Whilst the [installation instructions on the Zola website](https://www.getzola.org/documentation/getting-started/installation/#windows) recommend *Chocolatey* or *Scoop* for installing Zola on Windows, at least the Chocolatey package is outdated. Instead, you can [download the latest Zola version from the releases page on GitHub](https://github.com/getzola/zola/releases/) unzip it and copy the executable file to a location like `C:\Program Files\zola`. Then you need to add that directory to the `PATH` environment variable:

1. Press the Windows key and enter `environment` in the search bar.
2. Choose `Edit environment variables for this user` or `Edit system environment variables` depending on what you want (the latter requires administrator privileges and will make Zola available to every user).
3. Select the `Path` variable in the list of variables and click `edit`.
4. Click `New` then `Browse`.
5. A folder picker opens. Select the folder you want to add to the `PATH` environment variable, e.g., `C:\Program Files\zola`.
6. Click `OK` then `OK` again to close the environment variable window.
7. Close and reopen your PowerShell sessions.

Zola should now be available. You can confirm this by typing `zola --version`. It should print (at time of writing) `v0.17.2`.

### Setting up the website source

Now that Zla works on Windows as well, let us set up the source, for instance in sub directories of `C:\Users\<username>\zola`: 

```powershell
cd C:\Users\<username>
New-Item -path zola -ItemType Directory
cd .\zola
git clone https://your-vcs.tld/your-username/your-website.git
cd .\your-website
```

Again, if you use a theme for your website, you likely have set that up as a git submodule, which needs to get initialised and updated to the latest version on this system as well:

```powershell
git submodule update --init
cd .\themes\theme-name
git pull origin main
cd ..\..\
```

### Pre-commit hook

There are various tasks that a traditional *content management system* like *WordPress* would take care of for us, such as changing the update date automatically when changes occur in the content. To make that process less tedious for us, we can use a *git pre-commit hook*. Such a hook is a script that runs before a commit is made in a local repository. It can get used to validate or modify a commit and abort it if issues get encountered.  
You will likely want this pre-commit hook to be integrated into your repository on every platform where you intend to write blog posts---so it makes sense to keep the pre-commit hook in a file within the repository structure, which means we need to alter a git configuration for the repository, too:

```powershell
New-Item -path .githooks -ItemType Directory
New-Item -path .githooks\pre-commit -ItemType File
git config core.hooksPath .githooks
```

So what kind of things are worth putting into a pre-commit hook on a Zola project?

For some inspiration, it is worth checking out [this article on osc.garden](https://osc.garden/blog/zola-date-git-hook/), which admittedly contains a few Tabi-specific automations but also some generally useful ones.

Git bash already ships with most of the tools you will need to run the git hook successfully, like *grep* or *awk*. At least one needs to get installed and then added to the `PATH` environment variable, though.

#### Oxipng

To losslessly compress PNG images, you have two options: *optipng* or *oxipng*. oxipng is the more modern alternative, so I highly recommend it. You can download the latest release from the [GitHub releases page](https://github.com/shssoichiro/oxipng/releases) (at time of writing version `9.0.0`), move the binary to a suitable location and add it to the `PATH` environment variable the same way you did with Zola.

#### *Tabi only*: GNU parallel, brotli, fonttools and shot-scraper

*GNU parallel* is useful when you need to run the same task or a set of tasks on several files, e.g., the *Markdown* files containing blog posts. Rather than running the task(s) on each file in sequence, it runs them simultaneously on multiple files, in parallel. In the pre-commit hook, parallel gets used to quickly generate multiple screenshots of the website for the social cards, which likely is a Tabi-specific procedure.

To install GNU parallel such that Git BASH can use it, refer to [this StackOverflow answer](https://stackoverflow.com/a/52451011). Once the installation is complete, you should run parallel once, like this:

```bash
parallel echo ::: 1
```

This way, as the warning printed to the console indicates, parallel can look up the maximum command length once, which may take a while. Afterwards, it uses a cached value and therefore runs fast.

To run the Tabi-specific parts of the pre-commit hook, you also need to [download and install *Python*](https://www.python.org/downloads/), then execute the following command:

```powershell
pip install brotli fonttools shot-scraper
```

*brotli* and *fonttools* are necessary to generate the [custom font subset](https://welpo.github.io/tabi/blog/custom-font-subset/), *shot-scraper* gets used to automatically create screenshots of your website for [social cards](https://osc.garden/blog/automating-social-media-cards-zola/). Both articles contain scripts which you need to save to a file name `custom_subset` and `social-cards-zola` respectively, then move these files to a directory that you added to the `PATH` environment variable. Additionally, to make shot-scraper work correctly, you also need to run this command:

```powershell
shot-scraper install
```

That installs a *Chrome* version which can then get controled using *Playwright*. This should be all you need to do on Windows, but if running `shot-scraper www.google.com` results in an error message, you will need to additionally execute this command to install Playwright dependencies:

```powershell
playwright install-deps
```

With all that done, Windows should be set up comfortably. You can test this by running `zola serve --base-url 127.0.0.1` and then navigating to `http://127.0.0.1:1111/` in your browser of choice. You can test that the pre-commit hook works properly by making some changes to your source, staging them for commit, then commiting them like this:

```powershell
git add .
git commit -m "Testing."
```

The pre-commit hook should run now and any errors should become apparent. You can then undo that commit:

```powershell
git reset --soft HEAD~
```

## GNU Linux desktop and WSL

### A word on WSL

The *Windows Subsystem for Linux (WSL)* is a good alternative to setting up Zola on Windows if you prefer to stick to the same operating system you use on the server. While Ubuntu is the standard distribution you can easily install others. Please refer to [Microsoft's official installation guide](https://learn.microsoft.com/en-us/windows/wsl/install) to learn how to set up WSL.

### Preparing the system

No matter whether you use WSL or have a GNU Linux desktop set up on your computer, you will have to ensure the necessary tools are installed:

```bash
sudo apt update
sudo apt upgrade
sudo apt install build-essential software-properties-common git
```

### Installing Zola

While the distribution on your server and your desktop or WSL may not be identical, the steps are the same, so please [check out the installation instructions for the server above](#installing-zola).

### Setting up the website source

These steps, too, should be familiar from the server setup:

```bash
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
```

If you use a theme, you also need to set it up again:

```bash
git submodule update --init
cd themes/theme-name/
git pull origin main
cd ../../
```

### Pre-commit hook

This procedure is very similar to the one described in the [Windows pre-commit hook configuration](#pre-commit-hook). If it does not already exist, create the file for the pre-commit hook and add the script that you would like to run:

```bash
mkdir .githooks
touch .githooks/pre-commit
```

Then configure git:

```bash
git config core.hooksPath .githooks
```

#### Oxipng

In a regular Linux environment, most required tools should already be available or should be easy to install via the respective package manager, but we need to install oxipng manually---at least on Ubuntu. Check out the [oxipng releases page on GitHub](https://github.com/shssoichiro/oxipng/releases) and install the latest oxipng release for your Linux distribution (`9.0.0` at the time of writing) then install it for the local user like this:

```bash
wget -O oxipng.tar.gz https://github.com/shssoichiro/oxipng/releases/download/v9.0.0/oxipng-9.0.0-aarch64-unknown-linux-gnu.tar.gz
tar -xvf oxipng.tar.gz
cp oxipng-9.0.0-aarch64-unknown-linux-gnu/oxipng ~/bin
sudo chmod +x ~/bin/oxipng
rm oxipng.tar.gz
rm -rf oxipng-9.0.0-aarch64-unknown-linux-gnu/
```

Alternatively, you can move the binary to any other directory that will be available on your `PATH`.

#### *Tabi only*: GNU parallel, brotli, fonttools and shot-scraper

To run the entire pre-commit hook, including the Tabi-specific parts, we need to install a few more tools.

```bash
sudo apt install parallel python3-dev python3-pip
pip install brotli fonttools shot-scraper
shot-scraper install
sudo playwright install-deps
```

That should install all tools needed to run the pre-commit hook. Please note you will still need the BASH scripts mentioned in the blog posts on [creating custom font subsets and social cards](#tabi-only-gnu-parallel-brotli-fonttools-and-shot-scraper).

## iOS

Things are getting interesting now, because we are leaving the platforms static site generators usually run on. Why would one want to run Zola on an iOS device to begin with?  
Personally, I like to work on blog posts whenever I am in a creative mood. That can be pretty much anywhere, including on the road. Hence, being able to use my iPhone or [Android tablet](#android) to work on my posts is vital.

The primary problem is there is no Zola app for iPhones---there is no easy way to run it that just works out of the box. Instead, we need to opt for an app that emulates a Linux operating system on an iPhone or iPad.

### Introducing iSH Shell: Alpine Linux on iOS

The best option for this purpose that I was able to identify is [*iSH Shell*](https://apps.apple.com/de/app/ish-shell/id1436902243) which emulates Alpine Linux on iOS. It has some serious limitations---more on that later---but it enables you to run Zola locally on your iPhone and preview your website that way, no internet connection required. So install the app via the App Store and launch it.

### Switching to the most up-to-date repositories

By default, iSH ships with an internal package repository, since the app needs to be self-contained as per Apple's requirements for the App Store. Those packages, including Zola, are outdated. To fix that, we need to update the source from which iSH pulls packages:

```ash
rm /etc/apk/repositories
echo https://dl-cdn.alpinelinux.org/alpine/v3.18/main >> /etc/apk/repositories
echo https://dl-cdn.alpinelinux.org/alpine/v3.18/community >> /etc/apk/repositories
apk update
apk upgrade
```

### Preparing the system

As before, we need to install a few tools:

```ash
apk add bash git
```

### Installing Zola

This time, installing Zola is very easy:

```ash
apk add zola
```

That should already install the latest version of Zola, if you updated the apk community repository to the latest version.

### Mounting a writeable folder

The first real catch in using iSH is that accessing iSH files from other apps is not easy. While you can enable iSH as a file provider in the files app, the files are read-only, i.e., you can neither change their content, nor move them outside of the iSH file provider. That is less than ideal for our purpose, of course. Thankfully, iSH offers a means to mount other file providers. Run the following command:

```ash
mount -t ios . /mnt
```

A directory picker will open so you can select a folder which you want iSH (and other apps) to access. Once you confirmed your choice, `/mnt` will contain the content of that directory, both with read and write access.

### Running iSH shell in background

iOS has a nice feature: When you leave an app it will keep running for five minutes or so and then enter a kind of sleep mode in which the app does not take up any memory. This means you do not have to explicitly close an app to stop it from taking up resources---but it is also why iSH cannot keep on running a process like a web server in the background. Since you cannot open web pages from inside the app, you need to give iSH a task that allows it to stay active even when in the background. To do so, run the following command:

```ash
cat /dev/location > /dev/null &
```

This command accesses your current location data as a continuous stream and writes it to nowhere (`/dev/null`), thus keeping the app active even when in the background.

Your device will prompt you to give iSH permission to access your location. Confirm that and change the permissions in the settings app to always give iSH access to your location. Now the app and therefore any processes it runs should stay active. To terminate the background process and allow iSH to become inactive, run the following command:

```ash
killall -9 cat
```

### Setting up the website source

This procedure should be familiar by now, except for setting it up in `/mnt`:

```ash
cd /mnt
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
```

The theme setup, yet again:

```ash
git submodule update --init
cd themes/<theme-name>/
git pull origin main
cd ../../
```

### Downsides to using Zola in iSH

You will notice one downside immediately when you try to build your website:

```ash
zola build --base-url 127.0.0.1
```

For reasons unknown, the usually blazingly fast build process for Zola is painstakingly slow in iSH, to the point that you can go on an extended coffee break whenever you want to rebuild your website (at least on my 2nd generation iPhone SE). Another caveat is that `zola serve` does not work at all, so you need to find a different way to run a web server in iSH. The simplest way is probably using the `http.server` Python module:

```ash
apk add python3
cd public/
python -m http.server
```

If you open a web browser and navigate to `http://127.0.0.1:8000/` you should get greeted by your blog's homepage now.

### Pre-commit hook

The procedure should be fairly familiar by now. If it does not already exist, create the file for the pre-commit hook and add the script that you want to run:

```ash
mkdir .githooks
touch .githooks/pre-commit
```

Then configure git:

```ash
git config core.hooksPath .githooks/
```

#### Oxipng

Installing oxipng is simple enough:

```ash
apk add oxipng
```

#### *Tabi only*: GNU parallel, brotli and fonttools

At first glance, installing the tools to run the pre-commit hook looks simple:

```ash
apk add parallel python3-dev py3-pip
pip install brotli fonttools
```

#### Caveat: No access to shot-scraper

Things get complicated if you attempt installing shot-scraper, though. To save you a lot of trial and error: There is *no* way to install shot-scraper, or any browser for that matter, on iSH. At all. If you want to use the same pre-commit hook on all your platforms, you will have to alter the script such that it simply does nothing when the script is getting run on `linux-musl`, which is the OS type of iSH:

```bash
if [["$OSTYPE" =~ "linux-musl"]]; then
    # Code that executes the social card creation with shot-scraper goes here
fi
```

The rest of the pre-commit hook will run correctly (albeit slowly), but you will have to commit the changes to some separate branch, merge that branch into your main branch on a platform that supports shot-scraper, then commit the changes again to run the full pre-commit script. Impractical, but I could not come up with a better solution in spite of trying hard.

## Android

The last platform I will demonstrate a Zola setup for is Android---Android 13 to be specific.

I thought running Linux distributions on Android would be easier than on iOS, given Android has a reputation of being a less tightly controlled platform (and it is already based on Linux to begin with). Indeed, there are several ways to run GNU Linux distributions on Android but none are truly easy. Some require [rooting your Android device](https://www.androidcentral.com/root), others need you to [enable certain developer options](https://developer.android.com/studio/debug/dev-options)---in short, they require you to make changes to your device that can be hard to implement, have potentially undesirable side-effects and/or are difficult to undo.

The solution I came up with is the least invasive one, in my opinion, but it still requires plenty of configuring, so buckle up.

### UserLAnd

*UserLAnd* is an open source app that enables running several GNU Linux distributions on an Android device without rooting it. It is available both on the [Google Play store](https://play.google.com/store/apps/details?id=tech.ula) and on the [F-Droid app repository](https://f-droid.org/packages/tech.ula/). In my experience, installing the app via F-Droid gives you some extra liberties, such as choosing your username at the first start-up of a given distribution, but as far as I can tell it does not matter all that much from which source you install UserLAnd.

Once installed, select one of the offered distributions---I only managed to get the setup right on Ubuntu, but this may be due to me being most familiar with that distribution---and choose to connect via *SSH* instead of *VNC*. After a while (this can take up to a few minutes) the installation process is complete and a terminal window will open. At this point you can also connect to the *virtual machine* at port `2022`, even from devices in the same network. You will have to look up your device's IP address in the Android settings, though, since you cannot look them up from the Linux shell.

#### Caveat: No systemd, no root shell and reduced access rights

Using UserLAnd is comparatively easy, but that relative simplicity has a cost. For one thing, some operations simply are not permitted, like starting a *root shell* using `sudo -s`. Using `systemctl` to monitor, start and stop services is not possible. UserLAnd cannot run applications at the standard ports, nor can it query the hostname, access the device's IP address or mount a different file system provider the way iSH Shell can. That complicates some things, but it does not prevent running Zola and all the necessary tools.

### Preparing the system

UserLAnd ships with less pre-installed software in its distributions, so you will likely want to do some catching up first:

```bash
sudo apt update
sudo apt upgrade
sudo apt install software-properties-common build-essential git nano
```

### Installing Zola

The installation process is identical to [installing Zola on the server](#installing-zola); in particular, you may have to install it from source.

### Setting up the website source

This is the usual routine of cloning the repository and updating the theme (if you set one up as a git submodule):

```bash
git clone https://your-vcs.tld/your-username/your-website.git
cd your-website/
git submodule update --init
cd themes/<theme-name>/
git pull origin main
cd ../../
```

### Pre-commit hook

Thankfully, the process for setting up the pre-commit hook and all necessary tools is identical to the one used on [GNU linux and WSL](#pre-commit-hook-1).

### *Optional*: Accessing the source code with VS Code or SFTP

As mentioned before, UserLAnd does not offer a means to mount a different file system provider the way iSH Shell does. This is particularly problematic as you cannot access the files in UserLAnd's internal storage from other apps directly from Android 13 on. UserLAnd does offer its own file system provider which technically makes it possible to move files from other apps into UserLAnd, but this is, as far as I could tell, a one-way route and makes the process of writing and publishing blog posts pretty tedious.

The app creators of UserLAnd offer a means of running *Visual Studio Code* on your Android device and accessing the files that way. [The DevStudio app](https://play.google.com/store/apps/details?id=tech.ula.devstudio) is not accessible to the blind, but for sighted people this may be the easiest solution. Since I cannot test the app myself, I do not know how the setup would work exactly, but I am guessing it would be similar to [using the Remote SSH extension](https://code.visualstudio.com/docs/remote/ssh).

If you are blind like me or do not want to pay for the DevStudio app, though, you will have to opt for a *SFTP* server and a code editor that can access files from a SFTP source, like [Squircle CE](https://play.google.com/store/apps/details?id=com.blacksquircle.ui). I will show you how to set such a server up using *ProFTPd*.

#### Confusing file transfer protocols

When dealing with file transfers between a server and a client, there are commonly three protocols available to us: *FTP*, *FTPS* and SFTP.  
FTP is a *very* old protocol---RFC 354, the first specification of this protocol, got published in 1972---and is considered insecure by now. Like HTTP, it transfers all data as plain text---anyone listening in on the traffic on a network (*snooping*) can intercept the passwords used for the FTP access and read the files that get transfered.  
FTPS is an extension to the FTP protocol that supports ent-to-end encryption, similar to HTTPS. It is [considered obsolete](https://www.ssh.com/academy/ssh/ftp/ftps#ftps---combining-ftp-and-ssl), though.
SFTP, on the other hand, is a protocol that enables file transfers via the SSH protocol used to facilitate secure remote shell access. SFTP is the prefered means of transfering files securely nowadays. One could implement this using the default SSH server supplied by *OpenSSH*, but UserLAnd uses some custom means of providing SSH access and I did not want to mess with that. So I opted for using ProFTPd instead.

#### Installing and configuring ProFTPd

The installation is simple:

```bash
sudo apt install proftpd proftpd-mod-crypto
```

This installs both ProFTPd and the utilities necessary for its SFTP module, but we still have to configure ProFTPd to enable SFTP support.

First, we need to make some changes to `/etc/proftpd/proftpd.conf`:

- Change `ServerName` to a value appropriate to your server, e.g., the server's hostname or IP address.
- If you want to jail your SFTP users in their home directory (`/home/<username>/`), uncomment the `DefaultRoot` line and change it to `DefaultRoot ~`.
- SFTP does not support the `-l` flag to `ls`, so find the `ListOptions` line and comment it out: `#ListOptions "-l"`.
- Lastly, if you wish to use the default configuration file for SFTP, find the line that reads `#Include /etc/proftpd/sftp.conf` and uncomment it.

Next, edit `/etc/proftpd/modules.conf`. Find and uncomment the line `#LoadModule mod_sftp.c`.

Lastly, either edit `/etc/proftpd/sftp.conf` (if you uncommented the directive to include it in `/etc/proftpd/proftpd.conf`) or create a file in `/etc/proftpd/conf.d/`. For all configuration options, [check out the official SFTP module documentation](http://www.proftpd.org/docs/contrib/mod_sftp.html); my configuration looks like this:

```conf
<IfModule mod_sftp.c>
    SFTPEngine on
    Port 2222
    SFTPLog /var/log/proftpd/sftp.log

    # Configure both the RSA and DSA host keys, using the same host key
    # files that OpenSSH uses.
    SFTPHostKey /etc/ssh/ssh_host_rsa_key
    SFTPHostKey /etc/ssh/ssh_host_ecdsa_key
    
    # I enabled the support for password authentication, since the SFTP server 
    # should never be accessible from the internet. It is less secure, though.
    # Uncomment it if you want to enable it, too.
        SFTPAuthMethods publickey# password

    SFTPAuthorizedUserKeys file:/etc/proftpd/authorized_keys/%u

    # Enable compression
    SFTPCompression delayed
</IfModule>
```

Once all that is done, you can start the SFTP server:

```bash
sudo service proftpd start
```

Your SFTP server should now be available at port `2222`.

## Wrapping up

We are done. Hopefully, your Zola setup works on the server, Windows and GNU Linux desktop as well as Android and iOS mobile devices. While I did not explicitly cover them since I do not own any suitable devices, the setup process for MacOS and iPadOS should be similar to those for GNU Linux and iOS, respectively.

This is a long post with many moving parts and it took me weeks of trial and error to set all this up comfortably. So if you run into any problems trying to replicate what I did here, do not hesitate to leave a comment or contact me through different means. I will do my best to help solving any issues.

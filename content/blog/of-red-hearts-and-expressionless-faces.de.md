+++
title = "Von roten Herzen und ausdruckslosen Gesichtern"
date = 2023-10-05
updated = 2023-11-18
template = "page.html"
description = "Emoji sind überall verbreitet, aber für einen taubblinden Menschen sind sie alles andere als intuitiv. Ich versuche zu illustrieren, warum dies so ist."

[taxonomies]
tags = ["Emoji", "Kommunikation", "Meinung"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
isso = true
social_media_card = "img/social_cards/de_blog_of_red_hearts_and_expressionless_faces.jpg"
+++

## Inhaltsverzeichnis

<!-- toc -->

## Was ist los? :raised_eyebrow:

Emoji sind überall in unseren Texten zu finden, sie zieren die Ausgabe von hippen Kommandozeileninterfaces und tauchen zunehmend [in Gerichtsverfahren auf](https://edition.cnn.com/2019/07/08/tech/emoji-law/index.html). Obwohl sie aus der Sicht eines Computers nur eine weitere Gruppe von [Unicode-Zeichen](https://home.unicode.org/) sind, ist ihre Rolle in der textbasierten Kommunikation einzigartig. Sie fügen einer Aussage einen Kontext hinzu -- ähnlich wie ein Gesichtsausdruck oder der Tonfall einer Stimme.

Für taubblinde Menschen wie mich wird es schnell zu einer Herausforderung, Emoji richtig zu interpretieren. Sicher, textbasierte Smileys sind kein neues Konzept: Das [erste Beispiel des Smileys :-)](https://time.com/3341244/emoticon-birthday/) wurde 1982 veröffentlicht. Aber solche einfachen Smileys haben eine bestimmte Form, die man zeichnen, sich vorstellen und rationalisieren kann. Das macht es leichtersich sie zu merken und auch wenn es bei textbasierten Smileys Potenzial für Missverständnisse gibt, sind Emoji vielfältiger und komplexer. Daher gibt es viel mehr Möglichkeiten, sie (falsch) zu interpretieren.

## So ein Durcheinander! :skull:

Die menschliche Wahrnehmung von Gesichtsausdrücken unterscheidet sich bereits [zwischen verschiedenen Kulturen](https://www.apa.org/news/press/releases/2011/09/facial-expressions). Empirisch gesehen gilt das Gleiche für Emoji -- ihre Verwendung und Interpretation kann sehr unterschiedlich sein, oft abhängig davon, welche Art von Plattformen die jeweilige Person verwendet. Ein Beispiel ist das Totenkopf-Emoji, welches von einigen verwendet wird, um (selbst)abwertendes Lachen auszudrücken. Für eine blinde Person wie mich war dies nicht intuitiv zu verstehen: Wenn ich mir einen Totenkopf vorstelle, denke ich zuerst an die gewölbte Oberseite und die klaffenden Augenhöhlen; für Sehende ist das auffälligste Merkmal offenbar das breite, lippenlose "Grinsen" von Totenköpfen.

Dieses Grinsen ist der Grund, warum Totenköpfe oft in einem lachenden Kontext verwendet werden, aber es hat eine Weile gedauert, bis ich das verstanden habe.

## Zu viel Liebe :heart:

Ein weiteres Problem ist die mentale und emotionale Assoziation, die wir mit verschiedenen Symbolen eingehen. Von klein auf wurde mir gesagt, das rote Herz symbolisiere Liebe und Romantik. Kürzlich bemerkte ich, dass mehrere Leute das rote Herz sehr großzügig in meinen Konversationen mit ihnen verwendeten, fast wie Lesebestätigungen. Zuerst dachte ich, dass sie ein ernsthaftes romantisches Interesse an mir haben müssten; dann dämmerte mir, dass sie das rote Herz nicht im Zusammenhang mit Liebe, sondern mit "Gefällt mir" verwendeten.

Die Benutzeroberfläche von Social-Media-Plattformen ist für blinde Menschen wie mich text- und/oder sprachbasiert. Das bedeutet, dass die Schaltfläche, die ausdrückt, dass mir dieser Beitrag gefällt, oft mit "Favorit" oder "Gefällt mir" beschriftet ist. Aber für sehende Menschen ist diese Schaltfläche oft ein rotes Herz. Sie sehen dieses Symbol jeden Tag im Zusammenhang mit "Das gefällt mir" und entwickeln eine andere Assoziation damit als ich.

## Wütende ausdruckslose Gesichter :expressionless:

Eine Sache, die mir oft auffällt -- sowohl in Person als auch bei Emoji -- ist, dass Menschen dazu neigen, neutrale, leere oder ausdruckslose Gesichter negativ zu interpretieren. Wenn ich tief in Gedanken versunken bin, konzentriere ich mich nicht bewusst darauf, einen Gesichtsausdruck aufrechtzuerhalten. Offenbar wirkt der Ausdruck, den mein Gesicht dann annimmt, auf viele Menschen traurig oder düster. Das ist wahrscheinlich das, was die Leute ein "resting bitch face" nennen.

In ähnlicher Weise werden die ausdruckslosen und neutralen Emoji häufig von Menschen verwendet, um auszudrücken, dass sie wütend sind. Auch das ist etwas, welches ich mit der Zeit lernen musste.

## Schlussfolgerungen? :bulb:

Blinden Benutzern wird anstelle des Emoji ein beschreibender Text vorgesprochen oder angezeigt, und dieser Text kann mit der Absicht des Absenders des Emoji übereinstimmen oder auch nicht. Dies wiederum kann zu Missverständnissen führen, da die blinde Person möglicherweise nicht weiß, wie sie das Emoji richtig interpretieren soll. Im Zweifelsfall ist es eine gute Idee, zu erklären, wie man ein bestimmtes Emoji meint, bzw. nach der Absicht hinter diesem Emoji zu fragen; andernfalls lassen sich Kommunikationsprobleme kaum vermeiden.

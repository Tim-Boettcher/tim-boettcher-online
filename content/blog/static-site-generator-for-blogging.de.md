+++
title = "Warum ich auf einen statischen Website-Generator zum Bloggen umgestiegen bin"
date = 2023-10-05
updated = 2023-11-18
description = "Im Laufe der Jahre habe ich mehrmals versucht, mir das Bloggen zur Gewohnheit zu machen und dabei verschiedene Content-Management-Systeme (CMS) verwendet. In diesem Beitrag werde ich erklären, warum diese Projekte gescheitert sind und weshalb ich stattdessen zu einem statischen Website-Generator umgestiegen bin."

[taxonomies]
tags = ["bloggen", "Content-Management-Systeme", "Drupal", "Plume", "Statische Website-Generatoren", "WordPress", "WriteFreely", "Zola"]

[extra]
toc_ignore_pattern = "^Inhaltsverzeichnis$"
isso = true
social_media_card = "img/social_cards/de_blog_static_site_generator_for_blogging.jpg"
+++

Im Laufe der Jahre habe ich mehrmals versucht, mir das Bloggen mit verschiedenen Content-Management-Systemen (CMS) anzugewöhnen. In diesem Beitrag werde ich erklären, warum diese Projekte gescheitert sind und weshalb ich stattdessen auf einen statischen Seitengenerator umgestiegen bin.<!-- mehr -->

## Inhaltsverzeichnis

<!-- toc -->

-----

Vielleicht ist es Eitelkeit, aber ich habe das Gefühl, dass meine Perspektive ungewöhnlich ist. Es gibt nur wenige gesetzlich taubblinde Menschen unter 40 Jahren und noch weniger haben es geschafft, ein reguläres (d.h. nicht sonderpädagogisches) Gymnasium zu absolvieren -- ich bin sogar der erste Taubblinde, der dies in Deutschland geschafft hat. Ich mache trotz meiner Behinderungen drei Kampfsportarten, bin lizenzierter mathematisch-technischer Softwareentwickler und studiere angewandte Mathematik und Informatik. Genug Gründe, um meine Erfahrungen und Erkenntnisse mit der Welt zu teilen -- vorzugsweise über ein Medium, das für mich zugänglich ist: Text.

## Die bescheidenen Anfänge

Anfang der 2010er Jahre erstellte ich meine eigene Website, indem ich [HyperText Markup Language (HTML)](https://wiki.selfhtml.org/wiki/HTML) in einem einfachen Texteditor geschrieben habe -- die wirklich altmodische Art, Websites zu erstellen. Ich war damals ein junger Teenager, der das Konzept von HTML nicht richtig verstand, und meine rudimentären Werkzeuge trugen nicht gerade zur Qualität meiner Webinhalte bei. Dennoch, so peinlich diese frühen Projekte für mein dreiundzwanzigjähriges Ich auch sein mögen, brachten sie mich doch auf den Weg, Programmierer zu werden.

Allerdings schreiben nur noch wenige Menschen ganze HTML-Dokumente; das Internet hat sich erheblich demokratisiert, und die meisten, die heute Webinhalte erstellen, können nicht einmal HTML. Stattdessen nutzen sie Plattformen wie [Medium](https://medium.com/), [wordpress.com](https://wordpress.com/) oder [Ghost](https://ghost.org/), um ihre Inhalte zu veröffentlichen, je nach ihren jeweiligen Bedürfnissen. All diese Plattformen haben gemeinsam, dass es sich um Content-Management-Systeme (CMS) handelt.

## Überall Content-Management-Systeme

Wenn man sich [die 30 beliebtesten Websites, die WordPress im Jahr 2023 nutzen](https://colorlib.com/wp/most-popular-websites-powered-by-wordpress/) ansieht, wird eines schnell klar: WordPress ist bei Nachrichtenverlagen beliebt. [Das Gleiche gilt](https://www.vardot.com/en/ideas/blog/top-10-drupal-websites-world-updated) für [Drupal](https://www.drupal.org/), obwohl es eine etwas andere Klientel anzusprechen scheint. Ob Sie sich dessen bewusst sind oder nicht: Die meisten Websites, mit denen Sie interagieren, verwenden ein CMS -- Stand 14. September 2023 verzeichnet [trends.builtwith.com](https://trends.builtwith.com/cms/traffic/Entire-Internet) *77.679.306* Websites, die ein CMS verwenden. Aber was *sind* diese genau?

## Definition und Vorteile von CMS

> Content Management System, das: Ein Computersoftwaresystem zur Organisation und Erleichterung der gemeinschaftlichen Erstellung von Dokumenten und anderen Inhalten, insbesondere zum Laden auf eine Website. ([Wiktionary](https://en.m.wiktionary.org/wiki/Wiktionary:Main_Page), CC-BY-SA)

Mit anderen Worten: Ein CMS ist *jedes* Softwaresystem, das es den Benutzern ermöglicht, gemeinsam (Web-)Inhalte zu erstellen und diese Inhalte für sie zu verwalten. Das ist es, was ein CMS ideal für das Bloggen macht: Mehrere Personen können die Inhalte gemeinsam erstellen (z. B. Beiträge schreiben, Bilder hochladen oder Kommentare posten und moderieren), und das CMS verwaltet all diese Inhalte, normalerweise in einer Datenbank. Auch wenn dies nicht unbedingt in der obigen Definition enthalten ist, kümmert sich das CMS auch um die Darstellung der Inhalte für die Benutzer entsprechend ihrer Rolle, z. B. als schreibgeschützte Website, wenn es sich um Besucher handelt und als bearbeitbare Inhalte, wenn es sich um einen Autor handelt. Nachdem die Definitionen geklärt sind, werfen wir einen Blick auf die verschiedenen CMS, die ich im Laufe der Jahre zum Bloggen verwendet habe.

## Selbst hosten oder nicht selbst hosten

Ich persönlich liebe selbst gehostete Projekte. Es gibt einem viel mehr Kontrolle über das Aussehen eines Projekts, hat oft Vorteile für den Datenschutz und man lernt auf diese Weise eine Menge. Ich weiß jedoch, dass nicht jeder die Zeit, das Interesse, das Geld oder die Fähigkeiten hat, Projekte selbst zu hosten. Heutzutage ist das aber kein Problem mehr; fast alle CMS haben eine offizielle Version, die von den Entwicklern des CMS kostenlos gehostet wird, mit kostenpflichtigen Zusatzfunktionen. Alternativ können sie auch von einem Drittanbieter gehostet und verwaltet werden, auch wenn dies in der Regel teurer ist.

## WordPress

Die große Mehrheit der Websites mit einem CMS wird von WordPress betrieben. Das allein sollte uns schon verraten, dass es gut darin ist, was es tut. Es ist in zwei Varianten erhältlich: wordpress.com und wordpress.org. Erstere sind Websites, die von WordPress für Sie gehostet werden, mit verschiedenen kostenlosen und vielen weiteren kostenpflichtigen Funktionen. Bei letzteren handelt es sich um selbst gehostete Versionen der Open-Source-Software WordPress, bei denen die meisten Funktionen kostenlos sind, aber von Ihnen konfiguriert und gewartet werden müssen.

### Vorteile

- Sehr flexibel, kann mit der richtigen Konfiguration jede Art von Website betreiben,
- Riesige Community mit unzähligen Tutorials, Foren, Open-Source-Themes und -Plugins sowie engagierten WordPress-Designern und -Entwicklern,
- Kennzeichnungen auf Themes wie "accessibility-ready" helfen dabei, schnell einen Grundstandard für die Website festzulegen,
- Leistungsstarke Tools zum Entwerfen und Schreiben der Website-Inhalte direkt im Browser,
- Kostenlose Übersicht über mögliche Verbesserungen der Website mit Vorschlägen für Korrekturen,
- WordPress hat eine mobile App,
- Beliebte Editoren wie iA Writer lassen sich direkt in die Software integrieren.

### Nachteile

- Die schiere Menge an Konfigurationsoptionen, Plugins und Themes kann überwältigend sein,
- Da WordPress mit PHP-Skripten arbeitet, können Code-Schwachstellen in WordPress selbst oder in den Plugins oder Themes ein Problem darstellen,
- Es ist schwierig, Plugins oder WordPress selbst an die eigenen Bedürfnisse anzupassen; Änderungen können nach Aktualisierungen, von denen einige automatisch erfolgen, verloren gehen,
- Das Ändern von Übersetzungen ist nicht trivial und erfordert möglicherweise den Kauf eines speziellen Editors für die `.po`-Dateien.

### Warum ich nicht bei WordPress geblieben bin

Der Hauptgrund, warum ich nicht bei WordPress geblieben bin, ist die schiere Menge an Funktionen, die mich immer wieder von meinen Projekten ablenken. Ich habe viele Stunden damit verbracht, verschiedene Plugins, Einstellungen und Theme-Optionen zu optimieren, zu konfigurieren und zu versuchen, die Barrierefreiheit zu verbessern -- bis ich schließlich wieder das Interesse an dem Blogging-Projekt verlor und es verkommen ließ.

Ein weiteres Problem für mich war, dass es nicht trivial war, ein Detail zu ändern, wenn ich es für verbesserungswürdig hielt. Natürlich war es möglich -- jedes Detail in WordPress kann geändert werden, wenn man weiß, wie es geht -- aber das Labyrinth der PHP-Dateien ist schwer zu durchschauen und die direkte Bearbeitung der Dateien kann gefährlich sein. Außerdem müssen Änderungen nach jedem WordPress- oder Theme-Update neu eingespielt werden, es sei denn, man erstellt selbst ein Theme. Zumindest für mich war das alles zu lästig.

## Drupal

Ich habe Drupal nie wirklich durchschaut. Es ist angeblich sicherer als WordPress und anpassungsfähiger -- was allerdings den Nachteil hat, dass man tatsächlich einiges konfigurieren muss, bevor man seinen ersten Blogbeitrag schreiben kann. Im Gegensatz zu WordPress gibt es keine zentrale Organisation, bei der man Blogs hosten kann, aber einige Hosting-Anbieter bieten an, Drupal für Sie kostenlos (selbst verwaltet) oder gegen eine Gebühr (vom Hosting-Anbieter verwaltet) zu installieren. Leider verlor ich ziemlich schnell die Motivation, Drupal zu verstehen; auch hier stand das CMS dem regelmäßigen Schreiben von Blogbeiträgen im Weg.

## Writefreely

Also begann ich, mich nach inhaltsorientierten CMS umzusehen, die in der Regel weniger Anpassungsmöglichkeiten bieten und daher eine weniger unübersichtliche (und weniger leistungsfähige) Oberfläche haben. In der Regel können Sie Ihre Beiträge in [markdown](https://de.wikipedia.org/wiki/Markdown) verfassen. Das erste solche CMS, welches ich ausprobiert habe, war writefreely; es ist die Open-Source-Software, die [write.as](https://write.as/) zugrunde liegt. Ich hatte zwei Probleme mit ihr: An einigen Stellen könnte die Barrierefreiheit verbessert werden -- aber die Verbesserung der Website durch Änderung der Vorlagen war nicht so einfach, wie man denken würde. Auf jeden Fall war eine Änderung dieser Vorlagen nur auf dem Server möglich und erforderte einen Neustart des Writefreely-Dienstes.

## Plume

[Plume](https://github.com/Plume-org/Plume) ist ein föderiertes Blogsystem -- Leute, die das [fediverse](https://www.fediverse.to/) benutzen, können Autoren auf einer Plume-Instanz folgen und deren Beiträge aufgrund des zugrunde liegenden Protokolls favoritisieren, rebloggen und kommentieren. Viele Konzepte von Plume sind interessant und wenn Sie eine bestehende Instanz finden, die Registrierungen akzeptiert, müssen Sie selbst keine Konfiguration vornehmen. Leider hat Plume mehrere Nachteile:

- Es ist nicht sehr anpassungsfähig -- die Instanzen sollen ähnlich sein,
- Daher müssen alle Probleme mit der Barrierefreiheit der Plume-basierten Website im Plume-Projekt selbst behoben werden,
- Dieses Projekt ist in Rust geschrieben, was solche Änderungen nicht trivial macht (Rust hat eine sehr steile Lernkurve),
- Die Wartung und Entwicklung des Projekts scheint außerdem sporadisch und unregelmäßig zu sein.

## Statische Website-Generatoren anstelle eines CMS

Alle bisherigen Erfahrungen zusammengenommen sagten mir, dass ich eigentlich einen statischen Website-Generator für das Bloggen bräuchte. Sie akzeptieren in der Regel simple Markdown-Dateien als Eingabe, mit Metadaten wie Datum, Uhrzeit, Slug oder Tags am Anfang jeder Datei (Frontmatter genannt). Sie nutzen auch Vorlagen, um das genaue Aussehen und den Inhalt einer bestimmten Webseite zu bestimmen und unterstützen in der Regel Themes, die Ihrer Website ein vernünftiges, vordefiniertes Aussehen verleihen. Dieses kann auf verschiedenen Websites wiederverwendet werden.

Da statische Website-Generatoren einfache HTML-Dateien ohne serverseitiges Scripting erzeugen, können Sie das Ergebnis auf jeden beliebigen Webserver hochladen. Alternativ können auch [GitHub Pages] (https://pages.github.com/) und ähnliche Dienste Ihre statische Website für Sie hosten, oft sogar kostenlos.

Die bekanntesten Generatoren für statische Websites sind [Jekyll](https://jekyllrb.com/) und [Hugo](https://gohugo.io/), aber ich habe mich nicht für einen von ihnen entschieden, sondern für [Zola](https://www.getzola.org/).

### Zola

Welchen Generator für statische Websites Sie verwenden, bleibt letztendlich Ihnen überlassen. Da es sich oft um kostenlose und quelloffene Software handelt, können Sie sogar mehrere ausprobieren, um zu sehen, welcher Ihnen zusagt. Ich habe mich für Zola entschieden, weil

- Auf der Website steht: 
    > Zola nimmt Ihnen die Arbeit ab, damit Sie sich auf Ihre Inhalte konzentrieren können, sei es ein Blog, eine Wissensdatenbank, eine Landing Page oder eine Kombination davon.
- Zola benötigt keine Abhängigkeiten für Dinge wie das Parsen von Sass oder das Erstellen eines Inhaltsverzeichnisses,
- Es ist in Rust geschrieben, meiner Lieblingsprogrammiersprache; sollte ich später einen Beitrag zu Zola leisten wollen, wäre das genau mein Ding.

Ich werde in einem späteren Beitrag näher auf Zola eingehen[^1]; für den Moment genügt es zu sagen, dass Zola bisher gehalten hat, was es versprochen hat. Ich hoffe, dass ich mit dieser neuen Einrichtung endlich an einem Blogging-Projekt festhalten kann. 

-----

[^1]: Mittlerweile ist die [ausführliche Anleitung wie man Zola komfortabel einrichtet](@/blog/zola-setup-to-blog-from-anywhere.de.md) online!

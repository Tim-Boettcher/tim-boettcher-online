+++
title = "Neueste Beiträge"
template = "index.html"
sort_by = "date"

[extra]
header = {title = "Willkommen auf meiner Website!"}
section_path = "blog/_index.de.md"
max_posts = 4
post_listing_date = "both"
social_media_card = "img/social_cards/de.jpg"
+++

Dies ist meine persönliche Website, auf der ich Einsichten und Gedankengänge aus der Perspektive eines gesetzlich taubblinden Programmierers weitergebe. *Alle hier geäußerten Meinungen sind meine eigenen.*

Sie können [hier mehr über mich erfahren](@/pages/about.de.md).
